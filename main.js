/**
 * 入口
 */
cc.game.onStart = function(){
	if(!cc.sys.isNative && document.getElementById("cocosLoading"))
		document.body.removeChild(document.getElementById("cocosLoading"));

	cc.view.enableRetina(false);
	cc.view.adjustViewPort(true);
	cc.view.setDesignResolutionSize(1280, 768, cc.ResolutionPolicy.SHOW_ALL);
	cc.view.resizeWithBrowserSize(true);

	cc.LoaderScene.preload(g_resources_public, function() {
		gg.init();
		_ = new PlayMusic();
		// http://127.0.0.1:8000/?userId=16&expId=4/
		if (cc.sys.platform == cc.sys.DESKTOP_BROWSER) {
			// 浏览器
			uu.userId = $.getQueryString("userId");
			uu.expId = $.getQueryString("expId");
		} else if(cc.sys.os == "Android"
			|| cc.sys.os == "IOS"
				|| cc.sys.os == "iOS"){
			// Android IOS
			uu.userId = cc.sys.localStorage.getItem("userId");
			uu.expId = cc.sys.localStorage.getItem("expId");
			cc.sys.localStorage.removeItem("userId");
			cc.sys.localStorage.removeItem("expId");
		} else if(cc.sys.os == "Windows"){
			// cc.log("js get from c++: " + osInfo());
		}

		if(!!uu.userId && !!uu.expId){
			uu.loginType = LOGIN_TYPE_PLATFORM;
		} else {
			uu.loginType = LOGIN_TYPE_APP;
		}
		ch.addPublicRes();
		ch.howToGo(OP_TYPE_LOGIN);
	}, this);
	// 热更新
//	var scene = new HotUpdateScene();
//	scene.run();
};
cc.game.run();