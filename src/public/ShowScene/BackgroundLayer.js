var ShowBackgroundLayer = cc.Layer.extend({
    ctor:function (tag) {
        this._super();
        this.loadBg();
        this.loadback();
        this.loadtitle(tag);
        return true;
    },
    loadBg : function(){
    	var node = new cc.Sprite(res_public.run_back);
        this.addChild(node);
        node.setPosition(gg.c_p);
    },
    loadback :function(){
    	this.backButton = new ButtonScale(this,"#button/button_back.png", this.callback);
    	this.backButton.setLocalZOrder(5);
    	this.backButton.setPosition(cc.p(20 + this.backButton.width * 0.5, gg.height - 20 - this.backButton.height * 0.5));
    	this.backButton.setTag(TAG_BACK);
    },
    loadtitle :function(tag){
    	for(var i in expInfo){
    		var exp = expInfo[i];
    		if(gg.expId == exp.expId){
    			for(var j in exp.menuItem ){
    				menuItem = exp.menuItem[j];
    				if(menuItem.tag == tag){
    					var  label = new cc.LabelTTF(menuItem.menu,gg.fontName,30);
    					label.setPosition(this.backButton.x + label.width/2 + 50,this.backButton.y);
    					label.setColor(cc.color(19, 98, 27, 250));
    					this.addChild(label,5);
    					break;
    				}
    			}
    			break;
    		}  		
    	}
    },
    callback:function(){
    	ch.gotoStart(gg.runExpTag, g_resources_public_start, res_public_start.start_p );//返回开始界面	
    }
});