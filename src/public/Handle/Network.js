var /**
	 * 
	 */
net = {
	server_url:'http://121.40.195.52:8080/teach_platform/service',
	getUrl:/**
			 * 获取服务器URL
			 */
	function(){
		var url = this.server_url;
		if(md.server_url != null){
			url = md.server_url;
		}
		return url;
	},
	saveScore:function(){
		// /expRec/upload/{userId}/{expVer}/{expId}/{isFinished}/{lastStep}/{score}/{level}/{beginTime}/{endTime}/{totalStep}/{rightStep}
		var url = this.getUrl();
		url += "/expRec/upload";
		url += "/"+uu.userId;// userId
		//url += "/"+gg.userId;// userId
		url += "/"+gg.expVer;// expVer
		url += "/"+gg.expId;// expId

		url += "/"+1;// isFinished,1已过关
		url += "/"+gg.lastStep;// lastStep
		url += "/"+gg.score;// score
		url += "/"+gg.teach_type;// level
		url += "/"+$.sdf(gg.begin_time,'yyyyMMddhhmmss');// beginTime
		url += "/"+$.sdf(gg.end_time,'yyyyMMddhhmmss');// endTime
		url += "/"+gg.totalStep;// totalStep
		url += "/"+gg.totalStep;// rightStep
		cc.log("提交成绩:" + url);
		this.ajax(url);
	},
	login: function(workCode, passwd, func, target){
		var url = this.getUrl() + "/login/{workCode}/{passwd}";
		url = url.replace("{workCode}",workCode).replace("{passwd}",passwd);
		this.ajax(url, function(){
			if (this.readyState == 4 && (this.status >= 200 && this.status <= 207)) {
				var result = this.responseText;
				var jObject = JSON.parse(result);
				func.call(target, jObject);
			}
		});
	},
	getSubject:function(cb){
		var url = "http://teach-platform.oss-cn-hangzhou.aliyuncs.com/experiment/jsonLib/subject.json";
		this.ajax(url, function(a1, a2, a3){
			cc.log(a1);
		});
	},
	ajax:function(url,func,method){
		var xhr = cc.loader.getXMLHttpRequest();
		if(method != null){
			xhr.open(method, url);
		} else {
			xhr.open("GET", url);	
		}
		if(func != null){
			xhr.onreadystatechange = func.bind(xhr);	
		}
		xhr.send();
	}
};