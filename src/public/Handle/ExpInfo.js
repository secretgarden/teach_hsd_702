/**
 *实验配置信息
 */
expInfo = [
{
	expName:"公共部分",
	expTag:TAG_EXP_PUBLIC,
	expId:-1,
	loadRes:[{
		expTag:TAG_EXP_PUBLIC,
		type:TAG_LOAD_TYPE_START,
		finish:false,
		res:g_resources_public_start
	},{
		expTag:TAG_EXP_PUBLIC,
		type:TAG_LOAD_TYPE_RUN,
		finish:false,
		res:g_resources_public_run
	},{
		expTag:TAG_EXP_PUBLIC,
		type:TAG_LOAD_TYPE_ABOUT,
		finish:false,
		res:g_resources_public_about
	},{
		expTag:TAG_EXP_PUBLIC,
		type:TAG_LOAD_TYPE_GAME,
		finish:false,
		res:g_resources_public_game
	},{
		expTag:TAG_EXP_PUBLIC,
		type:TAG_LOAD_TYPE_FINISH,
		finish:false,
		res:g_resources_public_finish
	},{
		expTag:TAG_EXP_PUBLIC,
		type:TAG_LOAD_TYPE_TEST,
		finish:false,
		res:g_resources_public_test
	},]
},
{
	expName: "7.1  绿色开花植物的有性生殖和发育",
	expTag: TAG_EXP_01,
	expId: 100,
	expimage:"#exp/exp01.png",
	startPng:res_start01.start_png,
	YuanLi:"1、浸在液体里的物体，受到向上的浮力，浮" +
	"\n     力的大小等于物体排开的液体受到的重力",
	MuDi:"1、通过实验验证阿基米德原理的正确性" +
	"\n2、加深对阿基米德原理的理解" +
	"\n3、培养学生的实验操作能力",
	expDemo : null,
	run: function(){
		gg.runNext = startRunNext01;
		gg.initExp(this.expId,this.expName,this.YuanLi,this.MuDi,this.expDemo, exp01.teach_flow01);
		ch.gotoStart(this.expTag, this.loadRes[0].res, res_start01.start_p );
	},
	menuItem :[
	           {
	        	   menu:"花的结构",
	        	   tag:TAG_MENU1 
	           },
	           {
	        	   menu:"开花和传粉",
	        	   tag:TAG_MENU2 
	           },
	           {
	        	   menu:"受精和果实、种子的形成",
	        	   tag:TAG_MENU3 
	           }         
	           ],
	           loadRes:[{
	        	   expTag:TAG_EXP_01,
	        	   type:TAG_LOAD_TYPE_START,
	        	   finish:false,
	        	   res:g_resources_start01
	           },{
	        	   expTag:TAG_EXP_01,
	        	   type:TAG_LOAD_TYPE_RUN,
	        	   finish:false,
	        	   res:g_resources_run01
	           },{
	        	   expTag:TAG_EXP_01,
	        	   type:TAG_LOAD_TYPE_FINISH,
	        	   finish:false,
	        	   res:[]
	           },{
	        	   expTag:TAG_EXP_01,
	        	   type:TAG_LOAD_TYPE_SHOW,
	        	   finish:false,
	        	   res:g_resources_show01
	           },{
	        	   expTag:TAG_EXP_01,
	        	   type:TAG_LOAD_TYPE_ABOUT,
	        	   finish:false,
	        	   res:[]
	           },{
	        	   expTag:TAG_EXP_01,
	        	   type:TAG_LOAD_TYPE_TEST,
	        	   finish:false,
	        	   res:[]
	           }]
},
{
	expName: "7.2 种子的萌发和幼苗的形成",
	expTag: TAG_EXP_02,
	expId: 104,
	expimage:"#exp/exp02.png",
	startPng:res_start02.start_png,
	YuanLi:"1、闭合开关，电磁铁通电产生磁性，" +
	"\n     吸引衔铁，敲打电铃" +
	"\n2、衔铁脱离螺钉，电磁铁断电失去磁性，" +
	"\n     衔铁复原，电磁铁通电",
	MuDi:"1、了解电铃工作原理" +
	"\n2、了解电磁铁通电产生磁场的特性",
	expDemo:TAG_TYPE_CESHI,
	run: function(){
		gg.runNext = startRunNext02;
		gg.initExp(this.expId,this.expName,this.YuanLi,this.MuDi,this.expDemo, exp02.teach_flow02);
		ch.gotoStart(this.expTag, this.loadRes[0].res, res_start02.start_p);
	},
	menuItem :[
	           {
	        	   menu:"种子的结构",
	        	   tag:TAG_MENU1 
	           },
	           {
	        	   menu:"种子的寿命",
	        	   tag:TAG_MENU2 
	           },
	           {
	        	   menu:"种子的萌发和幼苗的生长",
	        	   tag:TAG_MENU3 
	           }         
	           ],
	loadRes:[{
		expTag:TAG_EXP_02,
		type:TAG_LOAD_TYPE_START,
		finish:false,
		res:g_resources_start02
	},{
		expTag:TAG_EXP_02,
		type:TAG_LOAD_TYPE_RUN,
		finish:false,
		res:g_resources_run02
	},{
		expTag:TAG_EXP_02,
		type:TAG_LOAD_TYPE_FINISH,
		finish:false,
		res:[]
	},{
		expTag:TAG_EXP_02,
		type:TAG_LOAD_TYPE_SHOW,
		finish:false,
		res:g_resources_show02
	},{
		expTag:TAG_EXP_02,
		type:TAG_LOAD_TYPE_ABOUT,
		finish:false,
		res:[]
	},{
		expTag:TAG_EXP_02,
		type:TAG_LOAD_TYPE_TEST,
		finish:false,
		res:[]
	}]
}

]
