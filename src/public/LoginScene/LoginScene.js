var LoginLayer = cc.Layer.extend({
	backgroundLayer : null,
	mainLayar : null,
	ctor:function () {
		this._super();
		this.loadBackground();
		this.loadMainLayer();
	},
	loadBackground : function(){
		this.backgroundLayer = new LoginBackgroundLayer();
		this.addChild(this.backgroundLayer);
	},
	loadMainLayer : function(){
		this.mainLayar = new LoginMainLayer();
		this.addChild(this.mainLayar);
	}
});

var LoginScene = PScene.extend({
	onEnter:function () {
		this._super();
		var layer = new LoginLayer();
		this.addChild(layer);
	}
});
