DIRECTION_RIGHT = 0;
DIRECTION_LEFT = 1;
DIRECTION_DOWN = 2;
DIRECTION_UP = 3;
//括号
Bracket = cc.Node.extend({
	marign : 10,
	ctor:/**
			 * @param parent
			 * @param startNode
			 *            开始点
			 * @param endNode
			 *            结束点
			 * @param direction
			 *            方向 0,向右 1,向左 2,向下 3,向上
			 */
	function(parent,startNode,endNode, direction){
		this._super();
		if(!direction){
			direction = 0;
		}
		parent.addChild(this);
		this.drawBracket(startNode, endNode, direction);
	},
	drawBracket:function(startNode, endNode, direction){
		var start = startNode.getPosition();
		var end = endNode.getPosition();
		var startAp = startNode.getAnchorPoint();
		var endAp = endNode.getAnchorPoint();
		var sX = start.x - startNode.width * startAp.x;
		var eX = end.x - endNode.width * endAp.x;
		// 大括号的高度/宽度
		var distance = 0;
		var posX = 0, posY = 0;
		if(direction == 0 && direction == 1){// 左右
			distance = start.y > end.y ? start.y - end.y : end.y - start.y;
			
			posX =  start.x - startNode.width * startAp.x + end.x - endNode.width * endAp.x;
			posY = (start.y + end.y) / 2;
		} else if(direction == 2 && direction == 3){//上下
			distance = start.x - end.x > 0 ? start.x - end.x : end.x - start.x;
			posX = (start.x - startNode.width * startAp.x + end.x - start) / 2;
			posY = (start.y + end.y) / 2;
		}
		if(direction == 0){
			this.drawRight(start, end, startNode, endNode, startAp, endAp);
		} else if(direction == 1){
			this.drawLeft(start, end, startNode, endNode, startAp, endAp);
		} else if(direction == 2){
			this.drawdDown();
		} else if(direction == 3){
			this.drawUp();
		}
	},
	drawLeft:function(start, end, startNode, endNode, startAp, endAp) {
		var distance = start.y > end.y ? start.y - end.y : end.y - start.y;//高度
		distance = distance / 2;
		var posX = 0, posY = 0;
		var sX = start.x + startNode.width * (1 - startAp.x);
		var eX = end.x + endNode.width * (1 - endAp.x);
		posX =  sX < eX ? eX : sX;
		posY = (start.y + end.y) / 2;
		
		var t1 = new cc.Sprite("#bracket/1.png");
		t1.setPosition(posX + t1.width * 0.5 + this.marign,posY + t1.height * 0.5);
		t1.setRotationY(180);
		this.addChild(t1);
		var t2 = new cc.Sprite("#bracket/2.png");
		t2.setScaleY(distance / 20);
		$.up(t1, t2, -1);
		this.addChild(t2);
		var t3 = new cc.Sprite("#bracket/3.png");
		t3.setRotationY(180);
		$.up(t2, t3, -1);
		this.addChild(t3);
		
		var t4 = new cc.Sprite("#bracket/1.png");
		t4.setPosition(posX + t4.width * 0.5 + this.marign,posY - t4.height * 0.5);
		t4.setRotation(180);
		this.addChild(t4);
		var t5 = new cc.Sprite("#bracket/2.png");
		t5.setScaleY(distance / 20);
		$.down(t4, t5, -1);
		this.addChild(t5);
		var t6 = new cc.Sprite("#bracket/3.png");
		t6.setRotation(180);
		$.down(t5, t6, -1);
		this.addChild(t6);
	},
	drawRight:function(start, end, startNode, endNode, startAp, endAp) {
		var distance = start.y > end.y ? start.y - end.y : end.y - start.y;//高度
		distance = distance / 2;
		var posX = 0, posY = 0;
		var sX = start.x - startNode.width * startAp.x;
		var eX = end.x - endNode.width * endAp.x;
		posX =  sX > eX ? eX : sX;
		posY = (start.y + end.y) / 2;
		
		var t1 = new cc.Sprite("#bracket/1.png");
		t1.setPosition(posX - t1.width * 0.5 - this.marign,posY + t1.height * 0.5);
		this.addChild(t1);
		var t2 = new cc.Sprite("#bracket/2.png");
		t2.setScaleY(distance / 20);
		$.up(t1, t2, -1);
		this.addChild(t2);
		var t3 = new cc.Sprite("#bracket/3.png");
		$.up(t2, t3, -1);
		this.addChild(t3);
	
		var t4 = new cc.Sprite("#bracket/1.png");
		t4.setPosition(posX - t4.width * 0.5 - this.marign,posY - t4.height * 0.5);
		t4.setRotationX(180);
		this.addChild(t4);
		var t5 = new cc.Sprite("#bracket/2.png");
		t5.setScaleY(distance / 20);
		$.down(t4, t5, -1);
		this.addChild(t5);
		var t6 = new cc.Sprite("#bracket/3.png");
		t6.setRotationX(180);
		$.down(t5, t6, -1);
		this.addChild(t6);
	},
	drawdDown:function() {

	},
	drawUp:function() {

	}
});