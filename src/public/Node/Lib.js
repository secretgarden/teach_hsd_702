/**
 * 药品库
 */
Lib = cc.Sprite.extend({
	libArr: [],
	openFlag:true,
	doing:false,
	doright:true,
	rm : -5,
	ctor:function(p){
		this._super("#lib/libshow.png");
		p.addChild(this,300);
		this.setTag(TAG_LIB);
		this.setCascadeOpacityEnabled(true);
		this.loadButton();
	},
	loadButton:function(){
		this.cancel = new ButtonScale(this,"#button/cancel.png",this.action);
		this.sure = new ButtonScale(this,"#button/sure.png",this.action);
		this.cancel.setPosition(cc.p(296 + this.cancel.width*0.5,  this.height - 446 - this.cancel.height*0.5));
		this.sure.setPosition(cc.p(526 + this.sure.width*0.5, this.height - 446 - this.sure.height*0.5 ));
		this.cancel.setTag(TAG_CANCEL);
		this.sure.setTag(TAG_SURE);
	},	
	loadBg:function(tags){
		this.setPosition(cc.p(140 + this.width*0.5, gg.height - 76 - this.height*0.5));
		this.checkArr = [];//错误数组
		this.rightArr = [];//正确数组
		var first = true;
		var firstPos = cc.p( 7 , this.height - 14);
		var prev = null;
		var num = 0;
		var j =1;
		for(var i in tags){
			var bg = new Button(this,2,TAG_LIB_BG+j,"#lib/uncheck.png", this.callback);	
			if(gg.teach_type == TAG_REAL){
				//实战模式中，bg可点击
				bg.setEnable(true);	
			}					
			j++;
			bg.setAnchorPoint(0,1);
			var rel = this.getLibRel(tags[i].tag);//通过TAG获取配置数组里的数据
			var lib = new LibButton(bg,4, rel.tag, rel.img, this.callback,this);			
			bg.setCascadeOpacityEnabled(true);
			lib.setCascadeOpacityEnabled(true);
			lib.setPosition(cc.p(bg.width*0.5,bg.height*0.5+10));
			//第一个背景格子的坐标
			if(first){
				first = false;
				bg.setPosition(firstPos);
			} else {				
				//方格摆放顺序实际从左到右，第一行1 2 3 4 5  ，  第二行10 9 8 7 6
				if(num == 5){
					bg.down(prev, this.rm);
				}else if(num >5){
					bg.left(prev, this.rm);
				}else{
					bg.right(prev, this.rm);
				}				
			}
			if(tags[i].checkright){
				//正确的,传入数组				
				this.rightArr.push(bg);
			}else{
				//错误的
				this.checkArr.push(bg);
			}
			prev = bg;
			num++;
		}		
	},
	getLibRel:function(tag){
		for(var i in libRelArr){
			if(libRelArr[i].tag == tag){
				return libRelArr[i];
			}
		}
	},
	callback:function(p){
		//检查仪器的父类（背景）是否点击
		if(!p.check){
			p.setSpriteFrame("lib/check.png");
			p.check = true;
			//p的子类（仪器）的名称可见
			p.getChildren()[0].getChildByTag(TAG_CLIPPING).getChildByTag(TAG_WORD_BG).runAction(cc.moveBy(0.4,0,58));
		}else{
			p.setSpriteFrame("lib/uncheck.png");
			p.check = false;
			p.getChildren()[0].getChildByTag(TAG_CLIPPING).getChildByTag(TAG_WORD_BG).runAction(cc.moveBy(0.4,0,-58));
		}	
		p.setEnable(true);	

		for(var i in this.rightArr){//如果是正确的就进行下一步
			if (this.rightArr[i].getTag() == p.getTag()){
				if(!p.click){
					p.click=true;
					gg.flow.next();
				}
			}			
		}	
	},
	action:function(p){
		switch(p.getTag()){
		case TAG_SURE:
			if(this.doright){
				var max = 0;
				//错误仪器中是否被选中，任意被选中，即错
				for(var i  in this.checkArr){						
					if(this.checkArr[i].check  ){
						this.show = new WinFrame(ll.tip,2,"仪器选择错误！");
						this.show.open2();
						//this.show.buttonSetEnable2(false);
						ll.tip.mdScore(-3);
						return;
					}
				}
				//正确仪器中是否被选中，任意被选中，即错
				for(var i  in this.rightArr){
					if(this.rightArr[i].check){
						max++;																			
					}								
				}
				if(max == 0){
					if(ll.run.getChildByTag(TAG_SHOW)==null){
						this.show = new WinFrame(ll.tip,2,"请选择仪器！");
						this.show.open2();
						//this.show.buttonSetEnable2(false);
						return;
					}
				}else if(max == this.rightArr.length){
					gg.flow.next();
					this.doright=false;		
					this.close();
					ll.run.checkVisible(true);
					ll.tip.mdScore(10);
					gg.scoreCheck = true;
					p.setEnable(true);
					this.setUnEnable();//实验过程中，无法选择仪器
				}else{
					if(ll.run.getChildByTag(TAG_SHOW)==null){
						this.show = new WinFrame(ll.tip,2,"仪器选择不足！");
						this.show.open2();
						//this.show.buttonSetEnable2(false);
						ll.tip.mdScore(-3);	
						return;
					}				
				}				
			}else{
				this.show = new WinFrame(ll.tip,2,"仪器已选择！");
				this.show.open2();
				//this.show.buttonSetEnable2(false);
			}
			p.setEnable(false);
			break;
		case TAG_CANCEL:
			this.close();
			ll.tip.arr.fadein();
			break;
		}	
	},
	isOpen:function(){
		return this.openFlag; 
	},
	open:function(){
		this.sure.setEnable(true);
		if(this.openFlag || this.doing){
			return;
		}
		ll.run.checkVisible(false);
		this.doing = true;
		var fadein = cc.fadeIn(0.4);
		ll.tip.scoreLabel.setVisible(false);
		var func = cc.callFunc(function(){			
			this.openFlag = true;
			this.doing = false;
			var tag = gg.flow.flow.tag;
			if(tag instanceof Array){
				if(TAG_LIB_MIN < tag[1]){
					// 显示箭头
					gg.flow.location();
				}
			}
		}, this);
		var seq = cc.sequence(fadein,func);
		this.runAction(seq);
	},
	close:function(){
		if(!this.openFlag || this.doing){
			return;
		}
		if(!this.doright){
			ll.run.checkVisible(true);
		}		
		this.doing = true;
		var fadeout = cc.fadeOut(0.4);
		if(gg.teach_type == TAG_REAL){
			ll.tip.scoreLabel.setVisible(true);
		}		
		var func = cc.callFunc(function(){
			this.openFlag = false;
			this.doing = false;
			var tag = gg.flow.flow.tag;
			if(tag instanceof Array){
				if(TAG_LIB_MIN < tag[1]){
					// 隐藏箭头
					//ll.tip.arr.out();
					ll.tip.arr.pos(ll.tool.getChildByTag(TAG_BUTTON_LIB));
				}
			}
		}, this);
		var seq = cc.sequence(fadeout,func);
		this.runAction(seq);
		if(ll.run.getChildByTag(TAG_SHOW) !== null){
			ll.run.getChildByTag(TAG_SHOW).fadein();
		}
		if(ll.run.getChildByTag(TAG_SHOW1) !== null){
			ll.run.getChildByTag(TAG_SHOW1).fadein();
		}
		if(ll.run.getChildByTag(TAG_SHOW2) !== null){
			ll.run.getChildByTag(TAG_SHOW2).fadein();
		}
	},
	setUnEnable:function(){
		for(var i in this.checkArr){//错误数组
			if(this.checkArr[i] != null){
				this.checkArr[i].setEnable(false);
			}

		}
		for(var i in this.rightArr){////正确数组
			if(this.rightArr[i] != null){
				this.rightArr[i].setEnable(false);
			}
		}		
	}
});
