
var MyPageView = ccui.PageView.extend({
	_pages:[],
	ctor: function () {
		this._super();
	},
//	_doLayoutDirty:false,
//	addPage: function (page) {
//		if (!page || this.pages.indexOf(page) !== -1)
//			return;
//
//		this.addChild(page);
//		this.pages.push(page);
//		//this._doLayoutDirty = true;
//	},
//	getPages:function(){
//		return this.pages;
//	},
//	scrollToPage: function (idx) {
//		this.pages = this.getPages();
//		cc.log("this._autoScrollDirection:" +this._autoScrollDistance);
//		cc.log("this._autoScrollDirection:" +this._autoScrollSpeed);
//		cc.log("this._autoScrollDirection:" +this._autoScrollDirection);
//		cc.log("this._autoScrollDirection:" +this._curPageIdx);
//		cc.log("_pageslength:" +this.pages.length);
//		if (idx < 0 || idx >= this.pages.length)
//			return;
//		this._curPageIdx = idx;
//		cc.log(idx);
//		var curPage = this.pages[idx];
//		this._autoScrollDistance = -(curPage.getPosition().x);
//		this._autoScrollSpeed = Math.abs(this._autoScrollDistance) / 0.5;
//		this._autoScrollDirection = this._autoScrollDistance > 0 ? 1 : 0;
//
//		cc.log("this._autoScrollDirection:" +this.isAutoScrolling);
//		this._isAutoScrolling = true;
//		cc.log("this._autoScrollDirection:" +this._isAutoScrolling);
//	},
//	scrollToPage: function (idx) {
//		this._super();
//		this.pages = this.getPages();
//		cc.log("_pageslength:" +this.pages.length);
//		if (idx < 0 || idx >= this.pages.length)
//			return;
//		this._curPageIdx = idx;
//		cc.log(idx);
//		var curPage = this.pages[idx];
//		this._autoScrollDistance = -(curPage.getPosition().x);
//		this._autoScrollSpeed = Math.abs(this._autoScrollDistance) / 0.5;
//		this._isAutoScrolling = true;
//	},
	_handleReleaseLogic: function (touchPoint) {
		if (this.pages.length <= 0)
			return;
		var curPage = this.pages[this._curPageIdx];
		if (curPage) {
			var curPagePos = curPage.getPosition();
			var pageCount = this.pages.length;
			var curPageLocation = curPagePos.x;
			var pageWidth = this.getSize().width;
			if (!this._usingCustomScrollThreshold)
				var boundary = pageWidth / 10.0;
			if (curPageLocation <= -boundary) {
				if (this._curPageIdx >= pageCount - 1)
					this._scrollPages(-curPageLocation);
				else
					this.scrollToPage(this._curPageIdx + 1);
			} else if (curPageLocation >= boundary) {
				if (this._curPageIdx <= 0)
					this._scrollPages(-curPageLocation);
				else
					this.scrollToPage(this._curPageIdx - 1);
			} else
				this.scrollToPage(this._curPageIdx);
		}
	},
	

})