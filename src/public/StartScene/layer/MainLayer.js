TAG_YINDAO = 1001;
TAG_SHIZHAN = 1002;
TAG_QUWEI = 1003;
TAG_QITA = 1004;
TAG_ABOUT = 1401;
TAG_TEST = 1402;
TAG_LIANX = 1403;
TAG_YUANL = 1404;
TAG_MUDE = 1405;
TAG_DTL = 1406;
TAG_PLATFORM = 1407;
TAG_WIN=1408;

var StartMainLayer = cc.Layer.extend({
	lead:null,
	real:null,
	title:null,
	YuanLi:null,
	MuDi:null,
	menulength:null,
	ctor:function () {
		this._super();
		// 加载回退按钮
		this.loadButton();

		// 加载标题
		this.loadTitle();
		//加载左侧图片
		this.loadimg();
		//右侧菜单
		this.loadMenu();
		//添加滚动
		this.loadSlide();
		return true;
	},
	loadTitle: function(){//标题
		var title = new cc.LabelTTF(gg.Title,gg.fontName,45);
		title.setAnchorPoint(0, 1);
		title.setPosition(52,gg.height - 160);
		this.addChild(title,5);
	},
	loadimg:function(){//左侧图片
		for(var i in expInfo){
			var exp = expInfo[i];
			if(gg.expId == exp.expId){
				var start_png = new cc.Sprite(exp.startPng);
				// 缩放
				if(start_png.width > start_png.height){
					scale = 200 / start_png.width;
				} else {
					scale = 283 / start_png.height;
				}
				start_png.setScale(scale * 1.5);
				start_png.setPosition(302, start_png.height * scale /2 + 150);
				this.addChild(start_png,10);
				break;
			}
			
		}
	},
	loadButton:function(){//返回按钮
		this.backButton = new ButtonScale(this,"#button/button_back.png", this.callback);
		this.backButton.setLocalZOrder(5);
		this.backButton.setPosition(cc.p(20 + this.backButton.width * 0.5, gg.height - 20 - this.backButton.height * 0.5));
		this.backButton.setTag(TAG_BACK);
	},
	loadMenu :function(){//右侧菜单
		this.menu = new cc.Menu();
		this.menu.setPosition(0,0);
		var prev = null;
		this.cur = 0;
		var marign = 50;
		for(var i in expInfo){
			var exp = expInfo[i];
			if(gg.expId == exp.expId){
				this.menulength = exp.menuItem.length;
				for(i=0;i<exp.menuItem.length;i++){
					var label = new cc.MenuItemLabel(
							new cc.LabelTTF(exp.menuItem[i].menu,gg.fontName,gg.menufont),
							this.callback,this);
					label.setAnchorPoint(0, 0.5);
					label.setColor(cc.color(255,255,255, 200));
					label.setTag(exp.menuItem[i].tag);
					this.menu.addChild(label, 10);				
					if(!prev){
						label.setPosition(680,520);
					} else {
						this.posDown(prev, label, marign);
					}
					prev = label;
					if(!this.cell){ 
						this.cell = label.height + marign;
					}				
				}
				break;
		    }								
		}
		//添加遮罩
		var stencil = new cc.DrawNode();
		stencil.drawRect(cc.p(650,100),cc.p(1280,568),cc.color(248,232,176,100),0,cc.color(248,232,176,100));
		var clip = new cc.ClippingNode(stencil);
		this.addChild(clip);
		clip.addChild(this.menu, 1);
	},
	posDown:function (standard, target, margin){
		if(!margin){
			margin = 0;
		}
		var sap = standard.getAnchorPoint();
		var ap = target.getAnchorPoint();
		// 标准物的y - 标准物的高度 * 缩放 * 锚点y - 本身的高度 * 缩放 * (1-锚点y) - 所需间隔
		var y = standard.y
		- standard.height * standard.getScaleY() * sap.y
		- target.height * (1-ap.y) * target.getScaleY() - margin;
		target.setPosition(standard.x, y);
	},
	callback:function(pSend){
		switch(pSend.getTag()){
		case TAG_BACK:
			cc.log("结束");
			if (cc.sys.platform == cc.sys.DESKTOP_BROWSER) {
				ch.howToGo(OP_TYPE_BACK);
			} else if(cc.sys.os == "IOS"){
			} else if(cc.sys.os == "Android"
				|| cc.sys.os == "Windows"){
				ch.howToGo(OP_TYPE_BACK);
			}
			break;
		case TAG_MENU1:
		case TAG_MENU2:
		case TAG_MENU3:
		case TAG_MENU4:
		case TAG_MENU5:
		case TAG_MENU6:
			gg.runNext(pSend.getTag());
			break;
		}
	},
	down:/**
	 * 向下滑动
	 */
		function(){
		if(this.cur >= this.menulength - 5){
			return;
		}
		this.cur++;
		this.menu.runAction(cc.moveBy(0.2,cc.p(0, this.cell)))
	},
	up:/**
	 * 向上滑动
	 */
		function(){
		if(this.cur <= 0){
			return;
		}
		this.cur--;
		this.menu.runAction(cc.moveBy(0.2,cc.p(0, -this.cell)))
	},
	loadSlide:/**
	 * 滑动
	 */
		function(){
		var listener_touch = cc.EventListener.create({
			event: cc.EventListener.TOUCH_ONE_BY_ONE,
			swallowTouches: false,
			hover: false,
			onTouchBegan:UpperLowerSliding.onTouchBegan,
			onTouchMoved:UpperLowerSliding.onTouchMoved,
			onTouchEnded:UpperLowerSliding.onTouchEnded});
		cc.eventManager.addListener(listener_touch, this);
		if ('mouse' in cc.sys.capabilities){
			var listener_mouse = cc.EventListener.create({
				event: cc.EventListener.MOUSE,
				swallowTouches: false,
				onMouseScroll:UpperLowerSliding.onMouseScroll
			});
			cc.eventManager.addListener(listener_mouse, this);
		}
	}
});
