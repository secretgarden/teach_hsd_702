var RunLayer901 = cc.Layer.extend({
	arr:null,
	scene:null,
	clock:null,
	ctor:function (parent) {
		this._super();
		this.scene = parent;
		this.scene.addChild(this, 10);
		gg.main = this;
		this.init();
	},
	init:function () {
		this.callNext = cc.callFunc(function(){
			gg.flow.next();
		}, this);
		this.callKill = cc.callFunc(function(p){
			var seq = cc.sequence(cc.fadeOut(0.5),cc.callFunc(function(){
				p.removeFromParent(true);	
			}, this));
			p.runAction(seq);
		}, this); 

		this.callNext.retain();
		this.callKill.retain();
		//时钟
		this.clock = new Clock(this);
		// 物品库
		this.lib = new Lib(this);
		this.lib.loadBg([{
			tag:TAG_LIB_AMPULLA,			
		},
		{
			tag:TAG_LIB_BEAKER,
			
		},
		{
			tag:TAG_LIB_DAOXIAN,
			checkright:true,	//实验所需器材添加checkright标签		
		},
		{
			tag:TAG_LIB_TESTTUBE,
		},
		{
			tag:TAG_LIB_YIBEI,
		},
		{
			tag:TAG_LIB_DIYADIANYUAN,
			checkright:true,
		},
		{
			tag:TAG_LIB_FLASK,
		},
		{
			tag:TAG_LIB_DIANJIEQI,
			checkright:true,
		},
		{
			tag:TAG_LIB_MATCH,
			checkright:true,			
		},
		{
			tag:TGA_LIB_GLASS,
			
		}
		]);
		//电解水实验
		this.electrolysis = new Electrolysis(this);
		//火柴
		this.match = new Match(this);
	},
	checkVisible:function(next){
		//是否可见
		var checkVisible = [];		
		var electrolysis = ll.run.getChildByTag(TAG_ELECTROLYSIS);
		var match = ll.run.getChildByTag(TAG_MATCH);
		var show = ll.run.getChildByTag(TAG_SHOW);
		checkVisible.push(electrolysis,match,show);
		//动作暂停，恢复
		var actionPause = [];
		var off = electrolysis.getChildByTag(TAG_OFF);
		var on = electrolysis.getChildByTag(TAG_ON);
		var dc_v = electrolysis.getChildByTag(TAG_BODY).getChildByTag(TAG_DC_V);
		var match1 =match.getChildByTag(TAG_MATCH1);
		var match2 =match.getChildByTag(TAG_MATCH2);
		actionPause.push(off,on,dc_v,match1,match2);

		for(var i in checkVisible){
			if(checkVisible[i] !== null){				
				checkVisible[i].setVisible(next);
				for(var i in actionPause){
					if(actionPause[i] !== null){
						if(next){
							actionPause[i].resume();
						}else{
							actionPause[i].pause();
						}
					}
				}
			}			
		}
	},
	loadInLib:function(obj, pos, tarPos,delay){
		obj.setPosition(pos);
		if(delay == null){
			delay = 1;
		}
		var ber = $.bezier(pos, tarPos, delay);
		var seq = cc.sequence(ber, this.callNext);
		obj.runAction(seq);
	},
	kill:function(obj){
		var fade = cc.fadeTo(0.5,0);
		var func = cc.callfunc(function(){
			obj.removeFromParent(true);
		},this);
		var seq = cc.sequence(fade,func);
		obj.runAction(seq)
	},

	callback:function (p){
		var func = cc.callFunc(this.actionDone, this);
		var action=gg.flow.flow.action;
		switch(p.getTag()){
		case TAG_XIANGPISAI:

			break;

		}
	},
	actionDone:function(p){
		var func = cc.callFunc(this.actionDone, this);
		switch(p.getTag()){

		}
	},
	flowNext:function(){
		gg.flow.next();
	},
	onExit:function(){
		this._super();
		this.callNext.release();
		this.callKill.release();
	}
});