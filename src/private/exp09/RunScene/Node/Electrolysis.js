/**
 * 电解水实验
 */
Electrolysis = cc.Node.extend({
	ctor:function(parent){
		this._super();
		parent.addChild(this,30);
		this.setTag(TAG_ELECTROLYSIS);
		this.init();
	},
	init : function(){
		this.setVisible(false);
		this.setCascadeOpacityEnabled(true);
		//左边玻璃管的水平面
		var line1 = new cc.Sprite("#cly_line.png");
		this.addChild(line1, 8, TAG_LINE1);
		line1.setPosition(cc.p(464,551));
		line1.setScale(0.45);
		//右边玻璃管的水平面
		var line2 = new cc.Sprite("#cly_line.png");
		this.addChild(line2, 8, TAG_LINE2);
		line2.setPosition(cc.p(572,551));
		line2.setScale(0.45);
		//实验整图
		this.body = new Button(this, 10, TAG_BODY, "#DC_body.png",this.callback);
		this.body.setPosition(cc.p(gg.width*0.6 + 50 , gg.height*0.47));
		this.body.setScale(0.9);
		//左边玻璃管尖嘴
		var left = new Button(this, 10, TAG_ELECTROLYSIS_LEFT, "#left1.png",this.callback);
		left.setPosition(cc.p(449,609));
		//右边玻璃管尖嘴
		var left = new Button(this, 10, TAG_ELECTROLYSIS_RIGHT, "#right1.png",this.callback);
		left.setPosition(cc.p(587,579));
		//开关按钮
		var dc_on_off = new Button(this.body, 10, TAG_DC_ONOFF, "#on-off.png",this.callback,this);
		dc_on_off.setPosition(cc.p(743,115));
		//电压按钮
		var dc_v = new Button(this.body, 10, TAG_DC_V, "#DC_button.png",this.callback,this);
		dc_v.setPosition(cc.p(660,125));
		dc_v.setRotation(66);
		//指示灯
		var dc_light = new Button(this.body, 10, TAG_DC_LIGHT, "#DC_green.png",this.callback,this);
		dc_light.setPosition(cc.p(805,185));
		//off开关
		var off = new cc.Sprite("#off.png");
		this.addChild(off, 20,TAG_OFF);
		off.setPosition(cc.p(1050,240));
		off.setVisible(false);
		//on开关
		var on = new cc.Sprite("#on.png");
		this.addChild(on, 20,TAG_ON);
		on.setPosition(cc.p(1050,240));
		on.setVisible(false);
		
		this.callNext = cc.callFunc(function(){
			gg.flow.next();
		}, this);
		this.callNext.retain();
	},	
	//左边玻璃管气泡
	bubble1:function(){
		var line1 = this.getChildByTag(TAG_LINE1);
		//小气泡
		var bubble1 = new cc.Sprite("#bubble1.png");
		this.addChild(bubble1, 7);
		bubble1.setPosition(cc.p(458+Math.random()*14,176));
		var move1 = cc.moveBy(0.5, 0,line1.getPositionY()-176-10*Math.random()-10);
		var fadeout1 = cc.fadeOut(0.5);
		var spa1 = cc.spawn(move1,fadeout1);
		var seq1 = cc.sequence(spa1,cc.callFunc(function() {
			bubble1.removeFromParent();
		}, this))
		bubble1.runAction(seq1);
		//大气泡
		var bubble2 = new cc.Sprite("#bubble2.png");
		this.addChild(bubble2, 7);
		bubble2.setScale(0.8);
		bubble2.setPosition(cc.p(461+Math.random()*7,176));
		var move2 = cc.moveBy(0.8, 0,line1.getPositionY()-176-10*Math.random()-10);
		var fadeout2 = cc.fadeOut(0.8);
		var spa2 = cc.spawn(move2,fadeout2);
		var seq2 = cc.sequence(spa2,cc.callFunc(function() {
			bubble2.removeFromParent();
		}, this))
		bubble2.runAction(seq2);
	},
	//右边玻璃管气泡
	bubble2:function(){
		var line2 = this.getChildByTag(TAG_LINE2);
		//小气泡
		var bubble1 = new cc.Sprite("#bubble1.png");
		this.addChild(bubble1, 7);
		bubble1.setPosition(cc.p(566+Math.random()*13,176));
		var move1 = cc.moveBy(0.7, 0,line2.getPositionY()-176-10*Math.random()-10);
		var fadeout1 = cc.fadeOut(0.7);
		var spa1 = cc.spawn(move1,fadeout1);
		var seq1 = cc.sequence(spa1,cc.callFunc(function() {
			bubble1.removeFromParent();
		}, this))
		bubble1.runAction(seq1);
		//大气泡
		var bubble2 = new cc.Sprite("#bubble2.png");
		this.addChild(bubble2, 7);
		bubble2.setScale(0.8);
		bubble2.setPosition(cc.p(569+Math.random()*7,176));
		var move2 = cc.moveBy(1, 0,line2.getPositionY()-176-10*Math.random()-10);
		var fadeout2 = cc.fadeOut(1);
		var spa2 = cc.spawn(move2,fadeout2);
		var seq2 = cc.sequence(spa2,cc.callFunc(function() {
			bubble2.removeFromParent();
		}, this))
		bubble2.runAction(seq2);
	},
	callback:function(p){
		var action = gg.flow.flow.action;
		switch(p.getTag()){
		case TAG_DC_ONOFF:
			if(action == ACTION_DO1){
				var off = this.getChildByTag(TAG_OFF);
				off.setVisible(true);
				off.runAction(cc.sequence(cc.delayTime(0.5),cc.callFunc(function() {
					off.setSpriteFrame("on.png");
					var light = this.getChildByTag(TAG_BODY).getChildByTag(TAG_DC_LIGHT);
					light.setSpriteFrame("DC_red.png");
				}, this),cc.delayTime(0.8),cc.callFunc(function() {
					gg.flow.next();
					off.removeFromParent();
				}, this)));	
			}else if(action == ACTION_DO2){
				var on = this.getChildByTag(TAG_ON);
				on.setVisible(true);
				on.runAction(cc.sequence(cc.delayTime(0.5),cc.callFunc(function() {
					on.setSpriteFrame("off.png");
					var light = this.getChildByTag(TAG_BODY).getChildByTag(TAG_DC_LIGHT);
					light.setSpriteFrame("DC_green.png");
					this.unschedule(this.bubble1);
					this.unschedule(this.bubble2);
					var line1 = this.getChildByTag(TAG_LINE1);
					line1.stopAllActions();
					var line2 = this.getChildByTag(TAG_LINE2);
					line2.stopAllActions();
				}, this),cc.delayTime(0.8),cc.callFunc(function() {
					gg.flow.next();
					on.removeFromParent();
				}, this)));	
			}			
			break;
		case TAG_DC_V:
			var turn =cc.rotateBy(1, 165);
			p.runAction(cc.sequence(turn,cc.delayTime(0.5),cc.callFunc(function() {
				var show = new ShowTip("电极上产生气泡，两支玻璃管内液面下降,\n产生的气体体积比约为2:1",cc.p(850,420));
				var line1 = this.getChildByTag(TAG_LINE1);
				line1.runAction(cc.sequence(cc.delayTime(0.5),cc.moveBy(30, 0,-351)));
				var line2 = this.getChildByTag(TAG_LINE2);
				line2.runAction(cc.sequence(cc.delayTime(0.5),cc.moveBy(30, 0,-175)));
				this.schedule(this.bubble1,0.3);
				this.schedule(this.bubble2, 0.3);
			}, this),cc.delayTime(9),this.callNext));
			break;
		}
	}
});
