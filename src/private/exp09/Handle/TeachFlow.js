startRunNext09 = function(tag){
	switch(tag){
	case TAG_YINDAO:
	case TAG_SHIZHAN:
		ch.gotoRun(TAG_EXP_09, g_resources_run09, null, new RunScene09());
		break;
	case TAG_QUWEI:
		gg.hJson = hJson09;
		ch.gotoGame(TAG_EXP_09, g_resources_public_game, res_public_game.game_p);
		break;
//		case TAG_TEST:
//		ch.gotoTest(TAG_EXP_09, g_resources_test09, null, res_test09.subject);
//		break;
	case TAG_ABOUT:
		ch.gotoAbout(TAG_EXP_09, g_resources_public_about, null, res_public_about.about_j);
		break;
	}
}
//任务流
teach_flow09 = 
	[
	 {
		 tip:"选择导线",
		 tag:[
		      TAG_LIB,
		      TAG_LIB_BG3,	   
		      ],
		      canClick:true
	 },
	 {
		 tip:"选择火柴",
		 tag:[
		      TAG_LIB,
		      TAG_LIB_BG9,
		      ],
		      canClick:true
	 },
	 {
		 tip:"选择水电解器",
		 tag:[
		      TAG_LIB,
		      TAG_LIB_BG8,
		      ],
		      canClick:true
	 },
	 {
		 tip:"选择低压直流电源",
		 tag:[
		      TAG_LIB,
		      TAG_LIB_BG6,
		      ],
		      canClick:true
	 },	
	 {
		 tip:"点击确定，开始实验",
		 tag:[
		      TAG_LIB,
		      TAG_SURE
		      ]
	 },
	 {
		 tip:"打开直流电源的开关",
		 tag:[TAG_ELECTROLYSIS,
		      TAG_BODY,
		      TAG_DC_ONOFF
		      ],action:ACTION_DO1
	 },
	 {
		 tip:"旋转按钮至12V，观察实验现象",
		 tag:[TAG_ELECTROLYSIS,
		      TAG_BODY,
		      TAG_DC_V
		      ]
	 },
	 {
		 tip:"关闭直流电源的开关",
		 tag:[TAG_ELECTROLYSIS,
		      TAG_BODY,
		      TAG_DC_ONOFF
		      ],action:ACTION_DO2
	 },
	 {
		 tip:"取出火柴点燃，并靠近左边玻璃管嘴，并分析实验",
		 tag:[TAG_MATCH,
		      TAG_BOX
		      ],action:ACTION_DO1
	 },{
		 tip:"将带火星的火柴靠近右边玻璃管嘴，并分析实验",
		 tag:[TAG_MATCH,
		      TAG_BOX
		      ],action:ACTION_DO2
	 },
	 {
		 tip:"恭喜过关",
		 over:true
	 }
	 ]
//游戏
hJson09 = [
           {tag:TAG_H1,image:"prop1.png",name:"量筒"},
           {tag:TAG_H2,image:"prop2.png",name:"胶头滴管"},
           {tag:TAG_H3,image:"prop3.png",name:"蒸发皿"},
           {tag:TAG_H4,image:"prop4.png",name:"表面皿"},
           {tag:TAG_H5,image:"prop5.png",name:"酒精灯"},
           {tag:TAG_H6,image:"prop6.png",name:"洗耳球"}
           ]





