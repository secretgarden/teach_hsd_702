exp02.ShowMainLayer03 = cc.Layer.extend({
	lead:null,
	real:null,
	curIndex: 0,
	ctor:function () {
		this._super();
		this.init();
		return true;
	},
	init:function(){
		this.pageView = new ccui.PageView();//建立pageview
		this.pageView.setSwallowTouches(false);
		this.pageView.setContentSize(cc.size(980, 600));//pageview展示框的大小
		this.pageView.setPosition(640,338);//pageview的坐标
		this.pageView.setAnchorPoint(0.5,0.5);//默认锚点是（0,0）
		this.addChild(this.pageView);
		
		this.loadLayer1();
		this.loadLayer2();
	},	
	loadLayer1:function(){
		this.layout1 = new ccui.Layout();//第一页
		this.layout1.setContentSize(cc.size(980, 600));//设置layout的大小
		this.pageView.addPage(this.layout1);

		var str = "在种子具有活力的前提下,在同时满足氧气、水分和十一的温度等条件时,种子才能发芽.种子萌发的过程是:种子吸收水分后,提及胀大,子叶里的营养物质(双子叶植物种子)或胚乳中的营养物质经过子叶(单子叶植物种子)转运给胚根、胚轴、胚芽.随后胚根发育成根,胚芽发育成茎和叶,并伸出土面,长成幼苗(deedling).";
		str = $.format(str, 900, gg.fontSize4);
		this.label1 = new cc.LabelTTF(str, gg.fontName, gg.fontSize4);
		this.label1.setPosition(this.layout1.width/2,this.layout1.height-this.label1.height/2);
		this.layout1.addChild(this.label1);	
	},
	loadLayer2:function(){
		this.layout2 = new ccui.Layout();//第一页
		this.layout2.setContentSize(cc.size(980, 600));//设置layout的大小
		this.pageView.addPage(this.layout2);

		var str = "幼苗形成后,植物体的营养器官不断生长,植株长高长大,逐渐进入花器官形成的阶段.从种子萌发到开花所经历的时间长短,不同的植物是不同的.番茄、睡到、玉米、黄瓜、凤仙花等一年生植物,在秋季播种,第二年春、夏季开花结果;桃、苹果、杨、柳等多年生植物(树木)的幼苗期则很长,在播种长出幼苗后,需要生长几年才能开花、结果(此后每年都能开花、结果)";
		str = $.format(str, 900, gg.fontSize4);
		this.label1 = new cc.LabelTTF(str, gg.fontName, gg.fontSize4);
		this.label1.setPosition(this.layout2.width/2,this.layout2.height-this.label1.height/2);
		this.layout2.addChild(this.label1);		
	},
	callback:function(p){
		switch(p){		
		case this.turnleft:
			var index = this.pageView.getCurPageIndex();
			this.pageView.scrollToPage(index - 1);
		    break;
		case this.turnright:
			var index = this.pageView.getCurPageIndex();
			this.pageView.scrollToPage(index + 1);
			break;
		case this.play:
			p.setSpriteFrame("sel.png");
			p.setEnable(false);
			var scale = cc.scaleTo(1,0.5);
			var scale1 = cc.scaleTo(1,0.8,0.7);
			var seq = cc.sequence(scale,scale1,cc.callFunc(function(){
				p.setSpriteFrame("unsel.png");
				p.setEnable(true);
			},this)); 
			this.hongxi.runAction(seq);
			break;
		case this.yuanyinnutton:
			p.setSpriteFrame("sel.png");
			p.setEnable(false);
			this.label4.setVisible(true);
		break;
		}
	}
	
});
