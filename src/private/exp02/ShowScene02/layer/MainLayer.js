exp02.ShowMainLayer02 = cc.Layer.extend({
	lead:null,
	real:null,
	curIndex: 0,
	ctor:function () {
		this._super();
		this.init();
		return true;
	},
	init:function(){
		this.pageView = new ccui.PageView();//建立pageview
		this.pageView.setSwallowTouches(false);
		this.pageView.setContentSize(cc.size(980, 600));//pageview展示框的大小
		this.pageView.setPosition(640,338);//pageview的坐标
		this.pageView.setAnchorPoint(0.5,0.5);//默认锚点是（0,0）
		this.addChild(this.pageView);
		
		this.loadLayer1();

	},	
	loadLayer1:function(){
		this.layout1 = new ccui.Layout();//第一页
		this.layout1.setContentSize(cc.size(980, 600));//设置layout的大小
		this.pageView.addPage(this.layout1);

		this.label1 = new cc.LabelTTF("与动物一样,种子也有一定的寿命." +
				"\n例如,在一般条件下、小麦、水稻、玉米的种子只能存货2~3年," +
				"\n白菜、蚕豆、绿豆、南瓜的种子能存活4~6年." +
				"\n但是,在低温和干燥的条件下保存种子,寿命可以延长;"+
				"\n而在高温和潮湿的条件下,寿命就会缩短"
				,gg.fontName,gg.fontSize4);
		this.label1.setPosition(this.layout1.width/2,this.layout1.height-this.label1.height/2);
		this.layout1.addChild(this.label1);	
	},
	callback:function(p){
		switch(p){		
		case this.turnleft:
			var index = this.pageView.getCurPageIndex();
			this.pageView.scrollToPage(index - 1);
		    break;
		case this.turnright:
			var index = this.pageView.getCurPageIndex();
			this.pageView.scrollToPage(index + 1);
			break;
		case this.play:
			p.setSpriteFrame("sel.png");
			p.setEnable(false);
			var scale = cc.scaleTo(1,0.5);
			var scale1 = cc.scaleTo(1,0.8,0.7);
			var seq = cc.sequence(scale,scale1,cc.callFunc(function(){
				p.setSpriteFrame("unsel.png");
				p.setEnable(true);
			},this)); 
			this.hongxi.runAction(seq);
			break;
		case this.yuanyinnutton:
			p.setSpriteFrame("sel.png");
			p.setEnable(false);
			this.label4.setVisible(true);
		break;
		}
	}
	
});
