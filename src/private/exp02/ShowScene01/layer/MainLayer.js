exp02.ShowMainLayer01 = cc.Layer.extend({
	lead:null,
	real:null,
	ctor:function () {
		this._super();
		this.init();
		this.turnmenu();
		this.scheduleUpdate();
		return true;
	},
	init:function(){//内容放在一个980*600的框里面，左下角的坐标（150,38） ，右上角坐标（1130,638）
		this.pageView = new ccui.PageView();//建立pageview
		this.pageView.setSwallowTouches(false);
		this.pageView.setContentSize(cc.size(980, 600));//pageview展示框的大小
		this.pageView.setPosition(640,338);//pageview的坐标
		this.pageView.setAnchorPoint(0.5,0.5);//默认锚点是（0,0）
		this.addChild(this.pageView);	
		
		gg.teach_flow01 = exp02.teach_flow01;
		this.loadLayer1();
		this.loadLayer2();
	},	
	loadLayer1:function(){
		this.layout1 = new ccui.Layout();//第一页
		this.layout1.setContentSize(cc.size(980, 600));//设置layout的大小
		this.pageView.addPage(this.layout1);

		this.label1 = new cc.LabelTTF("菜豆种子的结构",gg.fontName,gg.fontSize4);
		this.label1.setPosition(this.layout1.width/2,this.layout1.height-this.label1.height/2);
		this.layout1.addChild(this.label1);	
		
		var rl = new exp02.RunLayer01(this.layout1);
		rl.setScale(0.8);
		rl.setPosition(-200, -150);
	},
	loadLayer2:function(){
		this.layout2 = new ccui.Layout();//第一页
		this.layout2.setContentSize(cc.size(980, 600));//设置layout的大小
		this.pageView.addPage(this.layout2);

		this.label1 = new cc.LabelTTF("玉米种子的结构",gg.fontName,gg.fontSize4);
		this.label1.setPosition(this.layout2.width/2,this.layout2.height-this.label1.height/2);
		this.layout2.addChild(this.label1);	
		
		var rl = new exp02.RunLayer02(this.layout2);
		rl.setScale(0.8);
		rl.setPosition(-200, -150);
	},
	doaction:function(){//开始各参数动作集合
		this.doaction1(this.daqiya,3);
		this.doaction2(this.label1,1.5);
	},
	doaction1:function(obj,time){//图片旋转 放大
		var sca = cc.scaleTo(time,1);
		var rota = cc.rotateTo(time,720);
		var sp = cc.spawn(sca,rota);
		obj.runAction(sp);
	},
	doaction2:function(obj,time){//文字
		var scale = cc.scaleTo(time,0.7);
		var scale1 = cc.scaleTo(time,1);
		var seq = cc.sequence(scale,scale1);
		obj.runAction(seq);
	},
	turnmenu:function(){
		this.turnleft = new ButtonScale(this,res_public.leftbutton,this.callback);
		this.turnleft.setPosition(80,376-50);
		this.turnleft.setVisible(false);       
		this.turnright = new ButtonScale(this,res_public.rightbutton,this.callback);
		this.turnright.setPosition(1200,376-50);   	    	
	},
	update:function(){//用pageview翻页暂时没找到好的更新方法
		var index = this.pageView.getCurPageIndex();//获取当前页，以0 1 2 形式
		if(this.curIndex != index){
			if(index == 0){//当从第二页滑到第一页时，继续左滑，左滑按钮隐藏（滑到第一页）
				this.turnleft.setVisible(false);
				this.turnright.setVisible(true);
			}else if(index == 1){//当从第倒数第二页滑到最后页时，继续右滑，右滑按钮隐藏，根据自己创建的页数，手动调整（滑到最后页）
				this.turnleft.setVisible(true);
				this.turnright.setVisible(false);
			}else{
				this.turnleft.setVisible(true);
				this.turnright.setVisible(true);
			}  
			this.curIndex = index;
		}			
	},
	callback:function(p){
		switch(p){		
		case this.turnleft:
			var index = this.pageView.getCurPageIndex();
			this.pageView.scrollToPage(index - 1);
			break;
		case this.turnright:
			var index = this.pageView.getCurPageIndex();			
			this.pageView.scrollToPage(index + 1);
			break;
		}
	}
});
