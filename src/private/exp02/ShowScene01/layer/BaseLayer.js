/**
 * 基础类,处理公共方法,以及纹理等
 */
exp02.BaseLayer01 = cc.Layer.extend({
	ctor:function (parent) {
		this._super();
		parent.addChild(this, 10);
	},
	init:function () {
		cc.log("BaseLayer初始化");
		ll.run = this;
		this.callNext = cc.callFunc(function(){
			gg.flow.next();
		}, this);
		this.callKill = cc.callFunc(function(p){
			var seq = cc.sequence(cc.fadeOut(0.5),cc.callFunc(function(){
				p.removeFromParent(true);	
			}, this));
			p.runAction(seq);
		}, this);
		this.callNext.retain();
		this.callKill.retain();

		this.clock = new Clock(this);
		this.loadFrame();
	},
	//按钮中的文字
	loadName:function(name,pare){
		var str = new cc.LabelTTF(name,gg.fontName,gg.fontSize9);
		pare.addChild(str, 2);
		str.setColor(cc.color(255,255,255));
		str.setPosition(cc.p(pare.width*0.5, pare.height*0.5));
	},
	//右边名称框
	loadFrame:function(){
		var frame = new cc.Sprite("#help/nameFrame.png");
		this.addChild(frame, 2);
		frame.setPosition(cc.p(gg.width - 20 - frame.width*0.5,gg.height - 126 - frame.height*0.5));

		var frameTitle = new cc.LabelTTF("选择结构名称",gg.fontName,gg.fontSize3) ;
		frame.addChild(frameTitle, 3);
		frameTitle.setPosition(cc.p(frame.width*0.5 , frame.height - 10 - frameTitle.height*0.5));
	},
	//右侧框中的部位名称
	loadTipFrame:function(){
		this.frameName = [];
		this.labelNode = new cc.Node();
		this.addChild(this.labelNode);
		var first = null;
		for(var i in this.frameInfo){
			var info = this.frameInfo[i];
			this.label = new Label(this.labelNode, info.name,this.callback,this);
			this.label.setFontSize(gg.fontSize3);
			this.label.setTag(info.tag);
			this.label.setAnchorPoint(0.5, 1);
			this.label.setColor(cc.color(102,102,102));
			if(!!first){
				if(i == 1){
					//第二个名称坐标（右边）
					this.label.setPosition(gg.width - 90 , gg.height - 198 );	
					second = this.label;
				}else if(i>=2 && i%2 == 0){
					//第三个起，单数在左边名称下面
					this.label.down(first, 40);
					first = this.label;
				}else if(i>=2 && i%2 == 1){
					//第三个起，偶数在右边名称下面
					this.label.down(second, 40);
					second = this.label;
				}
			} else {
				//第一个名称坐标（左边）
				this.label.setPosition(gg.width - 235 , gg.height - 198 );
				first = this.label;							
			}	
			this.frameName.push(this.label);
		}		
	},
	//实战模式中，随机取值闪烁
	loadFlash:function(){
		this.no = 0;
//		for(var i in this.frame){
		for(var i = 0 ; i < this.frame.length; i++){
			if(this.frame[i].finish){
				this.no++;
				//数组中删除第i个起，1个
				this.frame.splice(i, 1);
				//数组中最前端，添加null,保持数组长度
				this.frame.unshift(null);			
			}
		}
		//添加几次null,删除
		if(this.no != 0 ){
			this.frame.splice(0,this.no);
		}
		//闪烁点击正确后，从数组中删除，为0时gg.next，否则实验未结束
		if(this.frame.length == 0){			
			return;
		}else{
			var i = parseInt(Math.random()*this.frame.length);
			this.curSel = this.body.getChildByTag(this.frame[i].tag);
			this.flash(this.curSel);
		}
	},
	//实战中，点击过的，增加名称（finish）
	loadWord:function(){
		for(var i in this.frameInfo){
			var info = this.frameInfo[i];
			if(info.finish){
				this.expWord(info);				
			}
		}
	},
	//实验具体部位的名称（图中注释）
	expWord:function(info){
		//切图中具体部位的名称
		this.ans = new cc.LabelTTF(info.name,gg.fontName,gg.fontSize2);
		this.addChild(this.ans,3);
		this.ans.setOpacity(255);
		this.ans.setPosition(info.posx,info.posy);	
		//名称的连接线
		var line = new cc.Sprite("#help/line.png");
		this.ans.addChild(line,3);		
		if(info.scale == null){
			info.scale = 1.5;
		}
		line.setScale(info.scale ,2);	
		if(info.next){
			//标签true，文字在左，线条在右
			line.setAnchorPoint(0,0.5);
			line.setPosition(cc.p( this.ans.width+10,this.ans.height*0.5));
		}else{
			line.setAnchorPoint(1,0.5);
			line.setPosition(cc.p(0-5,this.ans.height*0.5));
		}
	},
	//点击出名称
	showWord:function(p){
		for(var i in this.frameInfo){
			var info = this.frameInfo[i];
			if(info.tag == p.getTag()){
				if(gg.teach_type == TAG_LEAD){
					//引导模式下增加部位的作用解释，上一个解释remove
					if(this.ans_tip){
						this.ans_tip.removeFromParent();
					}
					//切图中具体部位的作用解释
					this.ans_tip = new cc.LabelTTF(info.st,gg.fontName,gg.fontSize1);
					this.addChild(this.ans_tip,3);
					this.ans_tip.setOpacity(255);
					this.ans_tip.setAnchorPoint(0, 1);
					this.ans_tip.setPosition(228, gg.height - 24);	
					//引导中，点击后，显示名称，上一个名称remove
					if(this.ans){
						this.ans.removeFromParent();
					}				
				}
				this.expWord(info);
				info.finish = true;
			}
		}
	},
	clickCheck:function(p){	
		//引导下，点击后，右侧名称变亮，图片闪烁，并出文字，出作用解释
		if(gg.teach_type == TAG_LEAD){
			//引导中，上一个颜色还原
			if(this.before){
				this.before.setColor(cc.color(102, 102, 102));
			}
			//上一个停止闪烁
			if(!!this.curSel){
				this.stop(this.curSel);
			}
			//当前点击变白色
			p.setColor(cc.color(255,255,255));
			for(var i in this.frameInfo){
				var info = this.frameInfo[i];
				if(info.tag == p.getTag()){
					this.curSel = this.body.getChildByTag(info.tag);
					this.flash(this.curSel);
					this.showWord(p);
					_.clever();	
				}
			}
			this.before = p;
			//实战下，点击后，点对文字变亮，并出名称，继续随机下一个闪烁
		}else if(gg.teach_type == TAG_REAL){
			for(var i in this.frameName){
				var frameName = this.frameName[i];
				//闪烁和右侧名称对应（找正确答案），变亮和点击的对应（判断操作是否正确）
				if(this.curSel.getTag() == frameName.getTag() && p.getTag() != this.curSel.getTag()){
					//右边判断不相等，为错，在正确答案上（左边判断）增加红色横线，并不可点击
					var wrong = new cc.Sprite("#help/line.png");
					frameName.addChild(wrong);
					wrong.setAnchorPoint(0, 1);
					wrong.setScale(frameName.width/wrong.width,2);
					wrong.setPosition(cc.p(0,frameName.height*0.5));					
					frameName.setEnable(false);
					frameName.doRight = false;
					ll.tip.mdScore(-3);	
					//并从部位数组中删除
					for(var j in this.frame){					
						var info2 = this.frame[j];
						//闪烁和部位对应
						if(this.curSel.getTag() == info2.tag){	
							this.showWord(this.curSel);
							//数组中删除
							this.frame.splice(j, 1);					
						}
					}				
				}
			}
			for(var i in this.frameInfo){
				var info = this.frameInfo[i];
				for(var j in this.frame){					
					var info2 = this.frame[j];
					if(info.tag == info2.tag){
						//点击和闪烁符合，点击和右侧文字符合
						if(this.curSel.getTag() == p.getTag() && info.tag == p.getTag()){							
							this.showWord(p);
							info.doRight = true;
							ll.tip.mdScore(10);								
							p.setColor(cc.color(255,255,255));
							p.setEnable(false);
							//点击正确后，数组中删除，继续随机一个部位闪烁
							this.frame.splice(j, 1);														
						}
					}
				}								
			}
			this.stop(this.curSel);
			this.loadFlash();
		}		
		this.choseNext(p,info);		
	},
	//通关，当前关已完成，true
	choseNext:function(p,info){
		if(this.checkOver(p)){
			if(info.ex == 1){
				gg.succeed_caidou = true;
			}else if(info.ex == 2){
				gg.succeed_yumi = true;
			}else if(info.ex == 3){
				gg.succeed_hua = true;
			}else if(info.ex == 4){
				gg.succeed_beizi = true;
			}			
		}
	},
	//检查通关
	checkOver:function(p){
		for(var i in this.frameInfo){
			var info = this.frameInfo[i];
			if(!info.finish){
				return false;
			}
		}
		return true;
	},
	buttoncallback:function (p){
		switch (p.getTag()) {
		case TAG_BUTTON_CAIDOU:			
			ll.run.removeFromParent(true);
			ll.run = new RunLayer_01_1(ll.main);
			break;
		case TAG_BUTTON_YUMI:	
			ll.run.removeFromParent(true);
			ll.run = new RunLayer_01_2(ll.main);
			break;
		case TAG_BUTTON_HUA:	
			ll.run.removeFromParent(true);
			ll.run = new RunLayer_01_3(ll.main);
			break;
		case TAG_BUTTON_BEIZI:
			ll.run.removeFromParent(true);
			ll.run = new RunLayer_01_4(ll.main);
			break;
		default:
			break;
		}
	},
	flash:function (bt){
		var fade1 = cc.fadeTo(0.3, 50);
		var fade2 = cc.fadeTo(0.3, 255);
		var seq = cc.sequence(fade1,fade2);
		var flash = cc.repeatForever(seq);
		bt.runAction(flash);
	},
	stop:function (bt){
		bt.stopAllActions();
		bt.setOpacity(1);
	},
	//销毁
	destroy:function(){
		this.callNext.release();	
		this.callKill.release();
		this.removeFromParent();	
	},
});