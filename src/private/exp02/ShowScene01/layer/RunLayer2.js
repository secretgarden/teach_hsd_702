exp02.RunLayer02 = exp02.BaseLayer01.extend({
	frame:[],
	num:0,
	no:0,
	curSel:null,
	ctor:function (parent) {
		this._super(parent,5);
		this.init();
	},
	init:function () {
		cc.log("玉米种子的结构初始化");
		this._super();	
		this.loadInfo();
		//加载主图（玉米）
		this.loadMain();
		if(gg.teach_type == TAG_REAL){
			//实战模式下，随机一个部位闪烁
			this.loadFlash();
			this.loadWord();
		}		
	},
	loadInfo:function(){
		for(var i in gg.teach_flow01){
			var info = gg.teach_flow01[i];
			if(info.tag == TAG_YUMI){
				//读取Flow里的任务流
				this.frameInfo = info.frameInfo;
				//复制一个数组，做实战中删除用
				this.frame = this.frameInfo.concat();
			}
		}
	},
	loadMain:function(){
		//玉米种子外观图
		this.body1 = new cc.Sprite("#yumi/body1.png");
		this.body1.setPosition(cc.p(gg.width*0.25,gg.height*0.52));
		this.body1.setScale(0.5);
		this.addChild(this.body1);
		var zongqie = new cc.LabelTTF("纵切方向",gg.fontName,gg.fontSize3);
		this.addChild(zongqie);
		zongqie.setPosition(cc.p(gg.width*0.3+20,gg.height*0.735));
		//切图
		this.body = new cc.Sprite("#yumi/body2.png");
		this.body.setPosition(cc.p(gg.width*0.53,gg.height*0.55));
		this.body.setScale(0.5);
		if(gg.teach_type == TAG_REAL){
			this.body.setOpacity(50);
			this.body1.setOpacity(50);
		}else {
			this.body.setOpacity(255);
			this.body1.setOpacity(255);
		}
		this.addChild(this.body);
		var bw = this.body.width;
		var bh = this.body.height;
		for(var i in this.frameInfo){
			var info = this.frameInfo[i];	
			var bt = new Button(this.body,10,info.tag,info.img,this.callback,this);
			bt.setPosition(cc.p(info.px, info.py));
			bt.setAnchorPoint(0,1);
			bt.setOpacity(1);
			bt.info = info;
			this.frame = this.frameInfo.concat();
		}
		//加载名称
		this.loadTipFrame();
	},
	callback:function (p){
		this.body.setOpacity(50);
		this.body1.setOpacity(50);
		switch (p.getTag()) {	
		case TAG_YUMI_PEIGEN:
		case TAG_YUMI_PEIYA:
		case TAG_YUMI_PEIZHOU:
		case TAG_YUMI_ZIYE:	
		case TAG_YUMI_ZHONGQI:
		case TAG_YUMI_ZHONGPI:	
			this.clickCheck(p);
			break;
		default:
			break;
		}
	},
	destroy:function(){
		this._super();
	},
	onExit:function(){
		this._super();
	}
});

