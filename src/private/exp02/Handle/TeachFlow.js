startRunNext02 = function(tag){
	switch(tag){//具体哪个菜单执行什么操作，这里修改
	case TAG_MENU1:
		ch.gotoShow(TAG_EXP_02, g_resources_show02, null, new exp02.ShowScene01(tag));
		break;
	case TAG_MENU2:
		ch.gotoRun(TAG_EXP_02, g_resources_show02, null, new exp02.ShowScene02(tag));
		break;
	case TAG_MENU3:
		ch.gotoShow(TAG_EXP_02, g_resources_show02, null, new exp02.ShowScene03(tag));
		break;
		gotoShow
	case TAG_ABOUT:
		ch.gotoAbout(TAG_EXP_02, g_resources_public_about, null, res_public_about.about_j);
		break;
	}
}

// 任务流
exp02.teach_flow01 = 
	[
        {
        	tip:"菜豆种子的结构",
        	tag:TAG_CAIDOU,
        	create:[],
        	destroy:[],
        	node:true,
        	frameInfo : [
        	             {ex:1,name:"种脐",tag:TAG_HUADOU_ZHONGQI,px:-262,py:240,img:"#huadou/zhongqi.png",st:"",posx:504,posy:361,next:false},
        	             {ex:1,name:"胚芽",tag:TAG_HUADOU_PEIYA,px:40,py:386,img:"#huadou/peiya.png",st:"胚包括胚芽、胚轴、胚根、子叶，是将来发育成幼苗的结构基础",posx:807,posy:488,next:false,pei:true,scale:2.5},
        	             {ex:1,name:"胚轴",tag:TAG_HUADOU_PEIZHOU,px:160,py:381,img:"#huadou/peizhou.png",st:"胚包括胚芽、胚轴、胚根、子叶，是将来发育成幼苗的结构基础",posx:807,posy:452,next:false,pei:true},
        	             {ex:1,name:"胚根",tag:TAG_HUADOU_PEIGEN,px:168,py:267,img:"#huadou/peigen.png",st:"胚包括胚芽、胚轴、胚根、子叶，是将来发育成幼苗的结构基础",posx:807,posy:401,next:false,pei:true},
        	             {ex:1,name:"子叶",tag:TAG_HUADOU_ZIYE,px:0,py:418,img:"#huadou/ziye.png",st:"胚包括胚芽、胚轴、胚根、子叶，是将来发育成幼苗的结构基础",posx:807,posy:336,next:false,pei:true},
        	             {ex:1,name:"种皮",tag:TAG_HUADOU_ZHONGPI,px:0,py:418,img:"#huadou/zhongpi.png",st:"",posx:807,posy:247,next:false},
        	             ],
        	             nodeName:"菜豆种子的结构"
        },
        {
        	tip:"玉米种子的结构",
        	tag:TAG_YUMI,
        	create:[],
        	destroy:[],
        	node:true,
        	frameInfo : [
        	             {ex:2,name:"子叶",tag:TAG_YUMI_ZIYE,px:80,py:700,img:"#yumi/ziye.png",st:"胚包括胚芽、胚轴、胚根、子叶，是将来发育成幼苗的结构基础",posx:815,posy:490,next:false,pei:true,scale:2},   
        	             {ex:2,name:"胚芽",tag:TAG_YUMI_PEIYA,px:238,py:519,img:"#yumi/peiya.png",st:"胚包括胚芽、胚轴、胚根、子叶，是将来发育成幼苗的结构基础",posx:815,posy:457,next:false,pei:true},
        	             {ex:2,name:"胚轴",tag:TAG_YUMI_PEIZHOU,px:188,py:473,img:"#yumi/peizhou.png",st:"胚包括胚芽、胚轴、胚根、子叶，是将来发育成幼苗的结构基础",posx:815,posy:392,pei:true,next:false},
        	             {ex:2,name:"胚根",tag:TAG_YUMI_PEIGEN,px:187,py:271,img:"#yumi/peigen.png",st:"胚包括胚芽、胚轴、胚根、子叶，是将来发育成幼苗的结构基础",posx:815,posy:341,pei:true,next:false},
        	             {ex:2,name:"胚乳",tag:TAG_YUMI_ZHONGQI,px:25,py:783,img:"#yumi/peiru.png",st:"子叶或胚乳中的营养物质是胚发育需要的营养物质的来源",posx:815,posy:538,next:false,scale:3},
        	             {ex:2,name:"种皮和果皮",tag:TAG_YUMI_ZHONGPI,px:25,py:809,img:"#yumi/zhongpi.png",st:"",posx:845,posy:615,next:false}
        	             ],
        	             nodeName:"玉米种子的结构"
        },
        {
        	tip:"恭喜过关",
        	over:true

        }
    ]

