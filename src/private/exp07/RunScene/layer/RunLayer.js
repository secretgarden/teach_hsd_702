var RunLayer701 = cc.Layer.extend({
	arr:null,
	scene:null,
	clock:null,
	showRed:false,
	showGreen:false,
	ctor:function (parent) {
		this._super();
		this.scene = parent;
		// 完成实验后 发现整体布局偏上,,所以把整个Layer下移
		this.setPosition(0, -100);
		this.scene.addChild(this, 10);
		gg.main = this;
		this.init();
	},
	init:function () {
		this.callNext = cc.callFunc(function(){
			gg.flow.next();
		}, this);
		this.callKill = cc.callFunc(function(p){
			var seq = cc.sequence(cc.fadeOut(0.5),cc.callFunc(function(){
				p.removeFromParent(true);	
			}, this));
			p.runAction(seq);
		}, this); 

		this.callNext.retain();
		this.callKill.retain();
		// 时钟
		this.clock = new Clock(this);
		
		// 神经系统
		var sp3 = new cc.Sprite("#3.png");
		sp3.setPosition(400, 384);
		this.addChild(sp3);
		
		this.sp5 = new cc.Sprite("#5.png");
		this.sp5.setPosition(820, 380);
		this.addChild(this.sp5);
		// 纹路,痛觉热觉感受器
		this.lines = new cc.Sprite("#7.png");
		this.lines.setPosition(130, 40);
		this.lines.setTag(TAG_LINES);
		this.sp5.addChild(this.lines);
		this.lines.setOpacity(0);
		
		this.loadRed();
		this.loadGreen();
		
		gg.d = new cc.DrawNode();
		this.addChild(gg.d);
		
		// 开水
		this.water = new Button(this, 10, TAG_WATER, "#water.png", this.callback);
		this.water.setPosition(1000, 300);
		
		this.dn = new cc.DrawNode();
		this.addChild(this.dn);
		
		this.drawTip("脊髓", cc.p(200,400), cc.p(400,400));
		
		this.scheduleUpdate();
	},
	callback:function (p){
		var func = cc.callFunc(this.actionDone, this);
		var action=gg.flow.flow.action;
		switch(p.getTag()){
		case TAG_WATER:
			var seq = cc.sequence(
				cc.moveTo(0.5, cc.p(860, 315)),func
			);
			this.water.runAction(seq);
			break;
		}
	},
	actionDone:function(p){
		var func = cc.callFunc(this.actionDone, this);
		switch(p.getTag()){
		case TAG_WATER:
			var seq = cc.sequence(
				cc.fadeIn(0.5),func
			);
			this.lines.runAction(seq);
			break;
		case TAG_LINES:
			var wp = this.lines.convertToWorldSpace();
			wp = cc.p(wp.x + this.lines.width * 0.5, wp.y + this.lines.height * 0.5 - this.getPositionY());
			this.curWp = wp;
			this.drawTip("痛觉(或热觉)\n  感受器", cc.p(1000,400), wp);
			this.showRed = true;
			break;
		}
	},
	drawTip:function(str, from, to, color, ap){
		var tip = new cc.LabelTTF(str, gg.fontName, 25);
		tip.setPosition(from);
		if(ap){
			tip.setAnchorPoint(ap);
		}
		if(!color){
			color = cc.color(0,0,0);
		}
		this.addChild(tip);
		this.dn.drawSegment(tip.getPosition(), to, 1, color);
	},
	update:function(){
		this.drawRed();
		this.drawGreen();
	},
	loadRed:function(){
		// 传入神经路线
		this.red = new cc.Sprite("#red_line.png");
		this.red.setPosition(650, 390);
		//增加遮罩
		this.stencilRed = new cc.DrawNode();
		this.stencilRed.drawRect(cc.p(880,300),cc.p(881,500),cc.color(255,255,255,0),1,cc.color(255,255,255,0));
		var clipping = new cc.ClippingNode(this.stencilRed);
		this.addChild(clipping);
		clipping.addChild(this.red);
	},
	redStX:880,
	drawRed:function(){
		// 画红线
		if(this.showRed){
			this.redStX -= 2;
			this.stencilRed.drawRect(cc.p(this.redStX,300),cc.p(881,500),cc.color(255,255,255,0),1,cc.color(255,255,255,0));
			if(this.redStX < 400){
				this.showRed = false;
				this.drawTip(" 至脑部产生痛觉", cc.p(420, 600), cc.p(420, 438), cc.color(40, 164, 199), cc.p(0, 0.5));
				var seq = cc.sequence(cc.delayTime(0.5),
						cc.callFunc(function(){
							this.drawTip("脑部做出反应 ", cc.p(410, 600), cc.p(410, 438), cc.color(40, 164, 199), cc.p(1, 0.5));
							this.showGreen = true;
						},this));
				this.dn.runAction(seq);
			}
			if(this.redStX == 650){
				this.drawTip("传入神经", cc.p(650, 200), cc.p(650, 418));
			}
		}
	},
	loadGreen:function(){
		// 传出神经路线
		var green = new cc.Sprite("#green_line.png");
		green.setPosition(645, 392);
		//增加遮罩
		this.stencilGreen = new cc.DrawNode();
		this.stencilGreen.drawRect(cc.p(400,300),cc.p(405,500),cc.color(255,255,255,0),1,cc.color(255,255,255,0));
		var clipping = new cc.ClippingNode(this.stencilGreen);
		this.addChild(clipping);
		clipping.addChild(green);
	},
	greenStX:404,
	drawGreen:function(){
		if(this.showGreen){
			// 画绿线
			this.greenStX += 2;
			this.stencilGreen.drawRect(cc.p(400,300),cc.p(this.greenStX,500),cc.color(255,255,255,0),1,cc.color(255,255,255,0));
			if(this.greenStX > 770){
				this.showGreen = false;
				this.drawTip("效应器", cc.p(800, 550), cc.p(770, 380));
				// 缩手反应
				this.drawTip("缩手", cc.p(950,550), cc.p(800, 550));
				this.red.setSpriteFrame("red_line2.png");
				this.lines.setPosition(115, 95);
				this.lines.setRotation(-40);
				this.sp5.setSpriteFrame("6.png");
				// 修改痛觉神经路线
				var wp = this.lines.convertToWorldSpace();
				wp = cc.p(wp.x + this.lines.width * 0.5, wp.y + this.lines.height * 0.5 - this.getPositionY());
				this.dn.drawSegment(this.curWp, wp, 1, cc.color(0,0,0));
			}
			if(this.greenStX == 550){
				this.drawTip("传出神经", cc.p(550, 550), cc.p(550, 415));
			}
		}
	},
	kill:function(obj){
		var fade = cc.fadeTo(0.5,0);
		var func = cc.callfunc(function(){
			obj.removeFromParent(true);
		},this);
		var seq = cc.sequence(fade,func);
		obj.runAction(seq)
	},
	flowNext:function(){
		gg.flow.next();
	},
	onExit:function(){
		this._super();
		this.callNext.release();
		this.callKill.release();
	}
});