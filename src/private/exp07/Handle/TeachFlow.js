startRunNext07 = function(tag){
	switch(tag){
	case TAG_YINDAO:
	case TAG_SHIZHAN:
		ch.gotoRun(TAG_EXP_07, g_resources_run07, null, new RunScene07());
		break;
	case TAG_QUWEI:
		gg.hJson = hJson07;
		ch.gotoGame(TAG_EXP_07, g_resources_public_game, res_public_game.game_p);
		break;
	case TAG_TEST:
		ch.gotoTest(TAG_EXP_07, null, null, res_test07.subject);
		cc.log("测试模式");
		break;
	case TAG_ABOUT:
		ch.gotoAbout(TAG_EXP_07, g_resources_public_about, null, res_about07.about_j);
		break;
	}
}
//任务流
teach_flow07 = 
	[
	 {
		 tip:"把热水放到手上,观察反应",
		 tag:TAG_WATER,
		 canClick:true		 
	 },
	 {
		 tip:"",
		 hidden:true,
		 over:true
	 }
	 ]
//游戏
hJson07 = [
           {tag:TAG_H1,image:"prop1.png",name:"量筒"},
           {tag:TAG_H2,image:"prop2.png",name:"胶头滴管"},
           {tag:TAG_H3,image:"prop3.png",name:"蒸发皿"},
           {tag:TAG_H4,image:"prop4.png",name:"表面皿"},
           {tag:TAG_H5,image:"prop5.png",name:"酒精灯"},
           {tag:TAG_H6,image:"prop6.png",name:"洗耳球"}
           ]





