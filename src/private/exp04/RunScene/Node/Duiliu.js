Duiliu = cc.Node.extend({
	ctor:function(p){
		this._super();
		p.addChild(this, 8, TAG_DUILIU_NODE);
		this.init();
	},
	init : function(){
		this.setCascadeOpacityEnabled(true);
		this.duiliu= new Button(this, 11, TAG_DUILIU, "#duiliu.png",this.callback);
		
		this.xianxiang= new Button(this, 10, TAG_XIANXIANG, "#xianxiang.png",this.callback);
		this.xianxiang.setPosition(400,-100);
		
		this.beaker= new Button(this, 10, TAG_BEAKER, "#beaker.png",this.callback);
		this.beaker.setPosition(650,0);
				
		this.xiangpini = new cc.Sprite("#xiangpini.png");
		this.xiangpini.setPosition(100,5);
		this.xianxiang.addChild(this.xiangpini);
		
		




	},
	
	addarrow:function(){		
		this.arrow1 = new cc.Sprite("#arrow.png");
		this.arrow1.setPosition(110,60);
		this.addChild(this.arrow1);		
		
		this.arrow3 = new cc.Sprite("#arrow.png");
		this.arrow3.setPosition(100,180);
		this.arrow3.setRotation(-30);
		this.addChild(this.arrow3);
		
		this.arrow4 = new cc.Sprite("#arrow.png");
		this.arrow4.setPosition(38,215);
		this.arrow4.setRotation(-90);
		this.addChild(this.arrow4);
		
		this.arrow5 = new cc.Sprite("#arrow.png");
		this.arrow5.setPosition(-20,190);
		this.arrow5.setRotation(-130);
		this.addChild(this.arrow5);
				
		this.arrow7 = new cc.Sprite("#arrow.png");
		this.arrow7.setPosition(-38,60);
		this.arrow7.setRotation(-180);
		this.addChild(this.arrow7);
		
		this.arrow8 = new cc.Sprite("#arrow.png");
		this.arrow8.setPosition(-15,-65);
		this.arrow8.setRotation(135);
		this.addChild(this.arrow8);
		
		this.arrow9 = new cc.Sprite("#arrow.png");
		this.arrow9.setPosition(38,-88);
		this.arrow9.setRotation(90);
		this.addChild(this.arrow9);
		
		this.arrow10 = new cc.Sprite("#arrow.png");
		this.arrow10.setPosition(95,-60);
		this.arrow10.setRotation(45);
		this.addChild(this.arrow10);

	},
	runarrow:function(time){
	    var seq = cc.sequence(cc.callFunc(function(){
	    	this.arrow1.setVisible(true);
	    	this.arrow3.setVisible(false);
	    	this.arrow4.setVisible(true);
	    	this.arrow5.setVisible(false);
	    	this.arrow7.setVisible(true);
	    	this.arrow8.setVisible(false);
	    	this.arrow9.setVisible(true);
	    	this.arrow10.setVisible(false);
	    },this),cc.delayTime(time),cc.callFunc(function(){
	    	this.arrow1.setVisible(false);
	    	this.arrow3.setVisible(true);
	    	this.arrow4.setVisible(false);
	    	this.arrow5.setVisible(true);
	    	this.arrow7.setVisible(false);
	    	this.arrow8.setVisible(true);
	    	this.arrow9.setVisible(false);
	    	this.arrow10.setVisible(true);
	    },this),cc.delayTime(time)); 	
        this.runAction(seq.repeatForever());
	},
	addyan:function(){
		this.midyan = new cc.Sprite("#mid.png");//中间
		this.midyan.setPosition(15,5);
		this.midyan.setAnchorPoint(0.5,0);
		this.midyan.setScale(0);
		this.xianxiang.addChild(this.midyan);
		var seq = cc.sequence(cc.delayTime(0),cc.spawn(cc.scaleTo(5,1),cc.moveTo(5,cc.p(30,3))));
		this.midyan.runAction(seq);

		this.rightyan = new cc.Sprite("#right.png");//右下
		this.rightyan.setPosition(110,250);
		this.rightyan.setAnchorPoint(0.5,1);
		this.rightyan.setScale(0);
		this.xianxiang.addChild(this.rightyan);
		var seq1 = cc.sequence(cc.delayTime(8),cc.scaleTo(5,1));
		this.rightyan.runAction(seq1);

		this.rightyan1 = new cc.Sprite("#right1.png");//右上
		this.rightyan1.setPosition(0,250);
		this.rightyan1.setAnchorPoint(0,0.5);
		this.rightyan1.setScale(0,1);
		this.xianxiang.addChild(this.rightyan1);
		var seq2 = cc.sequence(cc.delayTime(5),cc.scaleTo(3,1));
		this.rightyan1.runAction(seq2);

		this.leftyan = new cc.Sprite("#left.png");//左下
		this.leftyan.setPosition(-80,250);
		this.leftyan.setAnchorPoint(0.5,1);
		this.leftyan.setScale(0);
		this.xianxiang.addChild(this.leftyan);
		var seq3 = cc.sequence(cc.delayTime(8),cc.scaleTo(5,1));
		this.leftyan.runAction(seq3);

		this.leftyan1 = new cc.Sprite("#left1.png");//左上
		this.leftyan1.setPosition(30,250);
		this.leftyan1.setAnchorPoint(1,0.5);
		this.leftyan1.setScale(0,1);
		this.xianxiang.addChild(this.leftyan1);
		var seq4 = cc.sequence(cc.delayTime(5),cc.scaleTo(3,1));
		this.leftyan1.runAction(seq4);
	},
	addyan1 :function(){	
		var frames=[];
		for (i=2;i<=15;i++){
			var str ="yan/"+i+".png";
			var frame=cc.spriteFrameCache.getSpriteFrame(str);
			frames.push(frame);
		}
		var animation = new cc.Animation(frames,0.15);//负责动画序列
		var action = new cc.Animate(animation);//帧动画的动作创建



		var yan = new cc.Sprite("#yan/1.png");
		yan.setPosition(25,-45);
		yan.setAnchorPoint(0.5,0);
		yan.setScale(0.7);
		this.xianxiang.addChild(yan,1);
		var seq = cc.sequence(action,cc.fadeOut(1));
		yan.runAction(seq);
	},
	addyan2 :function(){	
		var frames=[];
		for (i=16;i<=19;i++){
			var str ="yan/"+i+".png";
			var frame=cc.spriteFrameCache.getSpriteFrame(str);
			frames.push(frame);
		}
		var animation = new cc.Animation(frames,0.3);//负责动画序列
		var action = new cc.Animate(animation);//帧动画的动作创建
		
	

		var yan = new cc.Sprite("#yan/1.png");
		yan.setPosition(25,-45);
		yan.setAnchorPoint(0.5,0);
		yan.setScale(0.7);
		this.xianxiang.addChild(yan,1);
		var seq = cc.sequence(action,cc.fadeOut(1));
		yan.runAction(seq);
	},
	addyan3 :function(){	
		var frames=[];
		for (i=20;i<=24;i++){
			var str ="yan/"+i+".png";
			var frame=cc.spriteFrameCache.getSpriteFrame(str);
			frames.push(frame);
		}
		var animation = new cc.Animation(frames,0.3);//负责动画序列
		var action = new cc.Animate(animation);//帧动画的动作创建



		var yan = new cc.Sprite("#yan/1.png");
		yan.setPosition(25,-45);
		yan.setAnchorPoint(0.5,0);
		yan.setScale(0.7);
		this.xianxiang.addChild(yan,1);
		var seq = cc.sequence(action,cc.fadeOut(1));
		yan.runAction(seq);
	},
	callback:function(p){
		var action = gg.flow.flow.action;
		var func = cc.callFunc(this.actionDone, this);	
		switch(p.getTag()){		
		case TAG_XIANXIANG:
			var move = cc.moveTo(1,cc.p(140,-100));
			var move1 = cc.moveTo(1,cc.p(470,-150));
			var seq= cc.sequence(move,cc.delayTime(0.5),cc.callFunc(function(){
				p.setSpriteFrame("xianxiang1.png");	
				var yan = this.xianxiang.getChildByTag(TAG_SHOW);
				var yan = new cc.Sprite("#yan/1.png");
				yan.setPosition(15,-5);
				yan.setAnchorPoint(0.5,0);
				yan.setScale(0.2);
				this.xianxiang.addChild(yan,1,TAG_SHOW);
				var seq = cc.sequence(cc.spawn(cc.moveTo(2,cc.p(25,-45)),cc.scaleTo(2,0.7)));
				yan.runAction(seq);
			},this),move1,cc.callFunc(function(){
				//this.xianxiang.getChildByTag(TAG_SHOW).removeFromParent(true);
				this.schedule(function(){
					this.addyan1();
				}, 1, 10000, 0);
			},this),cc.delayTime(2.2),cc.callFunc(function(){
				this.schedule(function(){
					this.addyan2();
				}, 1, 10000, 0);
			},this),cc.delayTime(1),cc.callFunc(function(){
				this.schedule(function(){
					this.addyan3();
				}, 0.8, 10000, 0);
			},this),cc.delayTime(10),cc.callFunc(this.flowNext ,this));
			p.runAction(seq);
			
			var seq1 = cc.sequence(cc.delayTime(1.5),cc.moveTo(1,cc.p(450,100)),cc.moveTo(1,cc.p(450,0)));
			this.beaker.runAction(seq1);
		break;
		
		}
	},
	flowNext:function(){
		gg.flow.next();
	},
});