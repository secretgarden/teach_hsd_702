Lamp04 = cc.Node.extend({
	ctor:function(p){
		this._super();
		p.addChild(this, 10, TAG_LAMP_NODE);
		this.init();
	},
	init : function(){
		this.setCascadeOpacityEnabled(true);
		this.lamplid= new Button(this, 11, TAG_LAMP_LID, "#lampLid.png",this.callback);
		this.lamplid.setPosition(0,40);
		this.lamplid.setScale(0.4);


		this.lamp= new Button(this, 10, TAG_LAMP, "#lampBottle.png",this.callback);
		this.lamp.setPosition(0,0);
		this.lamp.setScale(0.4);


		this.match= new Button(this, 1, TAG_MATCH, "#match.png",this.callback);
		this.match.setScale(0.3);
		this.match.setPosition(170,-20);



	},
	fire:function(){
		var frames=[];
		for (i=1;i<=3;i++){
			var str ="fire2/"+i+".png";
			var frame=cc.spriteFrameCache.getSpriteFrame(str);
			frames.push(frame);
		}
		var animation = new cc.Animation(frames,0.05);//负责动画序列
		var action = new cc.Animate(animation);//帧动画的动作创建

		var fire = new cc.Sprite("#fire2/1.png");
		fire.setPosition(165,295);
		fire.setAnchorPoint(0.5,0);
		fire.setScale(3.3);
		ll.run.lamp.lamp.addChild(fire,1,TAG_SHOW);
		fire.runAction(action.repeatForever());
	},
  
	callback:function(p){
		var action = gg.flow.flow.action;
		var func = cc.callFunc(this.actionDone, this);	
		switch(p.getTag()){		
		case TAG_MATCH:
			var match = new cc.Sprite("#match1.png");
			match.setPosition(240,42);
			match.setScale(0.25);
			match.setAnchorPoint(1,1);
			this.addChild(match,2);
			var seq = cc.sequence(cc.moveTo(0.3,cc.p(280,20)),cc.callFunc(function(){
				match.setSpriteFrame("match2.png");
			},this),cc.moveTo(0.8,cc.p(65,80)),cc.callFunc(function(){
				this.fire();
				match.removeFromParent(true);
				gg.flow.next();
			},this));
			match.runAction(seq);
			break;
		case TAG_LAMP_LID:
			var move = cc.moveTo(0.5,cc.p(0,90));
			var move1 = cc.moveTo(0.5,cc.p(70,60));
			var move2 = cc.moveTo(0.5,cc.p(100,-40));
			var seq = cc.sequence(move,move1,move2,cc.callFunc(this.flowNext ,this));
			p.runAction(seq);
			break;
		case TAG_LAMP:			
			var move  = cc.moveTo(1,cc.p(-100,60));
			var seq = cc.sequence(move,cc.delayTime(1),cc.callFunc(function(){			
				ll.run.duiliu.addarrow();
				ll.run.duiliu.runarrow(0.5);
			},this),cc.delayTime(1),cc.callFunc(function(){
				this.flowNext();
			},this));
			p.runAction(seq);


			break;

		}
	},
	flowNext:function(){
		gg.flow.next();
	},
});