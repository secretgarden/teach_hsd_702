startRunNext04 = function(tag){
	switch(tag){
	case TAG_YINDAO:
	case TAG_SHIZHAN:
		ch.gotoRun(TAG_EXP_04, g_resources_run04, null, new RunScene04());
		break;
	case TAG_QUWEI:
		gg.hJson = hJson04;
		ch.gotoGame(TAG_EXP_04, g_resources_public_game, res_public_game.game_p);
		break;
//		case TAG_TEST:
//		ch.gotoTest(TAG_EXP_04, g_resources_test04, null, res_test04.subject);
//		break;
	case TAG_ABOUT:
		ch.gotoAbout(TAG_EXP_04, g_resources_public_about, null, res_public_about.about_j);
		break;
	}
}
//任务流
teach_flow04 = 
	[
	 {
		 tip:"选择火柴",
		 tag:[
		      TAG_LIB,
		      TAG_LIB_BG1,	   
		      ],
		      canClick:true		 
	 },
	 {
		 tip:"选择烧杯",
		 tag:[
		      TAG_LIB,
		      TAG_LIB_BG4,	   
		      ],
		      canClick:true		 
	 },
	 {
		 tip:"选择酒精灯",
		 tag:[
		      TAG_LIB,
		      TAG_LIB_BG10,	   
		      ],
		      canClick:true		 
	 },
	 {
		 tip:"选择对流装置",
		 tag:[
		      TAG_LIB,
		      TAG_LIB_BG8,	   
		      ],
		      canClick:true		 
	 },
	 {
	     tip:"选择线香",
	     tag:[
	         TAG_LIB,
	         TAG_LIB_BG6,	   
	         ],
	         canClick:true		 
	 },
	 {
		 tip:"点击确定，开始实验",
		 tag:[
		      TAG_LIB,
		      TAG_SURE
		      ]
	 },
	 {
		 tip:"取下酒精灯帽",
		 tag:[TAG_LAMP_NODE,
		      TAG_LAMP_LID],action:ACTION_DO1
	 },
	 {
		 tip:"点燃酒精灯",
		 tag:[TAG_LAMP_NODE,
		      TAG_MATCH]
	 },
	 {
		 tip:"用酒精灯加热对流管一端，观察水的流向",
		 tag:[TAG_LAMP_NODE,
		      TAG_LAMP]
	 },
	 {
		 tip:"把线香放到酒精灯点燃，并用烧杯盖住",
		 tag:[TAG_DUILIU_NODE,
		      TAG_XIANXIANG]
	 },

	 {
		 tip:"恭喜过关",
		 over:true

	 }
]
//游戏
hJson04 = [
           {tag:TAG_H1,image:"prop1.png",name:"量筒"},
           {tag:TAG_H2,image:"prop2.png",name:"胶头滴管"},
           {tag:TAG_H3,image:"prop3.png",name:"蒸发皿"},
           {tag:TAG_H4,image:"prop4.png",name:"表面皿"},
           {tag:TAG_H5,image:"prop5.png",name:"酒精灯"},
           {tag:TAG_H6,image:"prop6.png",name:"洗耳球"}
           ]


