startRunNext11 = function(tag){
	switch(tag){
	case TAG_YINDAO:
	case TAG_SHIZHAN:
		ch.gotoRun(TAG_EXP_11, g_resources_run11, null, new RunScene11());
		break;
	case TAG_QUWEI:
		gg.hJson = hJson11;
		ch.gotoGame(TAG_EXP_11, g_resources_public_game, res_public_game.game_p);
		break;
//		case TAG_TEST:
//		ch.gotoTest(TAG_EXP_11, g_resources_test11, null, res_test11.subject);
//		break;
	case TAG_ABOUT:
		ch.gotoAbout(TAG_EXP_11, g_resources_public_about, null, res_public_about.about_j);
		break;
	}
}
//任务流
teach_flow11 = 
	[
	 {
		 tip:"选择铁架台",
		 tag:[
		      TAG_LIB,
		      TAG_LIB_BG1,	   
		      ],
		      canClick:true
	 },
	 {
		 tip:"选择烧瓶",
		 tag:[
		      TAG_LIB,
		      TAG_LIB_BG3,
		      ],
		      canClick:true
	 },
	 {
		 tip:"选择注射器",
		 tag:[
		      TAG_LIB,
		      TAG_LIB_BG4,
		      ],
		      canClick:true
	 },	
	 {
		 tip:"选择酒精灯",
		 tag:[
		      TAG_LIB,
		      TAG_LIB_BG9,
		      ],
		      canClick:true
	 },		 
	 {
		 tip:"点击确定，开始实验",
		 tag:[
		      TAG_LIB,
		      TAG_SURE
		      ]
	 },
	 {
		 tip:"取下酒精灯帽",
		 tag:[TAG_LAMP_NODE,
		      TAG_LAMP_LID],action:ACTION_DO1
	 },
	 {
		 tip:"点燃酒精灯",
		 tag:[TAG_LAMP_NODE,
		      TAG_MATCH]
	 },
	 {
		 tip:"用酒精灯加热蒸馏烧瓶",
		 tag:[
		      TAG_LAMP_NODE,
		      TAG_LAMP
		      ],action:ACTION_DO1
	 },
	 {
		 tip:"用注射器向瓶内充气，观察现象",
		 tag:[
		      TAG_INOCULATOR_NODE,
		      TAG_INOCULATOR
		      ],action:ACTION_DO1
	 }, 
	 {
		 tip:"取下酒精灯",
		 tag:[
		      TAG_LAMP_NODE,
              TAG_LAMP],action:ACTION_DO2
     },
	 {
    	 tip:"熄灭酒精灯",
    	 tag:[
    	      TAG_LAMP_NODE,
    	      TAG_LAMP_LID],action:ACTION_DO2
     },	 
     {
    	 tip:"用注射器从瓶内吸气，观察现象",
    	 tag:[
    	      TAG_INOCULATOR_NODE,
    	      TAG_INOCULATOR],action:ACTION_DO2
     }, 
	 {
		 tip:"恭喜过关",
		 over:true
	 }
	 ]
//游戏
hJson11 = [
           {tag:TAG_H1,image:"prop1.png",name:"量筒"},
           {tag:TAG_H2,image:"prop2.png",name:"胶头滴管"},
           {tag:TAG_H3,image:"prop3.png",name:"蒸发皿"},
           {tag:TAG_H4,image:"prop4.png",name:"表面皿"},
           {tag:TAG_H5,image:"prop5.png",name:"酒精灯"},
           {tag:TAG_H6,image:"prop6.png",name:"洗耳球"}
           ]





