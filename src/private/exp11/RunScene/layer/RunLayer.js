var RunLayer1101 = cc.Layer.extend({
	arr:null,
	scene:null,
	clock:null,
	ctor:function (parent) {
		this._super();
		this.scene = parent;
		this.scene.addChild(this, 10);
		gg.main = this;
		this.init();
	},
	init:function () {
		this.callNext = cc.callFunc(function(){
			gg.flow.next();
		}, this);
		this.callKill = cc.callFunc(function(p){
			var seq = cc.sequence(cc.fadeOut(0.5),cc.callFunc(function(){
				p.removeFromParent(true);	
			}, this));
			p.runAction(seq);
		}, this); 
		
		this.callNext.retain();
		this.callKill.retain();
		//时钟
		this.clock = new Clock(this);
		// 物品库
		this.lib = new Lib(this);
		this.lib.loadBg([{
			tag:TAG_LIB_IRON2,
			checkright:true,	//实验所需器材添加checkright标签		
		},
		{
			tag:TAG_LIB_CONICAL,

		},
		{
			tag:TAG_LIB_FLASK,
			checkright:true,
		},
		{
			tag:TAG_LIB_INOCULATOR,
			checkright:true,
		},
		{
			tag:TAG_LIB_YIBEI,

		},
		{
			tag:TAG_LIB_BEAKER,
		},
		{
			tag:TAG_LIB_ROD,
		},
		{
			tag:TAG_LIB_AMPULLA,
		},
		{
			tag:TAG_LIB_LAMP,
			checkright:true,
		},
		{
			tag:TAG_LIB_ZHENGFA,
		}
		]);

		this.lamp = new Lamp11(this);
		this.lamp.setVisible(false);
		this.lamp.setPosition(400,150);

		this.zhengliu = new Zhengliu11(this);
		this.zhengliu.setVisible(false);
		this.zhengliu.setPosition(700,120);
		
		this.inoculator = new Inoculator(this);
		this.inoculator.setVisible(false);
		this.inoculator.setPosition(855,430);
	},
	checkVisible:function(next){
		//是否可见
		var checkVisible = [];
		var node1 = ll.run.zhengliu;
		var node2 = ll.run.lamp;
		var node3 = ll.run.inoculator;


		checkVisible.push(node1,node2,node3);

		for(var i in checkVisible){
			if(checkVisible[i] !== null){				
				checkVisible[i].setVisible(next);				
			}			
		}
	},
	loadShelfFiltration:function(){
		//过滤装置--铁架台
		var shelf1 = new cc.Sprite("#shelf1.png");
		this.addChild(shelf1, 20,TAG_SHELF1);
		shelf1.setPosition(cc.p(gg.width*0.3-50,gg.height*0.43));
		shelf1.setScale(0.7);
		shelf1.setVisible(false);
		//过滤装置--漏斗
		var shelf2 = new cc.Sprite("#shelf2.png");
		this.addChild(shelf2, 23,TAG_SHELF2);
		shelf2.setPosition(cc.p(334,274));
		shelf2.setScale(0.7);
		shelf2.setVisible(false);
		//过滤装置--漏斗（里层切图）
		var shelf3 = new cc.Sprite("#shelf3.png");
		shelf1.addChild(shelf3, 2,TAG_SHELF3);
		shelf3.setPosition(cc.p(246,427));
	},
	loadShelfEvaporation:function(){
		//蒸发装置--铁架台
		var shelf4 = new cc.Sprite("#shelf4.png");
		this.addChild(shelf4, 20,TAG_SHELF4);
		shelf4.setPosition(cc.p(gg.width*0.8,gg.height*0.43));
		shelf4.setScale(0.7);
		shelf4.setVisible(false);
		//蒸发装置--蒸发皿
		var shelf5 = new Button(shelf4, 2, TAG_SHELF5, "#shelf5.png",this.callback);
		shelf5.setPosition(cc.p(203,415));
	},
	loadInLib:function(obj, pos, tarPos,delay){
		obj.setPosition(pos);
		if(delay == null){
			delay = 1;
		}
		var ber = $.bezier(pos, tarPos, delay);
		var seq = cc.sequence(ber, this.callNext);
		obj.runAction(seq);
	},
	kill:function(obj){
		var fade = cc.fadeTo(0.5,0);
		var func = cc.callfunc(function(){
			obj.removeFromParent(true);
		},this);
		var seq = cc.sequence(fade,func);
		obj.runAction(seq)
	},	
	callback:function (p){
		var func = cc.callFunc(this.actionDone, this);
		var action=gg.flow.flow.action;
		switch(p.getTag()){
		case TAG_SHELF5:
			p.setSpriteFrame("dish_salt.png");
			var seq = cc.sequence(cc.callFunc(function() {
				var shelf = p.getParent();
				shelf.runAction(cc.fadeOut(0));
				var show = new ShowTip("可以观察到，蒸发皿中有除去杂质、\n较为纯净的固体精盐。",cc.p(450,350));
			}, this),cc.delayTime(3),cc.callFunc(function() {
				gg.flow.next();
			}, this));
			p.runAction(seq);
		break;		 
		}
	},
	flowNext:function(){
		gg.flow.next();
	},
	onExit:function(){
		this._super();
		this.callNext.release();
		this.callKill.release();
	}
});