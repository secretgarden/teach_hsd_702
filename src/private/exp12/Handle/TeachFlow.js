startRunNext12 = function(tag){
	switch(tag){
	case TAG_YINDAO:
	case TAG_SHIZHAN:
		ch.gotoRun(TAG_EXP_12, g_resources_run12, null, new RunScene12());
		break;
	case TAG_QUWEI:
		gg.hJson = hJson12;
		ch.gotoGame(TAG_EXP_12, g_resources_public_game, res_public_game.game_p);
		break;
	case TAG_TEST:
		ch.gotoTest(TAG_EXP_12, g_resources_test12, null, res_test12.subject);
		break;
	case TAG_ABOUT:
		ch.gotoAbout(TAG_EXP_12, g_resources_public_about, null, res_public_about.about_j);
		break;
	}
}

// 任务流
teach_flow12 = 
	[
	 {
		 tip:"点击洒水壶，向泥土坡面洒水，并观察",
		 tag:[TAG_KETTLE,
		      TAG_KETTLE1
		      ],action:ACTION_DO1
	 },
	 {
		 tip:"点击洒水壶，向植被坡面洒水，并观察",
		 tag:[TAG_KETTLE,
		      TAG_KETTLE1
		      ],action:ACTION_DO2
	 },
	 {
		 tip:"分析实验结果",
		 tag:[TAG_LIB
		      ]
	 },
	 {
		 tip:"恭喜过关",
		 over:true
	 }
	 ]
//游戏
hJson12 = [
           {tag:TAG_H1,image:"prop1.png",name:"量筒"},
           {tag:TAG_H2,image:"prop2.png",name:"胶头滴管"},
           {tag:TAG_H3,image:"prop3.png",name:"蒸发皿"},
           {tag:TAG_H4,image:"prop4.png",name:"表面皿"},
           {tag:TAG_H5,image:"prop5.png",name:"酒精灯"},
           {tag:TAG_H6,image:"prop6.png",name:"洗耳球"}
           ]



