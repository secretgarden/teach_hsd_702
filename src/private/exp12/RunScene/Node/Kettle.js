/**
 * 洒水壶
 */
Kettle= cc.Node.extend({
	ctor:function(parent){
		this._super();
		parent.addChild(this, 51);
		this.init();
		this.setTag(TAG_KETTLE);
	},
	init:function(){
		this.setCascadeOpacityEnabled(true);

		// 洒水壶
		this.kettle = new Button(this, 10, TAG_KETTLE1, "#kettle2.png", this.callback);
		this.kettle.setPosition(cc.p(gg.width*0.5,600));
		this.kettle.setScale(0.7);
		
		this.callNext = cc.callFunc(function(){
			gg.flow.next();
		}, this);
		this.callNext.retain();
	},
	//洒水壶的水
	loadWater:function(pos){		
		var water = new cc.Sprite("#water1.png");
		this.addChild(water, 2,TAG_WATER);
		water.setScale(0.7);
		water.setPosition(pos);
	},
	//动画水
	water:function(p){
		var animFrames = [];
		for (var i = 1; i < 3; i++) {
			var str = "water" + i + ".png";
			var frame = cc.spriteFrameCache.getSpriteFrame(str);
			animFrames.push(frame);
		}
		var animation = new cc.Animation(animFrames, 0.2,10);
		var action = cc.animate(animation);
		var seq=cc.sequence(action);
		p.runAction(seq);
	},
	callback:function (p){
		var action=gg.flow.flow.action;
		var func=cc.callFunc(this.actionDone,this);
		switch(p.getTag()){
		case TAG_KETTLE1:
			if(action==ACTION_DO1){
				var mov = cc.moveTo(0.6, cc.p(1140,583));
				var rota = cc.rotateTo(1, -45);
				var rota1 = cc.rotateTo(1, 0);
				var mov1 = cc.moveTo(0.6, cc.p(gg.width*0.5,600));
				var seq = cc.sequence(mov,rota,cc.callFunc(function() {
					this.loadWater(cc.p(953,473));
					var water = this.getChildByTag(TAG_WATER);
					water.runAction(cc.sequence(cc.callFunc(function() {
						this.water(water);						
					}, this),cc.delayTime(0.5),cc.callFunc(function() {
						var land = this.getParent().getChildByTag(TAG_EXPERIMENT).getChildByTag(TAG_EXPERIMENT_BG1).getChildByTag(TAG_LAND);
						land.setVisible(true);
						ll.run.experiment.land(land);
						land.runAction(cc.sequence(cc.delayTime(5),cc.fadeOut(1)));

						var landflow = this.getParent().getChildByTag(TAG_EXPERIMENT).getChildByTag(TAG_LANDFLOW2);
						var move1 = cc.moveTo(3,cc.p(635,194));
						var move2 = cc.moveTo(3, cc.p(616,212));
						var seq =cc.sequence(move1,move2);
						landflow.runAction(seq);

						var waterflow = this.getParent().getChildByTag(TAG_EXPERIMENT).getChildByTag(TAG_WATERFLOW2);
						var move3 = cc.moveTo(3,cc.p(642,190));
						var move4 = cc.moveTo(2.5,cc.p(623,213));
						var seq2 =cc.sequence(move3,move4);
						waterflow.runAction(seq2);
					}, this),cc.delayTime(4),cc.callFunc(function() {
						water.removeFromParent();						
					}, this)));					
				}, this),cc.delayTime(5),rota1,mov1,cc.delayTime(1),this.callNext);
				p.runAction(seq);				
			}else if(action==ACTION_DO2){
				var mov = cc.moveTo(0.6, cc.p(540,583));
				var rota = cc.rotateTo(1, -45);
				var rota1 = cc.rotateTo(1, 0);
				var mov1 = cc.moveTo(0.6, cc.p(gg.width*0.5,600));
				var fadeout = cc.fadeOut(0);
				var seq = cc.sequence(mov,rota,cc.callFunc(function() {
					this.loadWater(cc.p(390,473));
					var water = this.getChildByTag(TAG_WATER);
					water.runAction(cc.sequence(cc.callFunc(function() {
						this.water(water);						
					}, this),cc.delayTime(0.5),cc.callFunc(function() {
						var grass = this.getParent().getChildByTag(TAG_EXPERIMENT).getChildByTag(TAG_EXPERIMENT_BG1).getChildByTag(TAG_GRASS);
						grass.setVisible(true);
						ll.run.experiment.grass(grass);
						grass.runAction(cc.sequence(cc.delayTime(5),cc.fadeOut(1)));
						
						var landflow = this.getParent().getChildByTag(TAG_EXPERIMENT).getChildByTag(TAG_LANDFLOW1);
						var move1 = cc.moveTo(6,cc.p(569,196));
						landflow.runAction(move1);

						var waterflow = this.getParent().getChildByTag(TAG_EXPERIMENT).getChildByTag(TAG_WATERFLOW1);
						var move2 = cc.moveTo(6,cc.p(570,198));
						waterflow.runAction(move2);
					}, this),cc.delayTime(4),cc.callFunc(function() {
						water.removeFromParent();
					}, this)));
				}, this),cc.delayTime(5),rota1,mov1,fadeout,cc.delayTime(1),this.callNext,cc.callFunc(function() {
					var show = new ShowTip("实验证明：植被能保持水土，增加雨水下渗。",cc.p(gg.width*0.5,550));
					
				}, this),cc.delayTime(5),this.callNext);
				p.runAction(seq);
			}
			break;
		default:
			break;
		}
	},
	actionDone:function(p){
		var action=gg.flow.flow.action;
		switch(p.getTag()){
		case TAG_KETTLE2:

			break;
		default:
			break;
		}
		gg.flow.next();
	}
})