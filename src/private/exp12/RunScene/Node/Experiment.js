/**
 * 实验(玻璃缸）
 */
Experiment = cc.Node.extend({
	ctor:function(parent){
		this._super();
		parent.addChild(this,30);
		this.setTag(TAG_EXPERIMENT);
		this.init();
	},
	init : function(){	
		var dang = new cc.Sprite(res_run12.out);
		this.addChild(dang,10);
		dang.setPosition(cc.p(gg.width*0.5,11));
		//实验背景1——玻璃缸中间层
		var bg1 = new cc.Sprite("#bg_1.png");
		this.addChild(bg1,7,TAG_EXPERIMENT_BG1);
		bg1.setPosition(cc.p(gg.width*0.5,gg.height*0.5-80));
		bg1.setScale(1.4);
		//实验背景2——玻璃缸最下层水沟面（左）
		var bg2 = new cc.Sprite("#bg_2.png");
		this.addChild(bg2,1,TAG_EXPERIMENT_BG2);
		bg2.setPosition(cc.p(549,312-80));	
		bg2.setScale(1.4);
		//实验背景3——玻璃缸最下层水沟面（右）
		var bg3 = new cc.Sprite("#bg_2.png");
		this.addChild(bg3,4,TAG_EXPERIMENT_BG3);
		bg3.setPosition(cc.p(611,312-80));	
		bg3.setScale(1.4);
		//实验背景4——玻璃缸最上层半透明
		var bg4 = new cc.Sprite("#bg_3.png");
		this.addChild(bg4,8,TAG_EXPERIMENT_BG4);
		bg4.setPosition(cc.p(gg.width*0.5,gg.height*0.5-80));
		bg4.setScale(1.4);
		
		//草地上流水
		var grass = new cc.Sprite("#caodi/grassland_1.png");
		bg1.addChild(grass,20,TAG_GRASS);
		grass.setPosition(cc.p(165,141));
		grass.setVisible(false);
		//泥土地上流水
		var land = new cc.Sprite("#tudi/land_1.png");
		bg1.addChild(land,20,TAG_LAND);
		land.setPosition(cc.p(485,149));
		land.setVisible(false);	
		
		//水槽里水平面（左）
		var waterflow1 = new cc.Sprite("#waterflow.png");
		this.addChild(waterflow1,3,TAG_WATERFLOW1);
		waterflow1.setPosition(cc.p(600,185));
		waterflow1.setScale(1.4);
		waterflow1.setOpacity(160);
		//水槽里泥平面（左）
		var landflow1 = new cc.Sprite("#landflow.png");
		this.addChild(landflow1,2,TAG_LANDFLOW1);
		landflow1.setPosition(cc.p(594,195));
		landflow1.setScale(1.4);
		
		//水槽里水平面（右）
		var waterflow2 = new cc.Sprite("#waterflow.png");
		this.addChild(waterflow2,6,TAG_WATERFLOW2);
		waterflow2.setPosition(cc.p(668,190));
		waterflow2.setScale(1.4);
		waterflow2.setOpacity(100);
		//水槽里泥平面（右）
		var landflow2 = new cc.Sprite("#landflow.png");
		this.addChild(landflow2,5,TAG_LANDFLOW2);
		landflow2.setPosition(cc.p(656,194));
		landflow2.setScale(1.4);
		
	},
	grass:function(p){
		var animFrames = [];
		for (var i = 1; i < 7; i++) {
			var str = "caodi/grassland_" + i + ".png";
			var frame = cc.spriteFrameCache.getSpriteFrame(str);
			animFrames.push(frame);
		}
		var animation = new cc.Animation(animFrames, 0.3,4);
		var action = cc.animate(animation);
		var seq=cc.sequence(action);
		p.runAction(seq);
	},	
	land:function(p){
		var animFrames = [];
		for (var i = 1; i < 7; i++) {
			var str = "tudi/land_" + i + ".png";
			var frame = cc.spriteFrameCache.getSpriteFrame(str);
			animFrames.push(frame);
		}
		var animation = new cc.Animation(animFrames, 0.2,5);
		var action = cc.animate(animation);
		var seq=cc.sequence(action);
		p.runAction(seq);
	},	
});
