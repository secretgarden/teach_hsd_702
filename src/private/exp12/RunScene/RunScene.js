var RunLayer12 = cc.Layer.extend({
	backgroundLayer : null,
	mainLayar : null,
	ctor:function () {
		this._super();
		this.initFrames();
		this.loadBackground();
		this.loadMainLayer();
	},
	initFrames : function(){
		cc.spriteFrameCache.addSpriteFrames(res_run12.run_p);
		gg.curRunSpriteFrame.push(res_run12.run_p);
	},
	loadBackground : function(){
		this.backgroundLayer = new RunBackgroundLayer();
		this.addChild(this.backgroundLayer);
	},
	loadMainLayer : function(){
		this.mainLayar = new RunMainLayer12();
		this.addChild(this.mainLayar);
	}
});

var RunScene12 = PScene.extend({
	onEnter:function () {
		this._super();
		gg.initTeach();
		gg.scoreCheck = true;
		var layer = new RunLayer12();
		this.addChild(layer);
	}
});
