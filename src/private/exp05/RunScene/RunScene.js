var RunLayer05 = cc.Layer.extend({
	backgroundLayer : null,
	mainLayar : null,
	ctor:function () {
		this._super();
		this.initFrames();
		this.loadBackground();
		this.loadMainLayer();
	},
	initFrames : function(){
		cc.spriteFrameCache.addSpriteFrames(res_run05.run_p);
		gg.curRunSpriteFrame.push(res_run05.run_p);
	},
	loadBackground : function(){
		this.backgroundLayer = new RunBackgroundLayer();
		this.addChild(this.backgroundLayer);
	},
	loadMainLayer : function(){
		this.mainLayar = new RunMainLayer05();
		this.addChild(this.mainLayar);
	}
});

var RunScene05 = PScene.extend({
	onEnter:function () {
		this._super();
		gg.initTeach();
		gg.firstRun = true;
		gg.firstRun_huxi = true;
		gg.succeed_huxi = false;
		var layer = new RunLayer05();
		this.addChild(layer);
	}	

});
