startRunNext05 = function(tag){
	switch(tag){
	case TAG_YINDAO:
	case TAG_SHIZHAN:
		ch.gotoRun(TAG_EXP_05, g_resources_run05, null, new RunScene05());
		break;
	case TAG_QUWEI:
		gg.hJson = hJson05;
		ch.gotoGame(TAG_EXP_05, g_resources_public_game, res_public_game.game_p);
		break;
//		case TAG_TEST:
//		ch.gotoTest(TAG_EXP_05, g_resources_test05, null, res_test05.subject);
//		break;
	case TAG_ABOUT:
		ch.gotoAbout(TAG_EXP_05, g_resources_public_about, null, res_public_about.about_j);
		break;
	}
}
//任务流
teach_flow05 = [
                {
                	tip:"人体内分泌腺的分布与功能",
                	tag:TAG_HUXI,
                	create:[],
                	destroy:[],
                	node:true,
                	frameInfo : [
                	             {ex:1,name:"脑垂体",tag:TAG_FENMI_CHUITI,px:0,py:492,img:"#fenmi/pituitary.png",st:"分泌的生长激素能控制人的发育。",posx:222,posy:584,next:true,scale:2.2},
                	             {ex:1,name:"甲状腺",tag:TAG_FENMI_JIAXIAN,px:0,py:492,img:"#fenmi/thyroid1.png",st:"分泌的甲状腺激素能够促进体内物质和能量的转换，\n提高神经系统的兴奋性。",posx:238,posy:548,next:true,scale:2.2},
                	             {ex:1,name:"肾上腺",tag:TAG_FENMI_SHENXIAN,px:0,py:492,img:"#fenmi/adrenal1.png",st:"分泌肾上腺素能加快心跳的节奏，扩张通往肌肉的血管",posx:800,posy:445,next:false,scale:2.2},
                	             {ex:1,name:"胰腺",tag:TAG_FENMI_YIXIAN,px:0,py:492,img:"#fenmi/pancreas1.png",st:"胰岛分泌的胰岛素能促进人体吸收的葡萄糖存储在肝脏和肌肉内。",posx:554,posy:445,next:false,scale:2.2},            	           
                	             {ex:1,name:"睾丸",tag:TAG_FENMI_GAOWAN,px:370,py:250,img:"#fenmi/testes.png",st:"分泌雄性激素促进生殖器官的发育和生殖细胞的生长，\n激发和维持人的第二性征。",posx:513,posy:395,next:true,scale:2.2},               	         
                	             {ex:1,name:"卵巢",tag:TAG_FENMI_LUANCHAO,px:163,py:257,img:"#fenmi/ovary.png",st:"分泌雌性激素促进生殖器官的发育和生殖细胞的生长，\n激发和维持人的第二性征",posx:264,posy:406,next:true,scale:2.2},
                	       
                	           
                	             ],
                	             nodeName:"人体内分泌腺的分布与功能"
                },
                {
                	tip:"恭喜过关",
                	over:true
                }
                ]
//游戏
hJson05 = [
           {tag:TAG_H1,image:"prop1.png",name:"量筒"},
           {tag:TAG_H2,image:"prop2.png",name:"胶头滴管"},
           {tag:TAG_H3,image:"prop3.png",name:"蒸发皿"},
           {tag:TAG_H4,image:"prop4.png",name:"表面皿"},
           {tag:TAG_H5,image:"prop5.png",name:"酒精灯"},
           {tag:TAG_H6,image:"prop6.png",name:"洗耳球"}
           ]




