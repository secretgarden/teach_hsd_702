startRunNext08 = function(tag){
	switch(tag){
	case TAG_YINDAO:
	case TAG_SHIZHAN:
		ch.gotoRun(TAG_EXP_08, g_resources_run08, null, new RunScene08());
		break;
	case TAG_QUWEI:
		gg.hJson = hJson08;
		ch.gotoGame(TAG_EXP_08, g_resources_public_game, res_public_game.game_p);
		break;
//		case TAG_TEST:
//		ch.gotoTest(TAG_EXP_08, g_resources_test08, null, res_test08.subject);
//		break;
	case TAG_ABOUT:
		ch.gotoAbout(TAG_EXP_08, g_resources_public_about, null, res_public_about.about_j);
		break;
	}
}
//任务流
teach_flow08 = 
	[
	 {
		 tip:"选择铁架台",
		 tag:[
		      TAG_LIB,
		      TAG_LIB_BG1,	   
		      ],
		      canClick:true		 
	 },
	 {
		 tip:"选择冷凝管",
		 tag:[
		      TAG_LIB,
		      TAG_LIB_BG4,	   
		      ],
		      canClick:true		 
	 },
	 {
		 tip:"选择锥形瓶",
		 tag:[
		      TAG_LIB,
		      TAG_LIB_BG5,	   
		      ],
		      canClick:true		 
	 },
	 {
		 tip:"选择蒸馏装置",
		 tag:[
		      TAG_LIB,
		      TAG_LIB_BG10,	   
		      ],
		      canClick:true		 
	 },
	 {
		 tip:"选择酒精灯",
		 tag:[
		      TAG_LIB,
		      TAG_LIB_BG8,	   
		      ],
		      canClick:true		 
	 },
	 {
		 tip:"点击确定，开始实验",
		 tag:[
		      TAG_LIB,
		      TAG_SURE
		      ]
	 },
	 {
		 tip:"将冷凝管装满水",
		 tag:[TAG_ZHENGLIU_NODE,
		      TAG_DOWN]
	 },

	 {
		 tip:"取下酒精灯帽",
		 tag:[TAG_LAMP_NODE,
		      TAG_LAMP_LID],action:ACTION_DO1
	 },
	 {
	    tip:"点燃酒精灯",
	    tag:[TAG_LAMP_NODE,
	         TAG_MATCH]
	 },
	 {
		 tip:"用酒精灯加热蒸馏烧瓶",
		 tag:[
		      TAG_LAMP_NODE,
		      TAG_LAMP
		      ]
	 },
	 {
		 tip:"恭喜过关",
		 over:true
	 }
	 ]
//游戏
hJson08 = [
           {tag:TAG_H1,image:"prop1.png",name:"量筒"},
           {tag:TAG_H2,image:"prop2.png",name:"胶头滴管"},
           {tag:TAG_H3,image:"prop3.png",name:"蒸发皿"},
           {tag:TAG_H4,image:"prop4.png",name:"表面皿"},
           {tag:TAG_H5,image:"prop5.png",name:"酒精灯"},
           {tag:TAG_H6,image:"prop6.png",name:"洗耳球"}
           ]





