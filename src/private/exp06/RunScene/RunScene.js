var RunLayer06 = cc.Layer.extend({
	backgroundLayer : null,
	mainLayar : null,
	ctor:function () {
		this._super();
		this.initFrames();
		this.loadBackground();
		this.loadMainLayer();
	},
	initFrames : function(){
		cc.spriteFrameCache.addSpriteFrames(res_run06.run_p);
		gg.curRunSpriteFrame.push(res_run06.run_p);
	},
	loadBackground : function(){
		this.backgroundLayer = new RunBackgroundLayer();
		this.addChild(this.backgroundLayer);
	},
	loadMainLayer : function(){
		this.mainLayar = new RunMainLayer06();
		this.addChild(this.mainLayar);
	}
});

var RunScene06 = PScene.extend({
	onEnter:function () {
		this._super();
		gg.initTeach();
		gg.firstRun = true;
		gg.firstRun_danao = true;
		gg.succeed_danao = false;
		var layer = new RunLayer06();
		this.addChild(layer);
	}	

});
