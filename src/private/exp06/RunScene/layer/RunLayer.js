var RunLayer0601 = BaseLayer.extend({
	frame:[],
	num:0,
	no:0,
	curSel:null,
	ctor:function (parent) {
		this._super(parent,5);
		this.init();
	},
	init:function () {
		ll.tip.tip.setVisible(false);
		cc.log("大脑皮层不同功能区");
		this._super();
		//第一次加载，弹框提示
		this.loadWin();
		
		this.loadInfo();		
		//加载主图
		this.loadMain();
		//第一次加载，弹框提示
		this.loadWin();
		if(gg.teach_type == TAG_REAL){
			//实战模式下，随机一个部位闪烁
			this.loadFlash();
		}				
	},
	loadInfo:function(){
		for(var i in teach_flow06){
			var info = teach_flow06[i];
			if(info.tag == TAG_DANAO){
				//读取Flow里的任务流
				this.frameInfo = info.frameInfo;
				//复制一个数组，做实战中删除用
				this.frame = this.frameInfo.concat();
			}
		}		
	},
	loadWin:function(){
		if(gg.firstRun){
			gg.synch_l = true;
			var tip = ll.tip.getChildByTag(TAG_TIP);
			tip.setEnable(false);
			var back = ll.tip.getChildByTag(TAG_CLOSE);
			back.setEnable(false);
			var win = new WinFrame(ll.tip,3,"点击右侧结构名称");
			win.open2();
			gg.firstRun = false;
		}					
	},
	loadFlash:function(){
		this._super();
	},
	loadMain:function(){
		//呼吸系统主图
		this.body = new cc.Sprite("#danao/body.png");
		this.body.setPosition(cc.p(gg.width*0.39, gg.height*0.52));
		this.body.setScale(2);
		if(gg.teach_type == TAG_REAL){
			this.body.setOpacity(50);
		}else {
			this.body.setOpacity(255);
		}		
		this.addChild(this.body,1);
		var bw = this.body.width;
		var bh = this.body.height;
		for(var i in this.frameInfo){
			var info = this.frameInfo[i];						
			if(gg.teach_type == TAG_LEAD){
				info.finish = false;			
			}else{
				if(gg.firstRun_danao){
					info.finish = false;					
				}
			}
			var bt = new Button(this.body,10,info.tag,info.img,this.callback,this);
			bt.setPosition(cc.p(info.px, info.py));
			bt.setAnchorPoint(0,1);
			bt.setOpacity(1);
			bt.info = info;
		}				
		//加载名称
		this.loadTipFrame();
		gg.firstRun_danao = false;
	},
	loadTipFrame:function(){
		this._super();
	},
	clickCheck:function(p){
		this._super(p);
	},
	showWord:function(p){
		this._super(p);
	},
	callback:function (p){
		this._super(p);
		this.body.setOpacity(50);
		switch (p.getTag()) {	
		case TAG_DAONAO_NAOGAN:
		case TAG_DANAO_PIFU:
		case TAG_DANAO_TINGJUE:
		case TAG_DANAO_SHIJUE:
		case TAG_DANAO_XIAONAO:
		case TAG_DANAO_XIUJUE:
		case TAG_DANAO_YUDONG:
		case TAG_DANAO_YUYAN:


			
			this.clickCheck(p);						
			break;			
		default:
			break;
		}
		if(gg.teach_type == TAG_REAL){
			if(gg.succeed_danao){			
				this.runAction(cc.sequence(cc.callFunc(function() {
				}, this),cc.delayTime(2),cc.callFunc(function() {
					gg.firstRun_danao = false;
					gg.flow.next();
				}, this)));		
			}
		}

	},
	destroy:function(){
		this._super();
	},
	onExit:function(){
		this._super();
	}
});