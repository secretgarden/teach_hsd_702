startRunNext06 = function(tag){
	switch(tag){
	case TAG_YINDAO:
	case TAG_SHIZHAN:
		ch.gotoRun(TAG_EXP_06, g_resources_run06, null, new RunScene06());
		break;
	case TAG_QUWEI:
		gg.hJson = hJson06;
		ch.gotoGame(TAG_EXP_06, g_resources_public_game, res_public_game.game_p);
		break;
//		case TAG_TEST:
//		ch.gotoTest(TAG_EXP_06, g_resources_test06, null, res_test06.subject);
//		break;
	case TAG_ABOUT:
		ch.gotoAbout(TAG_EXP_06, g_resources_public_about, null, res_public_about.about_j);
		break;
	}
}
//任务流
teach_flow06 = [
                {
                	tip:"大脑皮层的不同功能区",
                	tag:TAG_DANAO,
                	create:[],
                	destroy:[],
                	node:true,
                	frameInfo : [
                	             {ex:1,name:"脑干",tag:TAG_DAONAO_NAOGAN,px:108,py:78,img:"#danao/naogan.png",st:"脑干在大脑下面，主要控制血液循环系统、呼吸系统的活动。",posx:700,posy:280,next:false,scale:2},
                	             {ex:1,name:"小脑",tag:TAG_DANAO_XIAONAO,px:115,py:92,img:"#danao/xiaonao.png",st:"小脑位于脑干背侧，大脑的后下方，主要负责人体动作的协调性。",posx:750,posy:320,next:false,scale:2.8},
                	             {ex:1,name:"皮肤中枢",tag:TAG_DANAO_PIFU,px:93,py:183,img:"#danao/pifu.png",st:"控制人体皮肤感觉的高级中枢。",posx:750,posy:520,next:false,scale:2.2},
                	             {ex:1,name:"视觉中枢",tag:TAG_DANAO_SHIJUE,px:148,py:122,img:"#danao/shijue.png",st:"控制人体视觉的高级中枢。",posx:800,posy:380,next:false,scale:2.2},
                	             {ex:1,name:"听觉中枢",tag:TAG_DANAO_TINGJUE,px:98,py:138,img:"#danao/tingjue.png",st:"控制人体听觉的高级中枢。",posx:750,posy:430,next:false,scale:2.9},
                	            
                	             {ex:1,name:"嗅觉中枢",tag:TAG_DANAO_XIUJUE,px:43,py:133,img:"#danao/xiujue.png",st:"控制人体嗅觉的高级中枢。",posx:260,posy:430,next:true,scale:2},
                	             {ex:1,name:"运动中枢",tag:TAG_DANAO_YUDONG,px:88,py:188,img:"#danao/yundong.png",st:"控制人体运动的高级中枢。",posx:320,posy:540,next:true,scale:3.6},
                	             {ex:1,name:"语言中枢",tag:TAG_DANAO_YUYAN,px:73,py:160,img:"#danao/yuyan.png",st:"控制人体语言的高级中枢。",posx:250,posy:480,next:true,scale:3.6},
                	           
                	             ],
                	             nodeName:"大脑皮层的不同功能区"
                },
                {
                	tip:"恭喜过关",
                	over:true
                }
                ]
//游戏
hJson06 = [
           {tag:TAG_H1,image:"prop1.png",name:"量筒"},
           {tag:TAG_H2,image:"prop2.png",name:"胶头滴管"},
           {tag:TAG_H3,image:"prop3.png",name:"蒸发皿"},
           {tag:TAG_H4,image:"prop4.png",name:"表面皿"},
           {tag:TAG_H5,image:"prop5.png",name:"酒精灯"},
           {tag:TAG_H6,image:"prop6.png",name:"洗耳球"}
           ]




