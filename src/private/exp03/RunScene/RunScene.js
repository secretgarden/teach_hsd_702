var RunLayer03 = cc.Layer.extend({
	backgroundLayer : null,
	mainLayar : null,
	ctor:function () {
		this._super();
		this.initFrames();
		this.loadBackground();
		this.loadMainLayer();
	},
	initFrames : function(){
		cc.spriteFrameCache.addSpriteFrames(res_run03.run_p);
		gg.curRunSpriteFrame.push(res_run03.run_p);
	},
	loadBackground : function(){
		this.backgroundLayer = new RunBackgroundLayer();
		this.addChild(this.backgroundLayer);
	},
	loadMainLayer : function(){
		this.mainLayar = new RunMainLayer03();
		this.addChild(this.mainLayar);
	}
});

var RunScene03 = PScene.extend({
	onEnter:function () {
		this._super();
		gg.initTeach();
		var layer = new RunLayer03();
		this.addChild(layer);
	}
});
