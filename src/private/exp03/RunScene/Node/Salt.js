Salt03 = cc.Node.extend({
	ctor:function(p){
		this._super();
		p.addChild(this, 10, TAG_SALT_NODE);
		this.init();
	},
	init : function(){
		this.setCascadeOpacityEnabled(true);
		this.saltbottle= new Button(this, 1, TAG_SALT_BOTTLE, "#naclbottle1.png",this.callback);
		this.saltbottle.setScale(0.8);
		
		this.saltbottle1 = new cc.Sprite("#naclbottle2.png");
		this.addChild(this.saltbottle1,10);
		this.saltbottle1.setScale(0.8);
		
		var label = new cc.LabelTTF("食盐","微软雅黑",25);
		label.setPosition(60,85);
		label.setColor(cc.color(0, 0, 0, 0));
		this.saltbottle1.addChild(label);
		
		this.lid= new Button(this, 5, TAG_LID, "#lid1.png",this.callback);
		this.lid.setPosition(0,70);
		this.lid.setScale(0.8);
		
		this.spoon= new Button(this, 5, TAG_SPOON, "#spoon.png",this.callback);
		this.spoon.setPosition(-150,0);
		this.spoon.setScale(0.7);


	},

	callback:function(p){
		var action = gg.flow.flow.action;
		var func = cc.callFunc(this.actionDone, this);	
		switch(p.getTag()){	
		case TAG_LID:
			var move = cc.moveTo(0.5,cc.p(0,120));
			var ber = cc.bezierTo(1, [cc.p(50,100),cc.p(80,0),cc.p(100,-60)]);
			var rota = cc.rotateTo(1,180);
			var sp = cc.spawn(ber,rota);
			var seq = cc.sequence(move,sp,cc.callFunc(function(){
				p.setSpriteFrame("lid2.png");
				p.setRotation(0);
				this.flowNext(); 
			},this));
			p.runAction(seq);
		break;
		case TAG_SPOON:
			this.saltbottle.runAction(cc.rotateTo(0.5,-50));
			this.saltbottle1.runAction(cc.rotateTo(0.5,-50));
			var move = cc.moveTo(0.5,cc.p(-125,105));
			var rota = cc.rotateTo(0.5,-15);
			var sp = cc.spawn(move,rota);
			
			var move1 = cc.moveTo(0.5,cc.p(-30,35));
			var move2 = cc.moveTo(1.5,cc.p(-550,-50));
			var seq = cc.sequence(sp,move1,cc.callFunc(function(){
				p.setSpriteFrame("spoon2.png");
			},this),move,move2,cc.callFunc(function(){
				p.setSpriteFrame("spoon.png");
				ll.run.balance.addsalt();
				this.removeFromParent(true);
			},this));
			p.runAction(seq);
		break;
		}
	},
	flowNext:function(){
		gg.flow.next();
	},
});