Beaker03 = cc.Node.extend({
	ctor:function(p){
		this._super();
		p.addChild(this, 10, TAG_BEAKER_NODE);
		this.init();
	},
	init : function(){
		this.setCascadeOpacityEnabled(true);
		this.beaker= new Button(this, 1, TAG_BEAKER, "#beaker1.png",this.callback);
		this.beaker.setScale(0.7);
		
		beaker1= new Button(this, 10, TAG_BEAKER2, "#beaker2.png",this.callback);
		beaker1.setScale(0.7);

		this.beakerline = new cc.Sprite("#line.png");
		this.beakerline.setPosition(96,20);
		this.beaker.addChild(this.beakerline);
		this.beakerline.setScale(1,2);
		
		this.rod= new Button(this, 1, TAG_ROD, "#rod.png",this.callback);
		this.rod.setPosition(45,46);
		this.rod.setRotation(10);
		this.rod.setScale(0.5);
		
		this.cylinder= new Button(this, 5, TAG_CYLINDER, "#cylinder.png",this.callback);
		this.cylinder.setPosition(150,40);
		this.cylinder.setScale(0.5);
		
		this.cyline = new cc.Sprite("#cy_line.png");
		this.cyline.setPosition(64,55);
		this.cylinder.addChild(this.cyline);
		this.cyline.setScale(0.8,1.2);
		
		this.water= new Button(this, 1, TAG_WATER, "#water/1.png",this.callback);
		this.water.setPosition(250,20);
		this.water.setScale(0.7);	
	},
	addsalt:function(){
		var salt = new cc.Sprite("#salt1.png");
		salt.setPosition(98,250);
		this.beaker.addChild(salt,1,TAG_SHOW);
		salt.setScale(0.3,0.8);
		var move = cc.moveTo(1.5,cc.p(98,15));
		var scale = cc.scaleTo(1.5,0.9);
		var sp = cc.spawn(move,scale);
		var seq = cc.sequence(sp,cc.callFunc(function(){
			this.flowNext();
		},this));
		salt.runAction(seq);
	},
	addflow:function(obj,str,pos,apointx,apointy,time,scalex,scaley,rotate){
		var flow = new cc.Sprite(str);
		flow.setPosition(pos);
		flow.setRotation(rotate);
		flow.setAnchorPoint(apointx,apointy);
		flow.setScale(0);
		obj.addChild(flow,1);
		var seq = cc.sequence(cc.scaleTo(time,scalex,scaley),cc.delayTime(0.5),cc.fadeOut(0.5),cc.callFunc(function(){
			//flow.removeFromParent(true);
		},this));
		flow.runAction(seq);
	},
	addshow:function(obj,str,pos){
		var show = new cc.Sprite(str);
		show.setPosition(pos);
		show.setOpacity(0);
		show.setScale(2);
		obj.addChild(show,1,TAG_SHOW);
		var seq = cc.sequence(cc.fadeIn(0.5));
		show.runAction(seq);

	},
	callback:function(p){
		var action = gg.flow.flow.action;
		var func = cc.callFunc(this.actionDone, this);	
		switch(p.getTag()){	
		case TAG_WATER:
			var seq = cc.sequence(cc.rotateTo(0.5,15),cc.delayTime(2),cc.rotateTo(0.5,0));
			this.cylinder.runAction(seq);
			
			var move = cc.moveTo(0.5,cc.p(275,140));
			var rota = cc.rotateTo(0.5,-50);			
			var move1 = cc.moveTo(1,cc.p(350,20));
			var rota1 = cc.rotateTo(1,0);
			var sp1 = cc.spawn(move1,rota1);
			
			var se = cc.sequence(move,rota,cc.callFunc(function(){
				this.addflow(this.water, "#flow_right.png", cc.p(10,242), 1, 1, 0.5, 1.5, 1.5, 50);
				this.cyline.runAction(cc.moveTo(1,cc.p(64,300)));
				this.addshow(this.cylinder, "#45.png", cc.p(180,220));
				this.showtip = new ShowTip("量取45ml",cc.p(750,450));
			},this),cc.delayTime(1.5),sp1,cc.callFunc(function(){
				this.cylinder.getChildByTag(TAG_SHOW).removeFromParent(true);
				this.flowNext();
			},this));
			p.runAction(se);
			
		break;
		case TAG_CYLINDER:
			var move = cc.moveTo(1,cc.p(-35,46));
			var rota = cc.rotateTo(1,-10);
			var sp = cc.spawn(move,rota);
			this.rod.runAction(sp);
			
			var mov = cc.moveTo(1,cc.p(150,110));
			var rota = cc.rotateTo(1,-100);			
			var mov1 = cc.moveTo(1,cc.p(150,40));
			var rota1 = cc.rotateTo(1,0);
			var sp1 = cc.spawn(mov1,rota1);			
			var seq = cc.sequence(mov,rota,cc.delayTime(2),sp1,cc.callFunc(this.flowNext ,this));
            p.runAction(seq);
            
            var mo = cc.moveTo(1,cc.p(64,80));
            var ro = cc.rotateTo(1,50);
            var sca = cc.scaleTo(1,1.2);
            var spa = cc.spawn(ro,sca);
            var mo1 = cc.moveTo(0.3,cc.p(50,50));
            var sca1 = cc.scaleTo(0.3,0);
            var spa1 = cc.spawn(mo1,sca1);
            var se = cc.sequence(cc.delayTime(1),spa,cc.callFunc(function(){
            	this.addflow(this.cylinder, "#flow_right.png", cc.p(35,450), 1, 1, 1, 2.5, 2.5, 100);
            	this.beakerline.runAction(cc.moveTo(1,cc.p(96,70)));
            	this.beaker.getChildByTag(TAG_SHOW).runAction(cc.spawn(cc.fadeTo(1,200),cc.scaleTo(1,0.8,0.7)));
            },this),mo,spa1);
            this.cyline.runAction(se);
            
            
			break;
		case TAG_ROD:
			var move = cc.moveTo(0.3,cc.p(35,55));
			var rota = cc.rotateTo(0.2,0);
			var sp = cc.spawn(move,rota);
			
			var mov = cc.moveTo(0.3,cc.p(-35,46));
			var rot = cc.rotateTo(0.2,-10);
			var sp1 = cc.spawn(mov,rot);
			
			var move1 = cc.moveTo(0.3,cc.p(-25,55));
			var seq = cc.sequence(sp,move1,cc.callFunc(function(){
				this.beaker.getChildByTag(TAG_SHOW).runAction(cc.spawn(cc.fadeOut(2),cc.scaleTo(2,0)));
				this.showtip = new ShowTip("1、食盐:5g" +
						"\n2、蒸馏水:45ml" +
						"\n3、食盐质量分数:5 / (5 + 45) = 10%",cc.p(600,500));
			},this),move,move1,move,move1,sp1,cc.delayTime(5),cc.callFunc(this.flowNext ,this));
            p.runAction(seq);
			break;
		
		}
	},
	flowNext:function(){
		gg.flow.next();
	},
});