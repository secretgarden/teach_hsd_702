Balance = cc.Node.extend({
	ctor:function(p){
		this._super();
		p.addChild(this, 10, TAG_BALANCE_NODE);
		this.init();
	},
	init : function(){
		this.setCascadeOpacityEnabled(true);
		this.balance= new Button(this, 10, TAG_BALANCE, "#balance.png",this.callback);
		this.balance.setPosition(0,0);

		this.famahe= new Button(this, 11, TAG_FAMAHE, "#famahe.png",this.callback);
		this.famahe.setPosition(250,-85);
		this.famahe.setScale(0.5);	
		
		this.luomu= new Button(this, 10, TAG_LUOMU, "#luomu.png",this.callback);
		this.luomu.setPosition(140,16);
		
		this.paper= new Button(this, 10, TAG_PAPER, "#paper.png",this.callback);
		this.paper.setPosition(-250,0);		
	},
	showdisk:function(num){
		var disk = new cc.Sprite("#disk.png");
		disk.setPosition(160,340);
		this.balance.addChild(disk);
		
		var point =new cc.Sprite("#point1.png");
		point.setAnchorPoint(0.5, 0);
		point.setPosition(90,15);
		disk.addChild(point);
		var rota = cc.rotateTo(0.15,15);
		var rota1 = cc.rotateTo(0.3,-15);
		var rota2 = cc.rotateTo(0.3,15);
		var rota3 = cc.rotateTo(0.15,0);
		if(num ==1){
			var seq = cc.sequence(rota,rota1,rota2,cc.delayTime(0.5),cc.callFunc(function(){
				disk.removeFromParent(true);
				this.flowNext();
			},this));			
		}else if(num ==2 ){
			var seq = cc.sequence(rota,rota1,rota2,rota1,rota3,cc.delayTime(0.5),cc.callFunc(function(){
				disk.removeFromParent(true);
				this.flowNext();
			},this));
		}
		point.runAction(seq);
	},
	addfama:function(){		
		this.famahe.setSpriteFrame("famahe1.png");
		this.famahe.setPosition(250,-60);		
		var fama = new cc.Sprite("#fama1.png");
		fama.setPosition(98,90);
		fama.setScale(1.8);
		this.famahe.addChild(fama);
		
//		var niezi1 = new cc.Sprite("#niezi1.png");
//		niezi1.setPosition(98,90);
//		niezi1.setScale(0.8);
//		fama.addChild(niezi1);
		
		var niezi2 = new cc.Sprite("#niezi2.png");
		niezi2.setPosition(50,20);
		niezi2.setScale(0.5);
		fama.addChild(niezi2);
		
		
		var ber = cc.bezierTo(1, [cc.p(80,200),cc.p(-50,300),cc.p(-200,350)]);
		var seq = cc.sequence(ber,cc.callFunc(function(){
			this.showdisk(1);
			niezi2.removeFromParent();
			var label = new cc.LabelTTF("5g","微软雅黑",25);
			label.setPosition(40,10);
			label.setColor(cc.color(0, 0, 0, 0));
			fama.addChild(label);
		},this));
		fama.runAction(seq);
	},
	addsalt:function(){
		var salt = new cc.Sprite("#salt.png");
		salt.setPosition(50,110);
		this.paper.addChild(salt,1,TAG_SHOW);
		salt.setScale(0.2);
		var move = cc.moveTo(1,cc.p(50,30));
		var scale = cc.scaleTo(1,0.5);
		var sp = cc.spawn(move,scale);
		var seq = cc.sequence(sp,cc.callFunc(function(){
			this.showdisk(2);
		},this));
		salt.runAction(seq);
	},

	callback:function(p){
		var action = gg.flow.flow.action;
		var func = cc.callFunc(this.actionDone, this);	
		switch(p.getTag()){	
		case TAG_PAPER:
			if(action ==ACTION_DO1){
				var move = cc.moveTo(1,cc.p(0,200));
				var move1 = cc.moveTo(1,cc.p(-95,50));		
				var seq = cc.sequence(move,cc.callFunc(function(){
					this.paper1= new Button(this, 10, TAG_PAPER2, "#paper.png",this.callback);
					this.paper1.setPosition(0,200);	
					var mov = cc.moveTo(1,cc.p(100,55));
					this.paper1.runAction(mov);
				},this),move1,cc.callFunc(function(){
					this.showdisk(1);
				},this));
			}else if(action == ACTION_DO2){
				this.balance.removeFromParent(true);
				this.famahe.removeFromParent(true);
				this.luomu.removeFromParent(true);
				this.paper1.removeFromParent(true);
				ll.run.beaker.runAction(cc.moveTo(1,cc.p(600,200)));

				var ber = cc.bezierTo(1, [cc.p(-20,60),cc.p(20,80),cc.p(110,100)]);
				var rota = cc.rotateTo(1,20);
				var sp = cc.spawn(ber,rota);
				var seq = cc.sequence(sp,cc.callFunc(function(){
					p.getChildByTag(TAG_SHOW).runAction(cc.moveTo(0.3,cc.p(80,25)));
				},this),cc.delayTime(0.3),cc.callFunc(function(){
					this.removeFromParent(true);
					ll.run.beaker.addsalt();
				},this));
				
			}			
			p.runAction(seq);
		break;
		case TAG_LUOMU:
			this.showdisk(2);		
		break;
		case TAG_FAMAHE:
			this.addfama();
		break;
		}
	},
	flowNext:function(){
		gg.flow.next();
	},
});