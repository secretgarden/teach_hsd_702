var RunLayer301 = cc.Layer.extend({
	arr:null,
	scene:null,
	clock:null,
	ctor:function (parent) {
		this._super();
		this.scene = parent;
		this.scene.addChild(this, 10);
		gg.main = this;
		this.init();
	},
	init:function () {
		this.callNext = cc.callFunc(function(){
			gg.flow.next();
		}, this);
		this.callKill = cc.callFunc(function(p){
			var seq = cc.sequence(cc.fadeOut(0.5),cc.callFunc(function(){
				p.removeFromParent(true);	
			}, this));
			p.runAction(seq);
		}, this); 

		this.callNext.retain();
		this.callKill.retain();
		//时钟
		this.clock = new Clock(this);
		// 物品库
		this.lib = new Lib(this);
		this.lib.loadBg([{	
			tag:TAG_LIB_BEAKER,
			checkright:true,
		},
		{
			tag:TGA_LIB_GLASS,
		},
		{	
			tag:TAG_LIB_BALANCE,
			checkright:true,
		},
		{
			tag:TAG_LIB_CYLINDER,
			checkright:true,
		},
		{	
			tag:TAG_LIB_CONICAL,

		},
		{
			tag:TAG_LIB_YIBEI,
		},
		{
			tag:TAG_LIB_WATER,
			checkright:true,
		},
		{
			tag:TAG_LIB_FLASK,
		},
		{
			tag:TAG_LIB_NACL,
			checkright:true,
		},
		{
			tag:TAG_LIB_AMPULLA,
		}
		]);
		
		this.beaker = new Beaker03(this);
		this.beaker.setVisible(false);
		this.beaker.setPosition(850,200);
		
		this.balance = new Balance(this);
		this.balance.setVisible(false);
		this.balance.setPosition(450,250);
		
		this.salt = new Salt03(this);
		this.salt.setVisible(false);
		this.salt.setPosition(850,500);

	},
	checkVisible:function(next){
		//是否可见
		var checkVisible = [];
		var node1 = ll.run.beaker;
		var node2 = ll.run.balance;
		var node3 = ll.run.salt;

		checkVisible.push(node1,node2,node3);

		for(var i in checkVisible){
			if(checkVisible[i] !== null){				
				checkVisible[i].setVisible(next);				
			}			
		}
	},
	loadInLib:function(obj, pos, tarPos,delay){
		obj.setPosition(pos);
		if(delay == null){
			delay = 1;
		}
		var ber = $.bezier(pos, tarPos, delay);
		var seq = cc.sequence(ber, this.callNext);
		obj.runAction(seq);
	},
	kill:function(obj){
		var fade = cc.fadeTo(0.5,0);
		var func = cc.callfunc(function(){
			obj.removeFromParent(true);
		},this);
		var seq = cc.sequence(fade,func);
		obj.runAction(seq)
	},

	callback:function (p){
		var func = cc.callFunc(this.actionDone, this);
		var action=gg.flow.flow.action;
		switch(p.getTag()){

		}
	},
	actionDone:function(p){
		var func = cc.callFunc(this.actionDone, this);
		switch(p.getTag()){

		}
	},
	flowNext:function(){
		gg.flow.next();
	},
	onExit:function(){
		this._super();
		this.callNext.release();
		this.callKill.release();
	}
});