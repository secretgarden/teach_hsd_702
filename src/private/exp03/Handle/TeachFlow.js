startRunNext03 = function(tag){
	switch(tag){
	case TAG_YINDAO:
	case TAG_SHIZHAN:
		ch.gotoRun(TAG_EXP_03, g_resources_run03, null, new RunScene03());
		break;
	case TAG_QUWEI:
		gg.hJson = hJson03;
		ch.gotoGame(TAG_EXP_03, g_resources_public_game, res_public_game.game_p);
		break;
//		case TAG_TEST:
//		ch.gotoTest(TAG_EXP_03, g_resources_test03, null, res_test03.subject);
//		break;
	case TAG_ABOUT:
		ch.gotoAbout(TAG_EXP_03, g_resources_public_about, null, res_public_about.about_j);
		break;
	}
}

//任务流
teach_flow03 = 
	[
	 {
		 tip:"选择烧杯",
		 tag:[
		      TAG_LIB,
		      TAG_LIB_BG1,	   
		      ],
		      canClick:true		 
	 },
	 {
		 tip:"选择托盘天平",
		 tag:[
		      TAG_LIB,
		      TAG_LIB_BG3,	   
		      ],
		      canClick:true		 
	 },
	 {
		 tip:"选择量筒",
		 tag:[
		      TAG_LIB,
		      TAG_LIB_BG4,	   
		      ],
		      canClick:true		 
	 },

	 {
		 tip:"选择食盐",
		 tag:[
		      TAG_LIB,
		      TAG_LIB_BG9,	   
		      ],
		      canClick:true		 
	 },
	 {
		 tip:"选择水壶",
		 tag:[
		      TAG_LIB,
		      TAG_LIB_BG7,	   
		      ],
		      canClick:true		 
	 },

	 {
		 tip:"点击确定，开始实验",
		 tag:[
		      TAG_LIB,
		      TAG_SURE
		      ]
	 },
	 {
		 tip:"在托盘天平两端放上滤纸",
		 tag:[
		      TAG_BALANCE_NODE,
		      TAG_PAPER
		      ],action:ACTION_DO1
	 },
	 {
		 tip:"调节平衡螺母，使天平平衡",
		 tag:[
		      TAG_BALANCE_NODE,
		      TAG_LUOMU
		      ]
	 },
	 {
		 tip:"在天平右盘放上5克的砝码",
		 tag:[
		      TAG_BALANCE_NODE,
		      TAG_FAMAHE
		      ]
	 },
	 {
		 tip:"取下食盐样品盖子",
		 tag:[
		      TAG_SALT_NODE,
		      TAG_LID
		      ]
	 },
	 {
		 tip:"用药勺取5g食盐，放到天平左盘",
		 tag:[
		      TAG_SALT_NODE,
		      TAG_SPOON
		      ]
	 },
	 {
		 tip:"将食盐放到烧杯中",
		 tag:[
		      TAG_BALANCE_NODE,
		      TAG_PAPER
		      ],action:ACTION_DO2
	 },
	 {
		 tip:"用蒸馏水壶往量筒中倒入45ml蒸馏水",
		 tag:[
		      TAG_BEAKER_NODE,
		      TAG_WATER
		      ]
	 },
	 {
		 tip:"将量筒中的水倒入烧杯中",
		 tag:[
		      TAG_BEAKER_NODE,
		      TAG_CYLINDER
		      ]
	 },
	 {
		 tip:"用玻璃棒搅拌、使食盐溶解",
		 tag:[
		      TAG_BEAKER_NODE,
		      TAG_ROD
		      ]
	 },
	 
	 {
		 tip:"恭喜过关",
		 over:true

	 }
	 ]
//游戏
hJson03 = [
           {tag:TAG_H1,image:"prop1.png",name:"量筒"},
           {tag:TAG_H2,image:"prop2.png",name:"胶头滴管"},
           {tag:TAG_H3,image:"prop3.png",name:"蒸发皿"},
           {tag:TAG_H4,image:"prop4.png",name:"表面皿"},
           {tag:TAG_H5,image:"prop5.png",name:"酒精灯"},
           {tag:TAG_H6,image:"prop6.png",name:"洗耳球"}
           ]



