startRunNext10 = function(tag){
	switch(tag){
	case TAG_YINDAO:
	case TAG_SHIZHAN:
		ch.gotoRun(TAG_EXP_10, g_resources_run10, null, new RunScene10());
		break;
	case TAG_QUWEI:
		gg.hJson = hJson10;
		ch.gotoGame(TAG_EXP_10, g_resources_public_game, res_public_game.game_p);
		break;
//		case TAG_TEST:
//		ch.gotoTest(TAG_EXP_10, g_resources_test10, null, res_test10.subject);
//		break;
	case TAG_ABOUT:
		ch.gotoAbout(TAG_EXP_10, g_resources_public_about, null, res_public_about.about_j);
		break;
	}
}
//任务流
teach_flow10 = 
	[
	 {
		 tip:"选择粗盐试剂瓶",
		 tag:[
		      TAG_LIB,
		      TAG_LIB_BG1,	   
		      ],
		      canClick:true
	 },
	 {
		 tip:"选择烧杯",
		 tag:[
		      TAG_LIB,
		      TAG_LIB_BG3,
		      ],
		      canClick:true
	 },
	 {
		 tip:"选择过滤装置",
		 tag:[
		      TAG_LIB,
		      TAG_LIB_BG4,
		      ],
		      canClick:true
	 },
	 {
		 tip:"选择蒸发装置",
		 tag:[
		      TAG_LIB,
		      TAG_LIB_BG10,
		      ],
		      canClick:true
	 },	
	 {
		 tip:"选择酒精灯",
		 tag:[
		      TAG_LIB,
		      TAG_LIB_BG9,
		      ],
		      canClick:true
	 },	
	 {
		 tip:"选择玻璃棒",
		 tag:[
		      TAG_LIB,
		      TAG_LIB_BG7,
		      ],
		      canClick:true
	 },	
	 {
		 tip:"点击确定，开始实验",
		 tag:[
		      TAG_LIB,
		      TAG_SURE
		      ]
	 },
	 {
		 tip:"取出一勺粗盐，加入烧杯溶解",
		 tag:[
		      TAG_SALT,
		      TAG_SALT_BOTTLE
		      ]
	 },
	 {
		 tip:"使用玻璃棒搅拌，使粗盐溶解完全",
		 tag:[
		      TAG_ROD,
		      TAG_ROD1
		      ],action:ACTION_DO1
	 },
	 {
		 tip:"使用玻璃棒引流，通过过滤漏斗过滤",
		 tag:[
		      TAG_ROD,
		      TAG_ROD1
		      ],action:ACTION_DO2
	 },
	 {
		 tip:"取出烧杯，观察溶液，并将溶液倒至蒸发皿",
		 tag:[
		      TAG_BEAKER,
		      TAG_BEAKER2
		      ]
	 },
	 {
		 tip:"点燃酒精灯，加热",
		 tag:[
		      TAG_LAMP,
		      TAG_LAMP_BOTTLE
		      ],action:ACTION_DO1
	 },
	 {
		 tip:"使用玻璃棒搅拌，防止局部温度过高导致液体飞溅",
		 tag:[
		      TAG_ROD,
		      TAG_ROD1
		      ],action:ACTION_DO3
	 },
	 {
		 tip:"待蒸发皿中较多固体时，停止加热",
		 tag:[
		      TAG_LAMP,
		      TAG_LAMP_BOTTLE
		      ],action:ACTION_DO2
	 },
	 {
		 tip:"观察蒸发皿",
		 tag:[
		      TAG_SHELF4,
		      TAG_SHELF5
		      ],X:0.3,Y:0.3
	 },
	 {
		 tip:"恭喜过关",
		 over:true
	 }
	 ]
//游戏
hJson10 = [
           {tag:TAG_H1,image:"prop1.png",name:"量筒"},
           {tag:TAG_H2,image:"prop2.png",name:"胶头滴管"},
           {tag:TAG_H3,image:"prop3.png",name:"蒸发皿"},
           {tag:TAG_H4,image:"prop4.png",name:"表面皿"},
           {tag:TAG_H5,image:"prop5.png",name:"酒精灯"},
           {tag:TAG_H6,image:"prop6.png",name:"洗耳球"}
           ]





