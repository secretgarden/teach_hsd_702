/**
 * 酒精灯
 */
Lamp = cc.Node.extend({
	ctor:function(parent){
		this._super();
		parent.addChild(this,21);
		this.setTag(TAG_LAMP);
		this.init();
	},
	init : function(){
		this.setVisible(false);
		this.setCascadeOpacityEnabled(true);
		//酒精灯瓶身
		var bottle = new Button(this, 10, TAG_LAMP_BOTTLE, "#lampBottle.png",this.callback);
		bottle.setPosition(cc.p(964,169));
		bottle.setScale(0.6);
		//酒精灯灯帽
		var lid = new Button(bottle, 11, TAG_LAMP_LID, "#lampLid.png",this.callback,this);
		lid.setPosition(cc.p(102,223));
		//火柴
		var match = new Button(this, 11, TAG_LAMP_MATCH, "#match.png",this.callback);
		match.setPosition(cc.p(1059,130));
		match.setScale(0.12);
	},	
	fire:function(p){
		var fire = new cc.Sprite("#fire/fire1.png");
		p.addChild(fire,3,TAG_FIRE);
		fire.setPosition(cc.p(99,273));
		fire.setScale(2.5);

		var animFrames = [];
		for (var i = 1; i < 4; i++) {
			var str = "fire/fire" + i + ".png";
			var frame = cc.spriteFrameCache.getSpriteFrame(str);
			animFrames.push(frame);
		}
		var animation = new cc.Animation(animFrames, 0.05);
		var action = cc.animate(animation);
		var repeat = cc.repeatForever(action);
		fire.runAction(repeat);
	},
	callback:function(p){
		var action = gg.flow.flow.action;
		switch(p.getTag()){
		case TAG_LAMP_BOTTLE:
			if(action  == ACTION_DO1){
				var mo1 = cc.moveTo(1, cc.p(634,269));
				var mo2 = cc.moveTo(1, cc.p(998,168));
				var se = cc.sequence(cc.callFunc(function() {
					//火柴盒
					var match = this.getChildByTag(TAG_LAMP_MATCH);
					var mov = cc.moveTo(1, cc.p(759,226));
					var seq = cc.sequence(cc.delayTime(0.3),mov,cc.delayTime(1.5),cc.callFunc(function() {
						//单根火柴
						var match2 = new cc.Sprite("#match2.png");
						match.addChild(match2, 2);
						match2.setScale(2);
						match2.setPosition(cc.p(658,617));
						match2.runAction(cc.sequence(cc.moveTo(0.5, cc.p(968,453)),cc.callFunc(function() {
							//火柴点燃
							match2.setSpriteFrame("match1.png");
						}, this),cc.moveTo(0.5, cc.p(984, 515)),cc.moveTo(1, cc.p(-350,1295)),cc.callFunc(function() {
							//酒精灯点燃
							this.fire(p);	
							match.removeFromParent();
						}, this)))										
					}, this));
					match.runAction(seq);
				}, this),mo1,cc.callFunc(function() {
					//灯帽
					var lid = p.getChildByTag(TAG_LAMP_LID);
					var move = cc.moveTo(0.5, cc.p(102,244));
					var bezier = cc.bezierBy(1,[cc.p(-120,120),cc.p(-250,-80),cc.p(-250,-170)]);
					var sequence = cc.sequence(move,bezier,cc.callFunc(function() {
						lid.removeFromParent();		
						var lid2 = new Button(this, 25, TAG_LAMP_LID2, "#lampLid.png",this.callback);
						lid2.setScale(0.6);
						lid2.setPosition(cc.p(483,244));
					}, this));
					lid.runAction(sequence);
				}, this),cc.delayTime(5),mo2,cc.callFunc(function() {
					//调整蒸发皿高度
					var shelf5 =this.getParent().getChildByTag(TAG_SHELF4).getChildByTag(TAG_SHELF5);
					shelf5.runAction(cc.moveBy(1, 0,-35));
				}, this),cc.delayTime(1.2),cc.callFunc(function() {
					gg.flow.next();
				}, this));
				p.runAction(se);	
			}else if(action == ACTION_DO2){
				var move = cc.moveTo(1, cc.p(556,168));
				var sequ = cc.sequence(cc.callFunc(function() {
					//灯帽
					var lid = this.getChildByTag(TAG_LAMP_LID2);
					var mov = cc.moveTo(1, cc.p(521,244));
					var rot = cc.rotateTo(1, -35);
					var spa = cc.spawn(mov,rot);
					var mov1 = cc.moveTo(0.3, cc.p(546,244));
					var mov2 = cc.moveTo(0.5, cc.p(555,229));
					var rot2 = cc.rotateTo(0.5, 0);
					var spa2 = cc.spawn(mov2,rot2);
					var mov3 = cc.moveBy(1,0,30);
					var mov4 = cc.moveBy(1,0,-30);
					var seq = cc.sequence(cc.delayTime(0.7),spa,mov1,cc.callFunc(function() {
						//火焰熄灭
						var fire = this.getChildByTag(TAG_LAMP_BOTTLE).getChildByTag(TAG_FIRE);
						fire.removeFromParent();
					}, this),spa2,cc.delayTime(0.5),mov3,cc.delayTime(0.3),mov4,cc.callFunc(function() {
						gg.flow.next();
						this.removeFromParent();
					}, this));
					lid.runAction(seq);
				}, this),move);
				p.runAction(sequ);
			}
			break;
		default:
			break;
		}
	}
});
