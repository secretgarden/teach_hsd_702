/**
 * 粗盐样品试剂瓶
 */
Salt = cc.Node.extend({
	ctor:function(parent){
		this._super();
		parent.addChild(this,30);
		this.setTag(TAG_SALT);
		this.init();
	},
	init : function(){
		this.setVisible(false);
		this.setCascadeOpacityEnabled(true);

		//瓶身
		var bottle = new Button(this, 5, TAG_SALT_BOTTLE, "#crudeSaltBottle.png",this.callback);
		bottle.setPosition(cc.p(gg.width*0.45  , gg.height*0.6+50));
		bottle.setScale(0.4);
		
		//瓶塞
		var lid = new Button(this, 2, TAG_SALT_LID, "#crudeSaltlid.png",this.callback);
		lid.setPosition(cc.p(gg.width*0.45  , gg.height*0.6+112));
		lid.setScale(0.4);
		
		//药匙
		var spoon = new Button(this, 3, TAG_SALT_SPOON, "#spoon.png",this.callback);
		spoon.setPosition(cc.p(gg.width*0.6,gg.height*0.6));
		spoon.setRotation(60);

		this.callNext = cc.callFunc(function(){
			gg.flow.next();
		}, this);
		this.callNext.retain();
	},	
	//左边玻璃管气泡
	bubble1:function(){

	},
	//右边玻璃管气泡
	bubble2:function(){

	},
	callback:function(p){
		var action = gg.flow.flow.action;
		switch(p.getTag()){
		case TAG_SALT_BOTTLE:
			//瓶塞打开
			var lid = this.getChildByTag(TAG_SALT_LID);
			var ber = cc.bezierBy(1.5,[cc.p(-60,120),cc.p(-80,-80),cc.p(-80,-110)]);
			var rotate = cc.rotateTo(1.5, -180);
			var spawn = cc.spawn(ber, rotate);
			lid.runAction(spawn);
			//瓶身			
			var mov = cc.moveTo(1, cc.p(596,530));
			var rot = cc.rotateTo(1, 50);
			var spa = cc.spawn(mov,rot);
			var mov1 = cc.moveTo(1, cc.p(576,510));
			var rot1 = cc.rotateTo(1, 0);
			var spa1 = cc.spawn(mov1,rot1);			
			var seq = cc.sequence(cc.delayTime(1.5),cc.callFunc(function() {
				//药匙
				var spoon = this.getChildByTag(TAG_SALT_SPOON);
				var moveto = cc.moveTo(1, cc.p(736,626));
				var rotateto = cc.rotateTo(1, 25);
				var spawn = cc.spawn(moveto,rotateto);
				var moveto1 = cc.moveTo(1, cc.p(639,564));
				var moveto2 = moveto.clone();
				var moveto3 = cc.moveTo(1, cc.p(760,296));
				var sequence = cc.sequence(spawn,moveto1,cc.callFunc(function() {
					spoon.setSpriteFrame("spoon2.png");
				}, this),moveto2,moveto3,cc.callFunc(function() {
					spoon.setSpriteFrame("spoon.png");
					//粗盐倒入烧杯				
					var sand = this.getParent().getChildByTag(TAG_BEAKER).getChildByTag(TAG_BEAKER1).getChildByTag(TAG_SAND1);
					sand.setVisible(true);
					var mo1 = cc.moveTo(1, cc.p(150,47));
					var mo2 = cc.moveTo(1, cc.p(150,20));
					var sca = cc.scaleTo(1, 0.7,0.6);
					var sp = cc.spawn(mo2,sca);
					var se = cc.sequence(mo1,cc.callFunc(function() {
						var liquid = this.getParent().getChildByTag(TAG_BEAKER).getChildByTag(TAG_BEAKER1).getChildByTag(TAG_BEAKER_LIQUID);
						liquid.runAction(cc.fadeTo(1, 120));
					}, this),sp);
					sand.runAction(se);
				}, this));
				spoon.runAction(sequence);
			}, this),spa,cc.delayTime(3),spa1,cc.delayTime(2),cc.callFunc(function() {
				gg.flow.next();
				this.removeFromParent();
			},this));
			p.runAction(seq);
			break;
		case TAG_SALT_LID:
			
			break;
		}
	}
});
