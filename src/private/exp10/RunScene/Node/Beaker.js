/**
 * 烧杯
 */
Beaker = cc.Node.extend({
	ctor:function(parent){
		this._super();
		parent.addChild(this,21);
		this.setTag(TAG_BEAKER);
		this.init();
	},
	init : function(){
		this.setVisible(false);
		this.setCascadeOpacityEnabled(true);
		//烧杯1号（装有水）
		var beaker1 = new Button(this, 10, TAG_BEAKER1, "#beaker.png",this.callback);
		beaker1.setPosition(cc.p(686,184));
		beaker1.setScale(0.5);
		//1号水位线
		var beaker_line1 = new Button(beaker1, 10, TAG_BEAKER_LINE1, "#line.png",this.callback,this);
		beaker_line1.setPosition(cc.p(120,52));
		//粗盐（带泥沙）
		var sand1 = new cc.Sprite("#sand2.png");
		beaker1.addChild(sand1, 20,TAG_SAND1);
		sand1.setPosition(cc.p(150,249));
		sand1.setScale(0.5, 1);
		sand1.setVisible(false);
		//加泥土后的溶液
		var liquid = new Button(beaker1, 10, TAG_BEAKER_LIQUID, "#liquid.png",this.callback,this);
		liquid.setPosition(cc.p(120,27));
		liquid.setScale(1,0.5);
		liquid.setOpacity(0);
		//烧杯2号（铁架台上）
		var beaker2 = new Button(this, 10, TAG_BEAKER2, "#beaker2.png",this.callback);
		beaker2.setPosition(cc.p(406,177));
		beaker2.setScale(0.5);
		//2号水位线（初始不可见）
		var beaker_line2 = new Button(beaker2, 10, TAG_BEAKER_LINE2, "#line.png",this.callback,this);
		beaker_line2.setPosition(cc.p(103,20));	
		beaker_line2.setOpacity(0);
	},	
	callback:function(p){
		var action = gg.flow.flow.action;
		switch(p.getTag()){
		case TAG_BEAKER2:
			//漏斗上移
			var funnel_body = this.getParent().getChildByTag(TAG_SHELF2);
			var funnel_part = this.getParent().getChildByTag(TAG_SHELF1).getChildByTag(TAG_SHELF3);
			var moveby1 = cc.moveBy(1, 0, 85);
			var moveby2 = cc.moveBy(1, 0, 122);
			funnel_body.runAction(cc.sequence(moveby1,cc.delayTime(0.5),cc.callFunc(function() {
				funnel_body.removeFromParent();
			}, this)));
			funnel_part.runAction(cc.sequence(moveby2,cc.delayTime(0.5),cc.callFunc(function() {
				var shelf = this.getParent().getChildByTag(TAG_SHELF1);
				shelf.removeFromParent();
			}, this)));
			//2号烧杯倒至蒸发装置
			var move = cc.moveTo(1,	cc.p(855,425));
			var rotate1 = cc.rotateTo(2, 95);
			var move1 = cc.moveTo(2,cc.p(888,425));
			var spawn1 = cc.spawn(rotate1,move1);
			var move2 = cc.moveTo(1, cc.p(855,425));
			var rotate2 = cc.rotateTo(1, 0);
			var spawn2 = cc.spawn(move2,rotate2);
			var seq = cc.sequence(cc.delayTime(1.3),move,cc.callFunc(function() {
				//2号烧杯水平面
				var line2 = p.getChildByTag(TAG_BEAKER_LINE2);
				var ro = cc.rotateTo(0.5, -25);
				var mo1 = cc.moveTo(0.5, cc.p(129,68));
				var ro1 = cc.rotateTo(0.5, -45);
				var sp1 = cc.spawn(mo1,ro1);
				var mo2 = cc.moveTo(1, cc.p(171,106));
				var ro2 = cc.rotateTo(1, -85);
				var sp2 = cc.spawn(mo2,ro2);
				var mo3 = cc.moveTo(1, cc.p(185,219));
				var ro3 = cc.rotateTo(1, -85);
				var sc3 = cc.scaleTo(1, 0.1,0.1);
				var sp3 = cc.spawn(mo3,ro3,sc3);
				var fa = cc.fadeOut(0);
				var se = cc.sequence(ro,sp1,sp2,sp3,fa);
				line2.runAction(se);
			}, this),spawn1,cc.delayTime(2.5),spawn2,cc.callFunc(function() {
				gg.flow.next();
				p.removeFromParent();
			}, this));
			p.runAction(seq);
			break;
		default:
			break;
		}
	}
});
