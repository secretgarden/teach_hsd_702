/**
 * 玻璃棒
 */
Rod = cc.Node.extend({
	ctor:function(parent){
		this._super();
		parent.addChild(this,22);
		this.setTag(TAG_ROD);
		this.init();
	},
	init : function(){
		this.setVisible(false);
		this.setCascadeOpacityEnabled(true);
		//玻璃棒
		var rod = new Button(this, 10, TAG_ROD1, "#rod.png",this.callback);
		rod.setPosition(cc.p(gg.width*0.5+2 , gg.height*0.355));
		rod.setScale(0.5);
		rod.setRotation(80);
	},	
	callback:function(p){
		var action = gg.flow.flow.action;
		switch(p.getTag()){
		case TAG_ROD1:
			if(action == ACTION_DO1){
				//玻璃棒搅拌充分溶解
				var moveto = cc.moveTo(0.6, cc.p(689,280));
				var rotate = cc.rotateTo(0.6, 90);
				var spawn = cc.spawn(moveto,rotate);
				var moveto1 = cc.moveTo(0.3, cc.p(661,286));
				var moveto2 = cc.moveTo(0.6, cc.p(710,286));
				var moveto3 = cc.moveTo(0.3, cc.p(684,280));
				var action = cc.sequence(moveto1,moveto2,moveto3).repeat(4);
				var moveto4 = cc.moveTo(0.6, cc.p(642,gg.height*0.355));
				var rotate2 = cc.rotateTo(0.6, 80);
				var spawn2 = cc.spawn(moveto4,rotate2);
				var sequence = cc.sequence(spawn,cc.callFunc(function() {
					//泥沙溶液的变化
					var liquid = this.getParent().getChildByTag(TAG_BEAKER).getChildByTag(TAG_BEAKER1).getChildByTag(TAG_BEAKER_LIQUID);
					liquid.runAction(cc.fadeTo(3, 255));
					//泥沙颗粒的变化
					var sand = this.getParent().getChildByTag(TAG_BEAKER).getChildByTag(TAG_BEAKER1).getChildByTag(TAG_SAND1);
					var move1 = cc.moveTo(0.3, cc.p(128,18));
					var move2 = cc.moveTo(0.3, cc.p(93,21));		
					var move3 = cc.moveTo(0.6, cc.p(150,20));
					var move4 = cc.moveTo(0.3, cc.p(128,20));
					var scale1 = cc.scaleTo(2, 0.3,0.6);
					var scale2 = cc.scaleTo(3, 0.8,0.6);
					var seq = cc.sequence(move1,move2,move3,move4,scale1,cc.delayTime(1),scale2);
					sand.runAction(seq);
				}, this),action,spawn2,cc.delayTime(1),cc.callFunc(function() {
					gg.flow.next();
				}, this));
				p.runAction(sequence);
			}else if(action == ACTION_DO2){
				//引流、过滤
				var move1 = cc.moveTo(0.8, cc.p(642,420));
				var move2 = cc.moveTo(0.8, cc.p(406,550));
				var rotate = cc.rotateTo(0.8, 75);
				var spawn = cc.spawn(move2,rotate);
				var move3 = cc.moveTo(0.8, cc.p(300,480));
				var move4 = move2.clone();
				var move5 = cc.moveTo(1, cc.p(680,100));
				var rotate2 = cc.rotateTo(1, 0);
				var spawn3 = cc.spawn(move5,rotate2);
				var seq = cc.sequence(move1,cc.callFunc(function() {
					//1号烧杯过滤
					var beaker1 = this.getParent().getChildByTag(TAG_BEAKER).getChildByTag(TAG_BEAKER1);
					var mov1 = cc.moveTo(1.5, cc.p(383,500));
					var mov2 = cc.moveTo(2, cc.p(358,500));
					var rot = cc.rotateTo(2, -95);
					var spa = cc.spawn(mov2,rot);
					var mov3 = mov1.clone();
					var rot2 = cc.rotateTo(1, 0);
					var spa2 = cc.spawn(mov3,rot2);
					var seq = cc.sequence(mov1,cc.callFunc(function() {
						//泥沙溶液的变化
						var liquid = this.getParent().getChildByTag(TAG_BEAKER).getChildByTag(TAG_BEAKER1).getChildByTag(TAG_BEAKER_LIQUID);
						var danchu = cc.fadeOut(1.5);
						var suoxiao = cc.scaleTo(1.5, 0.25,0.25);
						var yidong1 = cc.moveTo(0.8,cc.p(81,17));
						var yidong2 = cc.moveTo(0.7,cc.p(61,35));
						var act = cc.sequence(yidong1,yidong2);
						var action = cc.spawn(danchu,suoxiao,act);
						liquid.runAction(action);
						//1号烧杯溶液水平面的变化
						var line1 = this.getParent().getChildByTag(TAG_BEAKER).getChildByTag(TAG_BEAKER1).getChildByTag(TAG_BEAKER_LINE1);
						var ro = cc.rotateTo(0.5, 25);
						var mo1 = cc.moveTo(0.5, cc.p(92,65));
						var ro1 = cc.rotateTo(0.5, 45);
						var sp1 = cc.spawn(mo1,ro1);
						var mo2 = cc.moveTo(0.5, cc.p(59,93));
						var ro2 = cc.rotateTo(0.5, 85);
						var sp2 = cc.spawn(mo2,ro2);
						var mo3 = cc.moveTo(1, cc.p(39,210));
						var ro3 = cc.rotateTo(1, 85);
						var sc3 = cc.scaleTo(1, 0.1,0.1);
						var sp3 = cc.spawn(mo3,ro3,sc3);
						var fa = cc.fadeOut(0);
						var se = cc.sequence(ro,sp1,sp2,sp3,fa);
						line1.runAction(se);						
						//泥沙颗粒的变化
						var sand = this.getParent().getChildByTag(TAG_BEAKER).getChildByTag(TAG_BEAKER1).getChildByTag(TAG_SAND1);
						var moveto1 = cc.moveTo(0.7, cc.p(48,30));
						var scaleto1 = cc.scaleTo(0.7,0.3,0.5 );
						var spawn1 = cc.spawn(moveto1,scaleto1);
						var moveto2 = cc.moveTo(0.5, cc.p(43,52));
						var scaleto2 = cc.scaleTo(0.5,0.1,2 );
						var spawn2 = cc.spawn(moveto2,scaleto2);
						var seq = cc.sequence(cc.delayTime(0.5),spawn1,spawn2);
						sand.runAction(seq);
						//2号烧杯水平面变化
						var line2 = this.getParent().getChildByTag(TAG_BEAKER).getChildByTag(TAG_BEAKER2).getChildByTag(TAG_BEAKER_LINE2);
						line2.runAction(cc.sequence(cc.delayTime(0.5),cc.fadeIn(0.5),cc.moveBy(3,0,37)));
					}, this),spa,cc.delayTime(2),cc.callFunc(function() {
						//1号烧杯过滤结束，泥沙回到初始状态
						var sand = this.getParent().getChildByTag(TAG_BEAKER).getChildByTag(TAG_BEAKER1).getChildByTag(TAG_SAND1);
						var mov = cc.moveTo(1, cc.p(65,27));
						var sca = cc.scaleTo(1, 0.5,1);
						var spa = cc.spawn(mov,sca);
						sand.runAction(spa);
					}, this),spa2,cc.delayTime(0.5),cc.callFunc(function() {
						beaker1.removeFromParent();
					}, this));
 					beaker1.runAction(seq);
				}, this),spawn,move3,cc.delayTime(6),move4,spawn3,cc.callFunc(function() {
					gg.flow.next();
				}, this));
				p.runAction(seq);
			}else if(action == ACTION_DO3){
				//玻璃棒
				var move1 = cc.moveTo(1, cc.p(680,560));
				var rota1 = cc.rotateTo(1, 90);
				var spaw1 = cc.spawn(move1,rota1);				
				var move2 = cc.moveTo(0.6, cc.p(741,488));
				var move5 = cc.moveTo(0.3, cc.p(715,492));
				var move3 = cc.moveTo(0.3, cc.p(741,488));
				var move4 = cc.moveTo(0.3, cc.p(760,487));
				var action = cc.sequence(move5,move3,move4,move3).repeat(3);
				var sequ = cc.sequence(cc.callFunc(function() {
					var show = new ShowTip("加热时玻璃棒搅拌：\n防止局部温度过高，导致液体飞溅。",cc.p(450,450));
					//蒸发装置移动
					var shelf = this.getParent().getChildByTag(TAG_SHELF4);
					var mov1 = cc.moveBy(1, -256,0);
					shelf.runAction(cc.sequence(cc.delayTime(0.5),mov1));
					//酒精灯移动
					var lamp = this.getParent().getChildByTag(TAG_LAMP).getChildByTag(TAG_LAMP_BOTTLE);
					var mov2 = mov1.clone();
					lamp.runAction(cc.sequence(cc.delayTime(0.5),mov2));				
				}, this),spaw1,cc.delayTime(0.8),move2,action,cc.delayTime(0.2),cc.callFunc(function() {
					gg.flow.next();
					p.removeFromParent();
				}, this));
				p.runAction(sequ);
			}	
			break;
			default:
				break;
		}
	}
});
