exp01.ShowMainLayer02 = cc.Layer.extend({
	lead:null,
	real:null,
	curIndex: 0,
	ctor:function () {
		this._super();
		this.init();
		this.turnmenu();
		this.scheduleUpdate();
		return true;
	},
	init:function(){
		this.pageView = new ccui.PageView();//建立pageview
		this.pageView.setSwallowTouches(false);
		this.pageView.setContentSize(cc.size(980, 600));//pageview展示框的大小
		this.pageView.setPosition(640,338);//pageview的坐标
		this.pageView.setAnchorPoint(0.5,0.5);//默认锚点是（0,0）
		this.addChild(this.pageView);
		
		this.loadLayer1();
		this.loadLayer2();
	},	
	loadLayer1:function(){
		this.layout1 = new ccui.Layout();//第一页
		this.layout1.setContentSize(cc.size(980, 600));//设置layout的大小
		this.pageView.addPage(this.layout1);

		this.label1 = new cc.LabelTTF("花的开放主要受外界环境中光照和温度的影响." +
				"\n大多数植物的开花需要光,在白天开放；" +
				"\n但也有一些植物的开花是在清晨、傍晚或晚上；" +
				"\n植物开花对温度也有要求。一般来说,温度适宜或较高时,"+
				"\n花朵开放较快;温度较低时,花朵开放较迟缓",gg.fontName,gg.fontSize4);
		this.label1.setPosition(this.layout1.width/2,this.layout1.height-this.label1.height/2);
		this.layout1.addChild(this.label1);	

		this.playbutton(cc.p(180,350));//展示按钮
		
//		this.flower = new cc.Sprite("#flower2/flower_1.jpg");
		this.flower = new cc.Sprite("res/private/exp01/flower/flower_1.jpg");
		this.layout1.addChild(this.flower);
		this.flower.setPosition(490,250);
		
		var animFrames = [];
		for (var i = 1; i < 27; i++) {
//			var str = "flower2/flower_" + i + ".jpg";
//			var frame = cc.spriteFrameCache.getSpriteFrame(str);
			var str = "res/private/exp01/flower/flower_" + i + ".jpg";
			var frame = new cc.SpriteFrame(str, cc.rect(0, 0, 366, 270));
			animFrames.push(frame);
		}
		var animation = new cc.Animation(animFrames, 0.1);
		// 跑动
		this.floweringAction = cc.Animate.create(animation);
		this.floweringAction.retain();// jsb BUG
	},
	loadLayer2:function(){
		this.layout2 = new ccui.Layout();//第一页
		this.layout2.setContentSize(cc.size(980, 600));//设置layout的大小
		this.pageView.addPage(this.layout2);

		this.label1 = new cc.LabelTTF("传粉一般分为自花传粉和异花传粉." +
				"\n为了弥补自然状态下的传粉不足,\n人们常常给异花传粉的植物进行辅助授粉",gg.fontName,gg.fontSize4);
		this.label1.setPosition(this.layout2.width/2,this.layout2.height-this.label1.height/2);
		this.layout2.addChild(this.label1);	

		var flower2 = new Angel(this.layout2, res_show01.flower1,this.callback,this);
		flower2.setPosition(490, 250);

	},
	turnmenu:function(){
		this.turnleft = new ButtonScale(this,res_public.leftbutton,this.callback);
		this.turnleft.setPosition(80,376-50);
		this.turnleft.setVisible(false);       
		this.turnright = new ButtonScale(this,res_public.rightbutton,this.callback);
		this.turnright.setPosition(1200,376-50);   	    	
	},
	playbutton:function(pos){
		this.play = new Angel(this.layout1,"#unsel.png",this.callback,this);
		this.play.setScale(0.3,0.5);
		this.play.setPosition(pos);
		this.play.setEnable(true);
		var label = new cc.LabelTTF("开花",gg.fontName,50);
		label.setPosition(this.play.width/2,this.play.height/2);
		this.play.addChild(label);
	},
	update:function(){//用pageview翻页暂时没找到好的更新方法
		var index = this.pageView.getCurPageIndex();//获取当前页，以0 1 2 形式
		if(this.curIndex != index){
			if(index == 0){//当从第二页滑到第一页时，继续左滑，左滑按钮隐藏（滑到第一页）
				this.turnleft.setVisible(false);
				this.turnright.setVisible(true);
			}else if(index == 1){//当从第倒数第二页滑到最后页时，继续右滑，右滑按钮隐藏，根据自己创建的页数，手动调整（滑到最后页）
				this.turnleft.setVisible(true);
				this.turnright.setVisible(false);
			}else{
				this.turnleft.setVisible(true);
				this.turnright.setVisible(true);
			}  
			this.curIndex = index;
		}			
	},
	callback:function(p){
		switch(p){		
		case this.turnleft:
			var index = this.pageView.getCurPageIndex();
			this.pageView.scrollToPage(index - 1);
		    break;
		case this.turnright:
			var index = this.pageView.getCurPageIndex();			
			this.pageView.scrollToPage(index + 1);
			break;
		case this.play:
			p.setSpriteFrame("sel.png");
			p.setEnable(false);
			var seq = cc.sequence(this.floweringAction,cc.callFunc(function(){
				p.setSpriteFrame("unsel.png");
				p.setEnable(true);
			},this)); 
			this.flower.runAction(seq);
			break;
		case this.yuanyinnutton:
			p.setSpriteFrame("sel.png");
			p.setEnable(false);
			this.label4.setVisible(true);
		break;
		}
	},
	onExit:function(){
		this._super();
		this.floweringAction.release();
	}
});
