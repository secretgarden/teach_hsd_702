exp01.ShowMainLayer02 = cc.Layer.extend({
	lead:null,
	real:null,
	curIndex: 0,
	ctor:function () {
		this._super();
		this.init();
		this.turnmenu();
		this.scheduleUpdate();
		return true;
	},
	init:function(){
		this.pageView = new ccui.PageView();//建立pageview
		this.pageView.setSwallowTouches(false);
		this.pageView.setContentSize(cc.size(980, 600));//pageview展示框的大小
		this.pageView.setPosition(640,338);//pageview的坐标
		this.pageView.setAnchorPoint(0.5,0.5);//默认锚点是（0,0）
		this.addChild(this.pageView);
		
		this.layout1 = new ccui.Layout();//第一页
		this.layout1.setContentSize(cc.size(980, 600));//设置layout的大小
		this.pageView.addPage(this.layout1);
		
		this.label1 = new cc.LabelTTF("1.把装满水的容器放在较高处，另一个没有盛水的容器放在较低处；" +
				"\n2.将整个管子浸在生谁的容器中，直到管中所有空气都排出；" +
				"\n3.用手指将水管的两端堵住，并将一段移到另一个空容器中；" +
				"\n4.把两端的手指放开，水就会从高处的容器中流到低处的容器中。",gg.fontName,gg.fontSize4);
		this.label1.setPosition(this.layout1.width/2,this.layout1.height-this.label1.height/2);
		this.layout1.addChild(this.label1);	

		this.hongxi = new Angel(this.layout1, "#hongxi.png",this.callback,this);
		this.hongxi.setScale(0.8,0.7);
		this.hongxi.setPosition(400,250);

		this.playbutton(cc.p(650,350));//展示按钮
						
		this.layout2 = new ccui.Layout();//第二页
		this.layout2.setContentSize(cc.size(980, 600));
		this.pageView.addPage(this.layout2);
		
		this.hongxi1 = new cc.Sprite("#hongxi.png");
		this.hongxi1.setScale(0.8,0.7);
		this.hongxi1.setPosition(490,this.layout2.height - this.hongxi1.height/2);
		this.layout2.addChild(this.hongxi1,5);	


		var text2 = $.format("　　虹吸现象在生产和生活中运用很广泛，农业生产中利用它能将渠道中水引入田中灌溉。" +
				"可以通过虹吸管换掉鱼缸中的水，有的自动抽水马桶就是利用这个原理工作的。", 980,30);
		this.label2 = new cc.LabelTTF(text2,gg.fontName,gg.fontSize4);
		$.down(this.hongxi1,this.label2,20);
		//this.label2.setPosition(640,160);
		this.layout2.addChild(this.label2);	
		
		
		this.layout3 = new ccui.Layout();//第三页
		this.layout3.setContentSize(cc.size(980, 600));
		this.pageView.addPage(this.layout3);
		
		var text3 = $.format("　　初次进入海拔3000米以上的高山地区的人，会出现高山反应，也叫高山病。" +
				"高山病的症状因人而异。主要包括：头晕、头痛、耳鸣、恶心、呕吐、脉搏和呼吸加快、" +
				"四肢麻木等。严重的会陷入昏迷，甚至死亡", 980,30);
		this.label3 = new cc.LabelTTF(text3,gg.fontName,gg.fontSize4);
		this.label3.setPosition(this.layout3.width/2,this.layout3.height-this.label3.height/2);		
		this.layout3.addChild(this.label3);	
		
		this.gaoshan = new cc.Sprite("#gaoshan.png");
		//this.gaoshan.setScale(0.8,0.7);
		this.gaoshan.setPosition(280,350);
		this.layout3.addChild(this.gaoshan,5);	
		
		this.gaoshan1 = new cc.Sprite("#gaoshan1.png");
		//this.gaoshan1.setScale(0.8,0.7);
		this.gaoshan1.setPosition(680,350);
		this.layout3.addChild(this.gaoshan1,5);	
		
		this.yuanyinnutton = new Angel(this.layout3,"#unsel.png",this.callback,this);
		this.yuanyinnutton.setScale(0.3,0.5);
		this.yuanyinnutton.setPosition(35,200);
		var label = new cc.LabelTTF("原因:",gg.fontName,50);
		label.setPosition(this.yuanyinnutton.width/2,this.yuanyinnutton.height/2);
		this.yuanyinnutton.addChild(label);
		

		var text4 = $.format("　　因为高山地区空气稀薄，大气压强低，人们呼吸时吸入的氧气的分压也低，造成肺泡中的氧" +
				"分压降低，血液中含量减少。从而使人的神经系统、呼吸系统和心血管系统出现障碍。", 980,30);
		this.label4 = new cc.LabelTTF(text4,gg.fontName,gg.fontSize4);
		this.label4.setPosition(490,160);
		this.label4.setVisible(false);
		this.layout3.addChild(this.label4);	
	},	
	turnmenu:function(){
		this.turnleft = new ButtonScale(this,res_public.leftbutton,this.callback);
		this.turnleft.setPosition(80,376-50);
		this.turnleft.setVisible(false);       
		this.turnright = new ButtonScale(this,res_public.rightbutton,this.callback);
		this.turnright.setPosition(1200,376-50);   	    	
	},
	playbutton:function(pos){
		this.play = new Angel(this.layout1,"#unsel.png",this.callback,this);
		this.play.setScale(0.3,0.5);
		this.play.setPosition(pos);
		this.play.setEnable(true);
		var label = new cc.LabelTTF("点击",gg.fontName,50);
		label.setPosition(this.play.width/2,this.play.height/2);
		this.play.addChild(label);
	},
	update:function(){//用pageview翻页暂时没找到好的更新方法
		var index = this.pageView.getCurPageIndex();//获取当前页，以0 1 2 形式
		if(this.curIndex != index){
			if(index == 0){//当从第二页滑到第一页时，继续左滑，左滑按钮隐藏（滑到第一页）
				this.turnleft.setVisible(false);
				this.turnright.setVisible(true);
			}else if(index == 2){//当从第倒数第二页滑到最后页时，继续右滑，右滑按钮隐藏，根据自己创建的页数，手动调整（滑到最后页）
				this.turnleft.setVisible(true);
				this.turnright.setVisible(false);
			}else{
				this.turnleft.setVisible(true);
				this.turnright.setVisible(true);
			}  
			this.curIndex = index;
		}			
	},
	callback:function(p){
		switch(p){		
		case this.turnleft:
			var index = this.pageView.getCurPageIndex();
			this.pageView.scrollToPage(index - 1);
		    break;
		case this.turnright:
			var index = this.pageView.getCurPageIndex();			
			this.pageView.scrollToPage(index + 1);
			break;
		case this.play:
			p.setSpriteFrame("sel.png");
			p.setEnable(false);
			var scale = cc.scaleTo(1,0.5);
			var scale1 = cc.scaleTo(1,0.8,0.7);
			var seq = cc.sequence(scale,scale1,cc.callFunc(function(){
				p.setSpriteFrame("unsel.png");
				p.setEnable(true);
			},this)); 
			this.hongxi.runAction(seq);
			break;
		case this.yuanyinnutton:
			p.setSpriteFrame("sel.png");
			p.setEnable(false);
			this.label4.setVisible(true);
		break;
		}
	}
	
});
