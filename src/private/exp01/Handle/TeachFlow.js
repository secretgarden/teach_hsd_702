startRunNext01 = function(tag){
	switch(tag){//具体哪个菜单执行什么操作，这里修改
	case TAG_MENU1:
		ch.gotoShow(TAG_EXP_01, g_resources_show01, null, new exp01.ShowScene01(tag));
		break;
	case TAG_MENU2:
		ch.gotoRun(TAG_EXP_01, g_resources_run01, null, new exp01.ShowScene02(tag));
		break;
	case TAG_MENU3:
		gg.teach_type = TAG_LEAD;
		ch.gotoShow(TAG_EXP_01, g_resources_show01, null, new exp01.ShowScene03(tag));
		break;
		gotoShow
	case TAG_ABOUT:
		ch.gotoAbout(TAG_EXP_01, g_resources_public_about, null, res_public_about.about_j);
		break;
	}
}

//任务流
exp01.teach_flow01 = [
    {
    	tip:"花的结构",
    	tag:TAG_HUA,
    	create:[],
    	destroy:[],
    	node:true,
    	frameInfo : [
             {ex:3,name:"柱头",tag:TAG_HUA_ZHUTOU,px:312,py:465,img:"#hua/zhutou.png",st:"雌蕊包括柱头、花柱、子房",posx:755,posy:519,next:false,scale:4.2},
             {ex:3,name:"花柱",tag:TAG_HUA_HUAZHU,px:276,py:443,img:"#hua/huazhu.png",st:"雌蕊包括柱头、花柱、子房",posx:220,posy:450,next:true,scale:5.5},            
             {ex:3,name:"花药",tag:TAG_HUA_HUAYAO,px:387,py:426,img:"#hua/huayao.png",st:"雄蕊包括花药、花丝",posx:755,posy:489,next:false,scale:3},
             {ex:3,name:"花丝",tag:TAG_HUA_HUASI,px:372,py:420,img:"#hua/huasi.png",st:"雄蕊包括花药、花丝",posx:755,posy:430,next:false,scale:3},
             {ex:3,name:"花瓣",tag:TAG_HUA_HUABAN,px:363,py:303,img:"#hua/huaban.png",st:"",posx:844,posy:354,next:false},
             {ex:3,ex:3,name:"萼片",tag:TAG_HUA_EPIAN,px:191,py:253,img:"#hua/epian.png",st:"",posx:272,posy:342,next:true,scale:3},
             {ex:3,name:"胚珠",tag:TAG_HUA_PEIZHU,px:261,py:174,img:"#hua/peizhu.png",st:"",posx:666,posy:273,next:false,scale:3},
             {ex:3,name:"子房",tag:TAG_HUA_ZIFANG,px:262,py:138,img:"#hua/zifang.png",st:"雌蕊包括柱头、花柱、子房",posx:272,posy:255,next:true,scale:3.8},
             {ex:3,name:"花托",tag:TAG_HUA_HUATUO,px:262,py:119,img:"#hua/huatuo.png",st:"",posx:666,posy:237,next:false,scale:3}
             ],
             nodeName:"花的结构"
    },
    {
    	tip:"恭喜过关",
    	over:true
    }
    ]
