exp01.Label = cc.LabelTTF.extend({
	ctor:function(parent, text){
		this._super(text, gg.fontName, gg.fontSize4);
		parent.addChild(this);
		this.setAnchorPoint(0, 0.5);
	}	
});