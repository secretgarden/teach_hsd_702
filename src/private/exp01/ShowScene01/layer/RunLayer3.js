exp01.RunLayer03 = exp01.BaseLayer01.extend({
	frame:[],
	num:0,
	no:0,
	curSel:null,
	ctor:function (parent) {
		this._super(parent,5);
		this.init();
	},
	init:function () {
		cc.log("花的结构初始化");
		this._super();	
		this.loadInfo();
		//加载主图（花）
		this.loadMain();
		if(gg.teach_type == TAG_REAL){
			//实战模式下，随机一个部位闪烁
			this.loadFlash();
			this.loadWord();
		}		
	},
	loadInfo:function(){
		for(var i in gg.teach_flow01){
			var info = gg.teach_flow01[i];
			if(info.tag == TAG_HUA){
				//读取Flow里的任务流
				this.frameInfo = info.frameInfo;
				//复制一个数组，做实战中删除用
				this.frame = this.frameInfo.concat();
			}
		}
	},
	loadMain:function(){
		//花
		this.body = new cc.Sprite("#hua/body.png");
		this.body.setPosition(cc.p(gg.width*0.5 - 100,gg.height*0.5));
		this.body.setScale(0.8);
		if(gg.teach_type == TAG_REAL){
			this.body.setOpacity(50);
		}else {
			this.body.setOpacity(255);
		}
		this.addChild(this.body);
		var bw = this.body.width;
		var bh = this.body.height;
		for(var i in this.frameInfo){
			var info = this.frameInfo[i];	
			var bt = new Button(this.body,10,info.tag,info.img,this.callback,this);
			bt.setPosition(cc.p(info.px, info.py));
			bt.setAnchorPoint(0,1);
			bt.setOpacity(1);
			bt.info = info;
			this.frame = this.frameInfo.concat();
		}
		//加载名称
		this.loadTipFrame();
	},
	callback:function (p){
		this.body.setOpacity(50);
		switch (p.getTag()) {	
		case TAG_HUA_EPIAN:
		case TAG_HUA_HUABAN:
		case TAG_HUA_HUASI:
		case TAG_HUA_HUATUO:	
		case TAG_HUA_HUAYAO:
		case TAG_HUA_HUAZHU:
		case TAG_HUA_PEIZHU:
		case TAG_HUA_ZHUTOU:
		case TAG_HUA_ZIFANG:
			this.clickCheck(p);
			break;
		default:
			break;
		}
		if(gg.teach_type == TAG_REAL){
			if(!gg.flow.over_flag){
				if(gg.succeed_hua){			
					this.runAction(cc.sequence(cc.delayTime(1),cc.callFunc(function() {
						gg.flow.next();
					}, this)));		
				}
			}
		}
	},
	destroy:function(){
		this._super();
	},
	onExit:function(){
		this._super();
	}
});