exp01.ShowMainLayer01 = cc.Layer.extend({
	lead:null,
	real:null,
	curIndex: 0,
	ctor:function () {
		this._super();
		this.init();
		this.turnmenu();
		this.scheduleUpdate();
		return true;
	},
	init:function(){
		this.pageView = new ccui.PageView();//建立pageview
		this.pageView.setSwallowTouches(false);
		this.pageView.setContentSize(cc.size(980, 600));//pageview展示框的大小
		this.pageView.setPosition(640,338);//pageview的坐标
		this.pageView.setAnchorPoint(0.5,0.5);//默认锚点是（0,0）
		this.addChild(this.pageView);
		
		gg.teach_flow01 = exp01.teach_flow01;
		this.loadLayer1();
		this.loadLayer2();
		this.loadLayer3();
	},
	loadLayer1:function(){
		this.layout1 = new ccui.Layout();//第一页
		this.layout1.setContentSize(cc.size(980, 600));//设置layout的大小
		this.pageView.addPage(this.layout1);

		var marign = 10;
		
		var label1 =new exp01.Label(this.layout1, "桃花的功能和结构");
		label1.setAnchorPoint(0.5, 0.5);
		label1.setPosition(this.layout1.width/2,this.layout1.height-label1.height/2);
		
		var label2 =new exp01.Label(this.layout1, "桃\n花");
		label2.setPosition(150, 270);
		
		var label4 =new exp01.Label(this.layout1, "花柄(1个)--连接茎和花");
		label4.setPosition(230, 450);
		
		var label5 =new exp01.Label(this.layout1, "花托(1个)--着生花的各部分");
		$.down(label4, label5, marign);
		
		var label6 =new exp01.Label(this.layout1, "花萼(有萼片5个)");
		$.down(label5, label6, marign);
		
		var label7 =new exp01.Label(this.layout1, "花冠(有花瓣5个)");
		$.down(label6, label7, marign);
		
		new Bracket(this.layout1, label6, label7 ,DIRECTION_LEFT);

		var label15 = new exp01.Label(this.layout1, "花被--花开放前保护花的内部结构");
		label15.setPosition(490, 320);
		
		var label8 =new exp01.Label(this.layout1, "雄蕊(多个)");
		$.down(label7, label8, marign * 3 + gg.fontSize4);

		var label10 =new exp01.Label(this.layout1, "花药--里面有花粉");
		label10.setPosition(label8.x + label8.width + 60, label8.y + label8.height);
		
		var label11 =new exp01.Label(this.layout1, "花丝--支持花药");
		$.down(label10, label11, marign);
		
		new Bracket(this.layout1, label10, label11 ,DIRECTION_RIGHT);
		
		var label9 =new exp01.Label(this.layout1, "雌蕊(1个)");
		$.down(label8, label9, marign * 4 + gg.fontSize4);
		
		var label12 =new exp01.Label(this.layout1, "柱头--接受花粉");
		label12.setPosition(label9.x + label9.width + 60, label9.y + label9.height);
		
		var label13 =new exp01.Label(this.layout1, "花柱--支持柱头");
		$.down(label12, label13, marign);
		
		var label14 =new exp01.Label(this.layout1, "子房--内生胚珠");
		$.down(label13, label14, marign);
		
		new Bracket(this.layout1, label12, label14 ,DIRECTION_RIGHT);
		new Bracket(this.layout1, label4, label9 ,DIRECTION_RIGHT);
	},
	loadLayer2:function(){
		this.layout2 = new ccui.Layout();//第二页
		this.layout2.setContentSize(cc.size(980, 600));
		this.pageView.addPage(this.layout2);

		var marign = 10;

		var label1 = new exp01.Label(this.layout2, "小麦花的功能和结构");
		label1.setAnchorPoint(0.5, 0.5);
		label1.setPosition(this.layout2.width/2,this.layout2.height-label1.height/2);

		var label2 = new exp01.Label(this.layout2, "小\n桃\n花");
		label2.setPosition(150, 270);

		var label4 = new exp01.Label(this.layout2, "外稃(1个)--花开放前保护花的内部结构");
		label4.setPosition(230, 450);

		var label5 = new exp01.Label(this.layout2, "内稃(1个)--花开放前保护花的内部结构");
		$.down(label4, label5, marign);

		var label6 = new exp01.Label(this.layout2, "浆片(2个)--开花时,吸水膨胀推开内、外稃");
		$.down(label5, label6, marign);

		var label8 = new exp01.Label(this.layout2, "雄蕊(3个)");
		$.down(label6, label8, marign * 3 + gg.fontSize4);

		var label10 = new exp01.Label(this.layout2, "花药--里面有花粉");
		label10.setPosition(label8.x + label8.width + 60, label8.y + label8.height);

		var label11 = new exp01.Label(this.layout2, "花丝--支持花药");
		$.down(label10, label11, marign);

		new Bracket(this.layout2, label10, label11 ,DIRECTION_RIGHT);

		var label9 = new exp01.Label(this.layout2, "雌蕊(1个)");
		$.down(label8, label9, marign * 4 + gg.fontSize4);

		var label12 = new exp01.Label(this.layout2, "柱头--接受花粉");
		label12.setPosition(label9.x + label9.width + 60, label9.y + label9.height);

		label14 = new exp01.Label(this.layout2, "子房--内生胚珠");
		$.down(label12, label14, marign);

		new Bracket(this.layout2, label12, label14 ,DIRECTION_RIGHT);
		new Bracket(this.layout2, label4, label9 ,DIRECTION_RIGHT);
	},
	loadLayer3:function(){
		this.layout3 = new ccui.Layout();//第三页
		this.layout3.setContentSize(cc.size(980, 600));
		this.pageView.addPage(this.layout3);
		
		var rl = new exp01.RunLayer03(this.layout3);
		rl.setScale(0.8);
		rl.setPosition(-200, 0);
	},
	turnmenu:function(){
		this.turnleft = new ButtonScale(this,res_public.leftbutton,this.callback);
		this.turnleft.setPosition(80,376-50);
		this.turnleft.setVisible(false);       
		this.turnright = new ButtonScale(this,res_public.rightbutton,this.callback);
		this.turnright.setPosition(1200,376-50);   	    	
	},
	playbutton:function(pos){
		this.play = new Angel(this.layout1,"#unsel.png",this.callback,this);
		this.play.setScale(0.3,0.5);
		this.play.setPosition(pos);
		this.play.setEnable(true);
		var label = new cc.LabelTTF("点击",gg.fontName,50);
		label.setPosition(this.play.width/2,this.play.height/2);
		this.play.addChild(label);
	},
	update:function(){//用pageview翻页暂时没找到好的更新方法
		var index = this.pageView.getCurPageIndex();//获取当前页，以0 1 2 形式
		if(this.curIndex != index){
			if(index == 0){//当从第二页滑到第一页时，继续左滑，左滑按钮隐藏（滑到第一页）
				this.turnleft.setVisible(false);
				this.turnright.setVisible(true);
			}else if(index == 2){//当从第倒数第二页滑到最后页时，继续右滑，右滑按钮隐藏，根据自己创建的页数，手动调整（滑到最后页）
				this.turnleft.setVisible(true);
				this.turnright.setVisible(false);
			}else{
				this.turnleft.setVisible(true);
				this.turnright.setVisible(true);
			}  
			this.curIndex = index;
		}			
	},
	callback:function(p){
		switch(p){		
		case this.turnleft:
			var index = this.pageView.getCurPageIndex();
			this.pageView.scrollToPage(index - 1);
		    break;
		case this.turnright:
			var index = this.pageView.getCurPageIndex();			
			this.pageView.scrollToPage(index + 1);
			break;
		case this.play:
			p.setSpriteFrame("sel.png");
			p.setEnable(false);
			var scale = cc.scaleTo(1,0.5);
			var scale1 = cc.scaleTo(1,0.8,0.7);
			var seq = cc.sequence(scale,scale1,cc.callFunc(function(){
				p.setSpriteFrame("unsel.png");
				p.setEnable(true);
			},this)); 
			this.hongxi.runAction(seq);
			break;
		case this.yuanyinnutton:
			p.setSpriteFrame("sel.png");
			p.setEnable(false);
			this.label4.setVisible(true);
		break;
		}
	}
	
});
