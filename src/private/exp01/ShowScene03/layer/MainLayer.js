exp01.ShowMainLayer03 = cc.Layer.extend({
	lead:null,
	real:null,
	curIndex: 0,
	ctor:function () {
		this._super();
		this.init();
		this.turnmenu();
		this.scheduleUpdate();
		return true;
	},
	init:function(){
		this.pageView = new ccui.PageView();//建立pageview
		this.pageView.setSwallowTouches(false);
		this.pageView.setContentSize(cc.size(980, 600));//pageview展示框的大小
		this.pageView.setPosition(640,338);//pageview的坐标
		this.pageView.setAnchorPoint(0.5,0.5);//默认锚点是（0,0）
		this.addChild(this.pageView);

		this.loadLayer1();
		this.loadLayer2();
	},	
	loadLayer1:function(){
		this.layout1 = new ccui.Layout();//第一页
		this.layout1.setContentSize(cc.size(980, 600));//设置layout的大小
		this.pageView.addPage(this.layout1);

		this.label1 = new cc.LabelTTF("花粉落到柱头上以后,在柱头上的黏液的刺激下开始萌发,长出花粉管." +
				"\n花粉管通过花柱,进入子房,一直到达胚珠." +
				"\n然后,花粉管顶端破裂,花粉管中的精子进入胚珠内部." +
				"\n胚珠里面有软细胞,它来花粉管的精子结合,形成受精卵,完成受精过程",
				gg.fontName,gg.fontSize4);
		this.label1.setPosition(this.layout1.width/2,this.layout1.height-this.label1.height/2);
		this.layout1.addChild(this.label1);	

		this.label2 = new cc.LabelTTF("花和果实的示意图", gg.fontName,gg.fontSize4);
		this.label2.setPosition(this.layout1.width/2,400);
		this.layout1.addChild(this.label2);	

		var flower2 = new Angel(this.layout1, res_show01.flower2,this.callback,this);
		flower2.setPosition(490,250);
	},
	loadLayer2:function(){
		this.layout2 = new ccui.Layout();//第二页
		this.layout2.setContentSize(cc.size(980, 600));
		this.pageView.addPage(this.layout2);

		var marign = 10;

		var label1 = new exp01.Label(this.layout2, "受精完成后,花的各部分变化");
		label1.setPosition(90, this.layout2.height-label1.height/2);

		var label2 = new exp01.Label(this.layout2, "花萼 → 凋落(如桃、苹果)或存留(如番茄)");
		label2.setPosition(50, 500);

		var label3 = new exp01.Label(this.layout2, "花冠 → 凋落");
		$.down(label2, label3, marign);
		
		var label4 = new exp01.Label(this.layout2, "雄蕊 → 凋落");
		$.down(label3, label4, marign);

		var label5 = new exp01.Label(this.layout2, "雌蕊");
		$.down(label4, label5, marign * 6);

		var label6 = new exp01.Label(this.layout2, "柱头和花柱 → 凋落");
		label6.setPosition(170, 350);

		var label8 = new exp01.Label(this.layout2, "子房");
		$.down(label6, label8, marign * 3 + gg.fontSize4);
		
		new Bracket(this.layout2, label6, label8 ,DIRECTION_RIGHT);

		var label10 = new exp01.Label(this.layout2, "子房壁 → 发育成果皮");
		label10.setPosition(label8.x + label8.width + 60, label8.y + label8.height);

		var label11 = new exp01.Label(this.layout2, "胚珠");
		$.down(label10, label11, marign * 3);

		new Bracket(this.layout2, label10, label11 ,DIRECTION_RIGHT);

		var label9 = new exp01.Label(this.layout2, "珠被 → 发育成种皮");
		label9.setPosition(400, 240);

		var label12 = new exp01.Label(this.layout2, "受精卵 → 发育成胚");
		$.down(label9, label12, marign);

		new Bracket(this.layout2, label9, label12 ,DIRECTION_RIGHT);
		new Bracket(this.layout2, label9, label12 ,DIRECTION_LEFT);
		
		var label13 = new exp01.Label(this.layout2, "种子");
		label13.setPosition(700, 220);
		
		new Bracket(this.layout2, label10, label13 ,DIRECTION_LEFT);
		var label14 = new exp01.Label(this.layout2, "果实");
		label14.setPosition(800,255);
		
		var label15 = new exp01.Label(this.layout2, "由此可见,受精完成后,只有子房继续发育,最终成为果实,\n果实包括果皮和种子");
		label15.setPosition(90, 80);
	},
	turnmenu:function(){
		this.turnleft = new ButtonScale(this,res_public.leftbutton,this.callback);
		this.turnleft.setPosition(80,376-50);
		this.turnleft.setVisible(false);       
		this.turnright = new ButtonScale(this,res_public.rightbutton,this.callback);
		this.turnright.setPosition(1200,376-50);   	    	
	},
	playbutton:function(pos){
		this.play = new Angel(this.layout1,"#unsel.png",this.callback,this);
		this.play.setScale(0.3,0.5);
		this.play.setPosition(pos);
		this.play.setEnable(true);
		var label = new cc.LabelTTF("开花",gg.fontName,50);
		label.setPosition(this.play.width/2,this.play.height/2);
		this.play.addChild(label);
	},
	update:function(){//用pageview翻页暂时没找到好的更新方法
		var index = this.pageView.getCurPageIndex();//获取当前页，以0 1 2 形式
		if(this.curIndex != index){
			if(index == 0){//当从第二页滑到第一页时，继续左滑，左滑按钮隐藏（滑到第一页）
				this.turnleft.setVisible(false);
				this.turnright.setVisible(true);
			}else if(index == 1){//当从第倒数第二页滑到最后页时，继续右滑，右滑按钮隐藏，根据自己创建的页数，手动调整（滑到最后页）
				this.turnleft.setVisible(true);
				this.turnright.setVisible(false);
			}else{
				this.turnleft.setVisible(true);
				this.turnright.setVisible(true);
			}  
			this.curIndex = index;
		}			
	},
	callback:function(p){
		switch(p){		
		case this.turnleft:
			var index = this.pageView.getCurPageIndex();
			this.pageView.scrollToPage(index - 1);
			break;
		case this.turnright:
			var index = this.pageView.getCurPageIndex();			
			this.pageView.scrollToPage(index + 1);
			break;
		case this.play:
			p.setSpriteFrame("sel.png");
			p.setEnable(false);
			var seq = cc.sequence(cc.callFunc(function(){
				p.setSpriteFrame("unsel.png");
				p.setEnable(true);
			},this)); 
			this.flower.runAction(seq);
			break;
		case this.yuanyinnutton:
			p.setSpriteFrame("sel.png");
			p.setEnable(false);
			this.label4.setVisible(true);
			break;
		}
	}
});
